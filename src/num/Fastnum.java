/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

public class Fastnum {
	
	/**
	 * Prevent the instantiation of this class.
	 */
	private Fastnum () {}
	
	public static double sin (double x, int terms) {
		double result = x;
		double term = x;
		double xsqr = x * x;
		int j = 0;
		for (int i = 1; i <= terms; i++) {
			j += 2;
			term *= -xsqr / (j * (j + 1));
			result += term;
		}
		return result;
	}
	
	public static float sin (float x, int terms) {
		float result = x;
		float term = x;
		float xsqr = x * x;
		int j = 0;
		for (int i = 1; i <= terms; i++) {
			j += 2;
			term *= -xsqr / (j * (j + 1));
			result += term;
		}
		return result;
	}
	
	public static double cos (double x, int terms) {
		double result = 1;
		double term = 1;
		double xsqr = x * x;
		int j = 0;
		for (int i = 1; i <= terms; i++) {
			j += 2;
			term *= -xsqr / (j * (j - 1));
			result += term;
		}
		return result;
	}
	
	public static float cos (float x, int terms) {
		float result = 1;
		float term = 1;
		float xsqr = x * x;
		int j = 0;
		for (int i = 1; i <= terms; i++) {
			j += 2;
			term *= -xsqr / (j * (j - 1));
			result += term;
		}
		return result;
	}
	
	public static double tan (double x, int terms) {
		return sin(x, terms) / cos(x, terms);
	}
	
	public static float tan (float x, int terms) {
		return sin(x, terms) / cos(x, terms);
	}
	
	public static double sqrt (double x, int iterationCount) {
		if (x > 0) {
			double result = (x + 1) / 2;
			for (int i = 0; i < iterationCount; i++) {
				result = (result + x / result) / 2;
			}
			return result;
		} else if (x == 0) {
			return 0;
		} else {
			throw new ArithmeticException ("sqrt(" + x + ") is undefined");
		}
	}
	
	public static float sqrt (float x, int iterationCount) {
		if (x > 0) {
			float result = (x + 1) / 2;
			for (int i = 0; i < iterationCount; i++) {
				result = (result + x / result) / 2;
			}
			return result;
		} else if (x == 0) {
			return 0;
		} else {
			throw new ArithmeticException ("sqrt(" + x + ") is undefined");
		}
	}
	
	public static double cbrt (double x, int iterationCount) {
		double result = (2 * x + 1 / x) / 3;
		for (int i = 0; i < iterationCount; i++) {
			result = (2 * result + x / (result * result)) / 3;
		}
		return result;
	}
	
	public static float cbrt (float x, int iterationCount) {
		float result = (2 * x + 1 / x) / 3;
		for (int i = 0; i < iterationCount; i++) {
			result = (2 * result + x / (result * result)) / 2;
		}
		return result;
	}
	
	public static double exp (double x, int terms) {
		double result = 1;
		double term = 1;
		for (int i = 1; i <= terms; i++) {
			term *= x / i;
			result += term;
		}
		return result;
	}
	
	public static float exp (float x, int terms) {
		float result = 1;
		float term = 1;
		for (int i = 1; i <= terms; i++) {
			term *= x / i;
			result += term;
		}
		return result;
	}
	
	public static double ln (double x, int terms) {
		if (x > 0) {
			double result = (x - 1) / (x + 1);
			double term = result;
			double xm1sqr = (x - 1) * (x - 1);
			double xp1sqr = xm1sqr + 4 * x;
			int j = 1;
			for (int i = 1; i <= terms; i++) {
				term *= j;
				j += 2;
				term *= xm1sqr / (j * xp1sqr);
				result += term;
			}
			return 2 * result;
		} else {
			throw new ArithmeticException ("ln(" + x + ") is undefined");
		}
	}
	
	public static float ln (float x, int terms) {
		if (x > 0) {
			float result = (x - 1) / (x + 1);
			float term = result;
			float xm1sqr = (x - 1) * (x - 1);
			float xp1sqr = xm1sqr + 4 * x;
			int j = 1;
			for (int i = 1; i <= terms; i++) {
				term *= j;
				j += 2;
				term *= xm1sqr / (j * xp1sqr);
				result += term;
			}
			return 2 * result;
		} else {
			throw new ArithmeticException ("ln(" + x + ") is undefined");
		}
	}
	
	public static double sinh (double x, int terms) {
		double result = x;
		double term = x;
		double xsqr = x * x;
		int j = 0;
		for (int i = 1; i <= terms; i++) {
			j += 2;
			term *= xsqr / (j * (j + 1));
			result += term;
		}
		return result;
	}
	
	public static float sinh (float x, int terms) {
		float result = x;
		float term = x;
		float xsqr = x * x;
		int j = 0;
		for (int i = 1; i <= terms; i++) {
			j += 2;
			term *= xsqr / (j * (j + 1));
			result += term;
		}
		return result;
	}
	
	public static double cosh (double x, int terms) {
		double result = 1;
		double term = 1;
		double xsqr = x * x;
		int j = 0;
		for (int i = 1; i <= terms; i++) {
			j += 2;
			term *= xsqr / (j * (j - 1));
			result += term;
		}
		return result;
	}
	
	public static float cosh (float x, int terms) {
		float result = 1;
		float term = 1;
		float xsqr = x * x;
		int j = 0;
		for (int i = 1; i <= terms; i++) {
			j += 2;
			term *= xsqr / (j * (j - 1));
			result += term;
		}
		return result;
	}
	
	public static double tanh (double x, int terms) {
		return sinh(x, terms) / cosh(x, terms);
	}
	
	public static float tanh (float x, int terms) {
		return sinh(x, terms) / cosh(x, terms);
	}
	
	public static double arsinh (double x, int terms) {
		return ln(x + sqrt(x * x + 1, terms), terms);
	}
	
	public static float arsinh (float x, int terms) {
		return ln(x + sqrt(x * x + 1, terms), terms);
	}
	
	public static double arcosh (double x, int terms) {
		if (x >= 1) {
			return ln(x + sqrt(x * x - 1, terms), terms);
		} else {
			throw new ArithmeticException ("arcosh(" + x + ") is undefined");
		}
	}
	
	public static float arcosh (float x, int terms) {
		if (x >= 1) {
			return ln(x + sqrt(x * x - 1, terms), terms);
		} else {
			throw new ArithmeticException ("arcosh(" + x + ") is undefined");
		}
	}
	
	public static double artanh (double x, int terms) {
		if (x < 1 && x > -1) {
			return ln(x + sqrt(x * x - 1, terms), terms);
		} else {
			throw new ArithmeticException ("artanh(" + x + ") is undefined");
		}
	}
	
	public static float artanh (float x, int terms) {
		if (x < 1 && x > -1) {
			return ln(x + sqrt(x * x - 1, terms), terms);
		} else {
			throw new ArithmeticException ("artanh(" + x + ") is undefined");
		}
	}
	
	public static double factorial (long n) {
		double r = EMath.factorial(n);
		if (Double.isNaN(r)) {
			throw new ArithmeticException (n + "! is undefined");
		} else if (Double.isInfinite(r)) {
			throw new ArithmeticException ("Infinity");
		} else {
			return r;
		}
	}
	
	public static float factorial (int n) {
		float r = (float)(EMath.factorial(n));
		if (Double.isNaN(r)) {
			throw new ArithmeticException (n + "! is undefined");
		} else if (Double.isInfinite(r)) {
			throw new ArithmeticException ("Infinity");
		} else {
			return r;
		}
	}
	
	public static double hypot (double x, double y, int iterationCount) {
		return sqrt((x * x) + (y * y), iterationCount);
	}
	
	public static float hypot (float x, float y, int iterationCount) {
		return sqrt((x * x) + (y * y), iterationCount);
	}
} 
