/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

import java.math.*;

/**
 * All mathematical functions used in this library are directly and indirectly called from this
 * file, if they're needed in arbitrary precision. Most of these methods are NOT safe to be
 * called directly from scripts other than the ones in the num package, as this can cause
 * endless loops and or incorrect results. Instead the more or less equivalent methods in the
 * Arbitrary or ADecimal classes should be called, as they do the exception handling.
 * 
 * Almost every function can only be calculated to a certain precision which is specified by the
 * number of significant decimal digits.
 * @author Johannes Kloimböck
 *
 */
class Functions {
	
	/**
	 * Prevent the instantiation of this class.
	 */
	private Functions () {}
	
	/**
	 * Constants that are calculated during runtime, are cached in order to reuse them
	 * in other calculations, instead of recalculating them every time. If, however, the
	 * required precision is higher than the one of these constants, it is recalculated and
	 * again stored in these variables. In the case of "ln10Cached", it is not recalculated
	 * from scratch. Instead, the cached value is used to be the first approximation in the
	 * calculation to make the computing process a bit faster.
	 */
	private static BigDecimal piCached = new BigDecimal (StrictMath.PI);
	private static int piCachedMaxDigits = 15;
	private static BigDecimal eCached = new BigDecimal (StrictMath.E);
	private static int eCachedMaxDigits = 15;
	private static BigDecimal em1Cached = new BigDecimal (1 / StrictMath.E);
	private static int em1CachedMaxDigits = 15;
	private static BigDecimal emCached = new BigDecimal ("0.57721566490153286060651209008240243104215933593992");
	private static int emCachedMaxDigits = 50;
	private static final double ln10 = StrictMath.log(10);
	static BigDecimal ln10Cached = new BigDecimal (ln10);
	static int ln10CachedMaxDigits = 15;
	private static BigDecimal ln2Cached = new BigDecimal (StrictMath.log(2.0));
	private static int ln2CachedMaxDigits = 15;
	private static final BigInteger [] powsOf10 = new BigInteger [30];
	private static int populatedpowOf10 = -1;
	
	private static final BigDecimal HALF = BigDecimal.valueOf(5, 1);
	
	/**
	 * Checks, whether the number represented by a BigDecimal is an integer or not.
	 * If the scale of the (trimmed) BigDecimal variable is not larger than 0,
	 * the number is an integer (as a BigDecimal is defined as unscaledValue * 10^(-scale)).
	 * The unscaled value is a BigInteger which is, as the name states an integer.
	 * @param n is the number to be checked
	 * @return boolean value; if n is an integer it is true, else it is false
	 */
	protected static boolean isInteger (BigDecimal n) {
		if (stripTrailingZeros(n, false).scale() <= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Makes a non-integer number an integer one by discarding all the digits coming after the
	 * decimal point. The method setScale() will extract the required digits.
	 * @param n is a number with some decimal places represented by a string.
	 * @return a number also represented by a string which only contains the integer part of n
	 */
	protected static BigDecimal toIntegerNumber (BigDecimal n) {
		return n.setScale(0, RoundingMode.DOWN);
	}
	
	private static BigDecimal roundEstimate (BigDecimal z, int n) {
		int precApprox = precisionEstimate(z) - 1;
		return z.setScale(z.scale() - precApprox + n, RoundingMode.HALF_UP);
	}
	
	/**
	 * Calculates the factorial of an integer to a certain precision.
	 * @param n is the number (should be an integer) of which the factorial is to be calculated
	 * @param n0 is the number of significant digits of the factorial
	 * @return the factorial of n (= n * (n - 1) * (n - 2) * ... * 2 * 1)
	 */
	protected static BigDecimal factorial (BigDecimal n, int n0) {
		int in = n.intValueExact();
		BigDecimal result = n;
		if (n.signum() == 0) {
			return BigDecimal.ONE;
		} else if (n.compareTo(BigDecimal.ONE) == 0) {
			return BigDecimal.ONE;
		} else {
			for (int i = 1; i < in; i++) {
				BigDecimal subtrahend = new BigDecimal (i);
				BigDecimal factor = n.subtract(subtrahend);
				result = result.multiply(factor).round(new MathContext (n0));
			}
			return result;
		}
	}
	
	/**
	 * Calculates the factorial of an integer
	 * @param n is the number (should be an integer) of which the factorial is to be calculated
	 * @return the factorial of n (= n * (n - 1) * (n - 2) * ... * 2 * 1) to unlimited precision
	 */
	protected static BigDecimal factorial (BigDecimal n) {
		BigInteger result = BigInteger.ONE;
		if (n.signum() == 0) {
			return BigDecimal.ONE;
		} else if (n.compareTo(BigDecimal.ONE) == 0) {
			return BigDecimal.ONE;
		} else {
			BigInteger n1 = n.toBigInteger();
			BigInteger factor;
			for (BigInteger i = BigInteger.ZERO; i.compareTo(n1) < 0; i = i.add(BigInteger.ONE)) {
				factor = n1.subtract(i);
				result = result.multiply(factor);
			}
			return new BigDecimal (result);
		}
	}
	
	/**
	 * The implementation of the Chudnovsky-Algorithm to compute Pi (3.14159...) to a given
	 * number of digits. See https://en.wikipedia.org/wiki/Chudnovsky_algorithm for further
	 * details.
	 * @param o is the number Pi should be multiplied with
	 * @param n is the number of digits Pi should be calculated to
	 * @return Pi to given precision and multiplied by o
	 */
	private static BigDecimal piFaster (BigDecimal o, int n) {
		if (n <= piCachedMaxDigits) {
			return piCached.multiply(o);
		}
		BigDecimal c = sqrt(new BigDecimal (1823176476672000L), n);
		BigDecimal kk = new BigDecimal (6);
		BigDecimal m = BigDecimal.ONE;
		BigDecimal l = new BigDecimal (13591409);
		BigDecimal x = BigDecimal.ONE;
		BigDecimal term = BigDecimal.ONE;
		BigDecimal big = new BigDecimal (-262537412640768000L);
		BigDecimal result = l;
		BigDecimal k = BigDecimal.ZERO;
		BigDecimal sixteen = new BigDecimal (16);
		BigDecimal twelve = new BigDecimal (12);
		BigDecimal fiveHundredMill = new BigDecimal (545140134);
		while (term.signum() != 0) {
			m = m.multiply(kk);
			m = m.multiply(kk.multiply(kk).subtract(sixteen));
			m = m.divide(k.add(BigDecimal.ONE).pow(3), n, RoundingMode.HALF_UP);
			l = l.add(fiveHundredMill);
			x = x.multiply(big);
			term = m.multiply(l).divide(x, n, RoundingMode.HALF_UP);
			result = result.add(term);
			k = k.add(BigDecimal.ONE);
			kk = kk.add(twelve);
		}
		piCached = c.divide(result, n, RoundingMode.HALF_UP);
		piCachedMaxDigits = n;
		return piCached.multiply(o);
	}
	
	private static BigDecimal eLowPrec (int n) {
		BigDecimal result = BigDecimal.ZERO;
		BigDecimal term = BigDecimal.ONE;
		int i = 0;
		while (term.signum() != 0) {
			result = result.add(term);
			term = term.divide(new BigDecimal (i + 1), n, RoundingMode.HALF_UP);
			i++;
		}
		return result;
	}
	
	private static int factorialSizeFromTenPower (int pow) {
		double result = pow;
		
		for (int i = 0; i < 25; i++) {
			result = (double)(pow) / (StrictMath.log10(result / StrictMath.E));
		}
		
		return (int)(result) + 1;
	}
	
	private static BigDecimal eHighPrec (int n) {
		int i = factorialSizeFromTenPower(n);
		
		BigInteger sum = BigInteger.ONE;
		int k = i;
		BigInteger factor = BigInteger.ONE;
		for (int j = 0; j < i; j++) {
			factor = factor.multiply(BigInteger.valueOf(k));
			k--;
			sum = sum.add(factor);
		}
		
		return new BigDecimal (sum).divide(new BigDecimal (factor), n, RoundingMode.HALF_UP);
	}


	private static BigDecimal em1LowPrec (int n) {
		BigDecimal result = BigDecimal.ZERO;
		BigDecimal term = BigDecimal.ONE;
		int i = 0;
		while (term.signum() != 0) {
			result = result.add(term);
			term = term.divide(new BigDecimal (i + 1), n, RoundingMode.HALF_UP);
			term = term.negate();
			i = i + 1;
		}
		return result;
	}
	
	private static BigDecimal em1HighPrec (int n) {		
		int i = factorialSizeFromTenPower(n);
		
		BigInteger sum = BigInteger.ONE;
		int k = i;
		BigInteger factor = BigInteger.ONE;
		for (int j = 0; j < i; j++) {
			factor = factor.multiply(BigInteger.valueOf(k));
			k--;
			sum = sum.add(factor);
		}
		
		return new BigDecimal (factor).divide(new BigDecimal (sum), n, RoundingMode.HALF_UP);
	}
	
	/**
	 * Calculates Euler's Number (e = 2.71828...) to a given precision using the Taylor Series
	 * of exp(1).
	 * @param m the number e should be multiplied with
	 * @param n the number of digits e should be calculated to
	 * @return Euler's Number to given precision and multiplied by m
	 */
	protected static BigDecimal eFaster (int n) {
		if (n <= eCachedMaxDigits) {
			return eCached.round(new MathContext (n));
		}
		
		if (n < 10000) {
			eCached = eLowPrec(n);
			eCachedMaxDigits = n;
			return eCached;
		} else {
			eCached = eHighPrec(n);
			eCachedMaxDigits = n;
			return eCached;
		}
	}

	protected static BigDecimal em1Faster (int n) {
		if (n <= em1CachedMaxDigits) {
			return em1Cached.round(new MathContext (n));
		}
		
		if (n < 10000) {
			em1Cached = em1LowPrec(n);
			em1CachedMaxDigits = n;
			return em1Cached;
		} else {
			em1Cached = em1HighPrec(n);
			em1CachedMaxDigits = n;
			return em1Cached;
		}
	}
	
	/**
	 * Calculates exp(z) (=e^z) to a given precision using the Taylor Series of the exponential
	 * function with the base Euler's Number (2.71828...). If z is between -1 and 1, the method
	 * will directly calculate exp(z) via the series. If z however is out of this range, e will
	 * be calculated separately via eFaster (if z > 1) or em1Faster (z < -1) and will be risen
	 * to the power of the integer-part of z via recursive multiplying and squaring
	 * (pow method). This value will then be multiplied by Euler's Number risen to the decimal
	 * part of z.
	 * @param z is the power Euler's number should be risen to
	 * @param n is the number of digits exp(z) should be calculated to
	 * @return exp(z) to given precision
	 */
	protected static BigDecimal expFaster (BigDecimal z, int n) {
		BigDecimal number = z;
		if (number.compareTo(BigDecimal.ONE) == 0) {
			return eFaster(n);
		} else if (number.signum() == 0) {
			return BigDecimal.ONE;
		} else if (number.compareTo(BigDecimal.ONE) < 0 && number.compareTo(new BigDecimal (-1)) > 0) {
			BigDecimal result = BigDecimal.ZERO;
			BigDecimal term = BigDecimal.ONE;
			number = number.multiply(BigDecimal.valueOf(125, 3));
			int i = 1;
			while (term.signum() != 0) {
				result = result.add(term);
				term = term.multiply(number);
				term = term.divide(new BigDecimal (i), n, RoundingMode.HALF_UP);
				i++;
			}
			return result.pow(8, new MathContext (n));
		} else if (number.compareTo(BigDecimal.ONE) > 0) {
			BigDecimal number1 = number;
			BigInteger number2 = number1.toBigInteger();
			number1 = number.subtract(new BigDecimal (number2));
			BigDecimal result = expFaster(number1, n);
			result = result.multiply(eFaster(n).pow(number2.intValueExact(), new MathContext (n)));
			return result;
		} else {
			BigDecimal number1 = number;
			BigInteger number2 = number1.toBigInteger();
			number1 = number.subtract(new BigDecimal (number2));
			BigDecimal result = expFaster(number1, n);
			result = result.multiply(em1Faster(n).pow(StrictMath.abs(number2.intValueExact()), new MathContext (n)));
			return result;
		}
	}
	
	/**
	 * Calculates exp(z) like the previous method. However, it is private, because it is meant
	 * to be used in the lnNewton method where Euler's Number and its reciprocal value should be
	 * precalculated instead of recalculating it in every iteration.
	 * @param z is the power e will be risen to
	 * @param e is the precalculated value of e
	 * @param ode is the precalculated value of 1/e
	 * @param n is the number of digits exp(z) should be calculated to
	 * @return exp(z) to given precision
	 */
	private static BigDecimal expFaster (BigDecimal z, BigDecimal e, BigDecimal ode, int n) {
		BigDecimal number = z;
		if (number.compareTo(BigDecimal.ONE) == 0) {
			return e;
		} else if (number.signum() == 0) {
			return BigDecimal.ONE;
		} else if (number.compareTo(BigDecimal.ONE) < 0 && number.compareTo(new BigDecimal (-1)) > 0) {
			number = number.multiply(BigDecimal.valueOf(25, 2));
			BigDecimal result = BigDecimal.ZERO;
			BigDecimal term = BigDecimal.ONE;
			int i = 1;
			while (term.signum() != 0) {
				result = result.add(term);
				term = term.multiply(number);
				term = term.divide(new BigDecimal (i), n, RoundingMode.HALF_UP);
				i++;
			}
			return result.pow(4, new MathContext (n));
		} else if (number.compareTo(BigDecimal.ONE) > 0) {
			BigDecimal number1 = number;
			BigInteger number2 = number1.toBigInteger();
			number1 = number.subtract(new BigDecimal (number2));
			BigDecimal result = expFaster(number1, e, ode, n);
			result = result.multiply(e.pow(number2.intValueExact(), new MathContext (n)));
			return result.round(new MathContext (n));
		} else {
			BigDecimal number1 = number;
			BigInteger number2 = number1.toBigInteger();
			number1 = number.subtract(new BigDecimal (number2));
			BigDecimal result = expFaster(number1, e, ode, n);
			result = result.multiply(ode.pow(StrictMath.abs(number2.intValueExact()), new MathContext (n)));
			return result.round(new MathContext (n));
		}
	}
	
	/**
	 * Calculates the sine function with the Taylor series of sin(x). This function does not
	 * optimize the argument (this is done in the sinr method). Therefore, this method is
	 * private.
	 * @param z is the number of which the sine function is to be calculated
	 * @param n is the number of digits sin(x) should be calculated to
	 * @return sin(z) in radiants to given precision
	 */
	private static BigDecimal sinriFaster (BigDecimal z, int n) {
		BigDecimal result = BigDecimal.ZERO;
		BigDecimal term = z;
		BigDecimal number = term;
		number = number.multiply(number).round(new MathContext (n));
		int mag = digitsm1Estimate(z);
		int termMag = digitsm1Estimate(term);
		int j = 0;
		int i = 1;
		while (term.signum() != 0) {
			i += 2;
			result = result.add(term);
			if (j % 5 == 4) {
				termMag = digitsm1Estimate(term);
				number = number.round(new MathContext ((n + termMag + 1 > 0) ? n + termMag + 1 : 1));
			}
			term = term.multiply(number);
			term = term.divide(new BigDecimal ((long)(i) * (long)(i - 1)), n - mag + 1, RoundingMode.HALF_UP);
			term = term.negate();
			j++;
		}
		return result.round(new MathContext (n));
	}
	
	/**
	 * Optimizes the argument to make the calculation of the sine function using Taylor series
	 * (see sinriFaster method) faster.
	 * @param number is the number (in radiants) of which the sine is to be calculated
	 * @param pibd is a cached value of pi which is now technically useless, as the value of
	 * pi is already cached in the variable piCached. Nevertheless, it is practical, because it
	 * avoids having to write "pi(BigDecimal.ONE, n)" all the time. 
	 * @param n is the precision to which sin(x) is to be calculated
	 * @return the sine of z to given precision (in radiants)
	 */
	protected static BigDecimal sinr (BigDecimal number, BigDecimal pibd, int n) {
		BigDecimal pibd2 = multiplyByPowerOfTwo(pibd, 1);
		BigDecimal number1 = number;
		if (number.compareTo(pibd2) <= 0 && number.compareTo(pibd2.negate()) >= 0) {
			number = multiplyByPowerOfTwo(number, 1);
			number = number.scaleByPowerOfTen(-1);
			if (number.signum() >= 0) {
				BigDecimal sinx5 = sinriFaster(number, n);
				BigDecimal sinx5sqr = sinx5.multiply(sinx5).round(new MathContext (n));
				BigDecimal sin = sinx5.multiply(new BigDecimal (5));
				sinx5 = sinx5.multiply(sinx5sqr);
				sin = sin.subtract(multiplyByPowerOfTwo(sinx5, 1).scaleByPowerOfTen(1));
				sinx5 = sinx5.multiply(sinx5sqr);
				sin = sin.add(multiplyByPowerOfTwo(sinx5, 4));
				return sin;
			} else {
				BigDecimal sinx5 = sinriFaster(number.negate(), n);
				BigDecimal sinx5sqr = sinx5.multiply(sinx5).round(new MathContext (n));
				BigDecimal sin = sinx5.multiply(new BigDecimal (-5));
				sinx5 = sinx5.multiply(sinx5sqr);
				sin = sin.add(multiplyByPowerOfTwo(sinx5, 1).scaleByPowerOfTen(1));
				sinx5 = sinx5.multiply(sinx5sqr);
				sin = sin.subtract(multiplyByPowerOfTwo(sinx5, 4));
				return sin;
			}
		} else if (number.compareTo(pibd2) > 0) {
			number = multiplyByPowerOfTwo(number, 1);
			number = number.scaleByPowerOfTen(-1);
			BigDecimal factor = number.divideToIntegralValue(pibd2);
			BigDecimal subtrahend = factor.multiply(pibd2);
			number1 = number.subtract(subtrahend);
			BigDecimal sinx5 = sinriFaster(number1, n);
			BigDecimal sinx5sqr = sinx5.multiply(sinx5).round(new MathContext (n));
			BigDecimal sin = sinx5.multiply(new BigDecimal (5));
			sinx5 = sinx5.multiply(sinx5sqr);
			sin = sin.subtract(multiplyByPowerOfTwo(sinx5, 1).scaleByPowerOfTen(1));
			sinx5 = sinx5.multiply(sinx5sqr);
			sin = sin.add(multiplyByPowerOfTwo(sinx5, 4));
			return sin;
		} else {
			number1 = number.negate();
			return sinr(number1, pibd, n).negate();
		}
	}
	
	/**
	 * Converts the argument from degrees into radiants, so that it can be processed by the
	 * sinr method to calculate its sine.
	 * @param z is the argument in degrees
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which sin(x) is to be calculated
	 * @return the sine function of z in degrees to given precision
	 */
	protected static BigDecimal sind (BigDecimal z, BigDecimal pi, int n) {
		z = z.multiply(pi).divide(new BigDecimal (180), new MathContext (n));
		return sinr(z, pi, n);
	}
	
	/**
	 * Converts the argument from gons into radiants, so that it can be processed by the
	 * sinr method to calculate its sine.
	 * @param z is the argument in gons
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which sin(x) is to be calculated
	 * @return the sine function of z in gons to given precision
	 */
	protected static BigDecimal sing (BigDecimal z, BigDecimal pi, int n) {
		z = z.multiply(pi).divide(new BigDecimal (200), new MathContext (n));
		return sinr(z, pi, n);
	}
	
	/**
	 * Calculates the cosine function with the Taylor series of cos(x). This function does not
	 * optimize the argument (this is done in the cosr method). Therefore, this method is
	 * private.
	 * @param z is the number of which the cosine function is to be calculated
	 * @param n is the number of digits cos(x) should be calculated to
	 * @return cos(z) in radiants to given precision
	 */
	private static BigDecimal cosriFaster (BigDecimal number, int n) {
		BigDecimal result = BigDecimal.ZERO;
		BigDecimal term = BigDecimal.ONE;
		number = number.multiply(number).round(new MathContext (n));
		int mag = digitsm1Estimate(number);
		int termMag = digitsm1Estimate(term);
		int j = 0;
		int i = 0;
		while (term.signum() != 0) {
			i += 2;
			result = result.add(term);
			if (j % 5 == 4) {
				termMag = digitsm1Estimate(term);
				number = number.round(new MathContext ((n + termMag + 1 > 0) ? n + termMag + 1 : 1));
			}
			term = term.multiply(number);
			term = term.divide(new BigDecimal ((long)(i) * (long)(i - 1)), n - mag + 1, RoundingMode.HALF_UP);
			term = term.negate();
			j++;
		}
		return result;
	}
	
	/**
	 * Optimizes the argument to make the calculation of the cosine function using Taylor series
	 * (see cosriFaster method) faster.
	 * @param number is the number (in radiants) of which the cosine is to be calculated
	 * @param pibd is a cached value of pi which is now technically useless, as the value of
	 * pi is already cached in the variable piCached. Nevertheless, it is practical, because it
	 * avoids having to write "pi(BigDecimal.ONE, n)" all the time. 
	 * @param n is the precision to which cos(x) is to be calculated
	 * @return the cosine of z to given precision (in radiants)
	 */
	protected static BigDecimal cosr (BigDecimal number, BigDecimal pibd, int n) {
		BigDecimal pibd2 = pibd.add(pibd);
		number = number.multiply(BigDecimal.valueOf(25, 2));
		BigDecimal number1 = number;
		if (number.compareTo(pibd2) <= 0 && number.compareTo(pibd2.negate()) >= 0 && number.signum() != 0) {
			BigDecimal cosx4 = cosriFaster(number, n);
			BigDecimal cosx4pow = cosx4.multiply(cosx4).round(new MathContext (n));
			BigDecimal cos = BigDecimal.ONE;
			cos = cos.subtract(multiplyByPowerOfTwo(cosx4pow, 3));
			cosx4pow = cosx4pow.multiply(cosx4pow);
			return cos.add(multiplyByPowerOfTwo(cosx4pow, 3));
		} else {
			BigDecimal factor = number.divide(pibd2, 0, RoundingMode.DOWN);
			BigDecimal subtrahend = factor.multiply(pibd2);
			number1 = number.subtract(subtrahend);
			BigDecimal cosx4 = cosriFaster(number1, n);
			BigDecimal cosx4pow = cosx4.multiply(cosx4).round(new MathContext (n));
			BigDecimal cos = BigDecimal.ONE;
			cos = cos.subtract(multiplyByPowerOfTwo(cosx4pow, 3));
			cosx4pow = cosx4pow.multiply(cosx4pow);
			return cos.add(multiplyByPowerOfTwo(cosx4pow, 3));
		}
	}
	
	/**
	 * Converts the argument from degrees into radiants, so that it can be processed by the
	 * cosr method to calculate its cosine.
	 * @param z is the argument in degrees
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which cos(x) is to be calculated
	 * @return the cosine function of z in degrees to given precision
	 */
	protected static BigDecimal cosd (BigDecimal number, BigDecimal pi, int n) {
		number = number.multiply(pi).divide(new BigDecimal (180), new MathContext (n));
		return cosr(number, pi, n);
	}
	
	/**
	 * Converts the argument from gons into radiants, so that it can be processed by the
	 * sinr method to calculate its sine.
	 * @param z is the argument in gons
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which sin(x) is to be calculated
	 * @return the sine function of z in gons to given precision
	 */
	protected static BigDecimal cosg (BigDecimal number, BigDecimal pi, int n) {
		number = number.multiply(pi).divide(new BigDecimal (200), new MathContext (n));
		return cosr(number, pi, n);
	}
	
	/**
	 * Calculates the tangent of an angle in radiants. This is done by dividing the sine and
	 * the cosine of it. The cosine is calculated as sqrt(1 - sin^2(x)) which is faster than
	 * calculating the Taylor Series for the cosine.
	 * @param z is the number of which the tangent is to be calculated
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which tan(x) is to be calculated
	 * @return the tangent of z to given precision
	 */
	protected static BigDecimal tanr (BigDecimal z, BigDecimal pi, int n) {
		BigDecimal sin = sinr(z, pi, n);
		BigDecimal cos = sqrt(BigDecimal.ONE.subtract(sin.multiply(sin)), n);
		if (z.abs().compareTo(multiplyByPowerOfTwo(pi, 1)) > 0) {
			BigDecimal factor = z.divideToIntegralValue(pi);
			z = z.subtract(pi.multiply(factor));
		}
		if (z.abs().compareTo(pi.multiply(HALF)) > 0 && z.abs().compareTo(pi.multiply(BigDecimal.valueOf(15, 1))) <= 0) {
			cos = cos.negate();
		}
		return sin.divide(cos, new MathContext (n));
	}
	
	/**
	 * Calculates the tangent of an angle in degrees. This is done by dividing the sine and
	 * the cosine of it. The cosine is internally calculated as sqrt(1 - sin^2(x)) which is
	 * faster than calculating the Taylor Series for the cosine.
	 * @param z is the number of which the tangent is to be calculated
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which tan(x) is to be calculated
	 * @return the tangent of z to given precision
	 */
	protected static BigDecimal tand (BigDecimal z, BigDecimal pi, int n) {
		return tanr(z.multiply(pi).divide(new BigDecimal (180), new MathContext (n)), pi, n);
	}
	
	/**
	 * Calculates the tangent of an angle in gons. This is done by dividing the sine and
	 * the cosine of it. The cosine is internally calculated as sqrt(1 - sin^2(x)) which is
	 * faster than calculating the Taylor Series for the cosine.
	 * @param z is the number of which the tangent is to be calculated
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which tan(x) is to be calculated
	 * @return the tangent of z to given precision
	 */
	protected static BigDecimal tang (BigDecimal z, BigDecimal pi, int n) {
		return tanr(z.multiply(pi).divide(new BigDecimal (200), new MathContext (n)), pi, n);
	}
	
	/**
	 * Calculates the cotangent of an angle in radiants. This is done by dividing the cosine and
	 * the sine of it. The cosine is calculated as sqrt(1 - sin^2(x)) which is faster than
	 * calculating the Taylor Series for the cosine.
	 * @param z is the number of which the cotangent is to be calculated
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which cot(x) is to be calculated
	 * @return the cotangent of z to given precision
	 */
	protected static BigDecimal cotr (BigDecimal z, BigDecimal pi, int n) {
		BigDecimal sin = sinr(z, pi, n);
		BigDecimal cos = sqrt(BigDecimal.ONE.subtract(sin.multiply(sin)), n);
		if (z.abs().compareTo(multiplyByPowerOfTwo(pi, 1)) > 0) {
			BigDecimal factor = z.divideToIntegralValue(pi);
			z = z.subtract(pi.multiply(factor));
		}
		if (z.abs().compareTo(pi.multiply(HALF)) > 0 && z.abs().compareTo(pi.multiply(BigDecimal.valueOf(15, 1))) <= 0) {
			cos = cos.negate();
		}
		return cos.divide(sin, new MathContext (n));
	}
	
	/**
	 * Calculates the cotangent of an angle in degrees. This is done by dividing the cosine and
	 * the sine of it. The cosine is internally calculated as sqrt(1 - sin^2(x)) which is
	 * faster than calculating the Taylor Series for the cosine.
	 * @param z is the number of which the cotangent is to be calculated
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which cot(x) is to be calculated
	 * @return the cotangent of z to given precision
	 */
	protected static BigDecimal cotd (BigDecimal z, BigDecimal pi, int n) {
		return cotr(z.multiply(pi).divide(new BigDecimal (180), new MathContext (n)), pi, n);
	}
	
	/**
	 * Calculates the cotangent of an angle in gons. This is done by dividing the cosine and
	 * the sine of it. The cosine is internally calculated as sqrt(1 - sin^2(x)) which is
	 * faster than calculating the Taylor Series for the cosine.
	 * @param z is the number of which the cotangent is to be calculated
	 * @param pi is the cached value of pi
	 * @param n is the number of digits to which cot(x) is to be calculated
	 * @return the cotangent of z to given precision
	 */
	protected static BigDecimal cotg (BigDecimal z, BigDecimal pi, int n) {
		return cotr(z.multiply(pi).divide(new BigDecimal (200), new MathContext (n)), pi, n);
	}
	
	/**
	 * Calculates the hyperbolic sine of the argument by its exponential definition
	 * (exp(x)-exp(-x)) * 0.5.
	 * @param z is the number of which the hyperbolic sine is calculated
	 * @param n is the number of digits to which sinh(x) is calculated
	 * @return the hyperbolic sine of z to given precision
	 */
	protected static BigDecimal sinh (BigDecimal z, int n) {
		BigDecimal ez = exp(z, n);
		BigDecimal result = ez;
		BigDecimal emz = BigDecimal.ONE.divide(ez, new MathContext (n));
		result = ez.subtract(emz);
		return result.multiply(HALF);
	}
	
	/**
	 * Calculates the hyperbolic cosine of the argument by its exponential definition
	 * (exp(x)+exp(-x)) * 0.5.
	 * @param z is the number of which the hyperbolic cosine is calculated
	 * @param n is the number of digits to which cosh(x) is calculated
	 * @return the hyperbolic cosine of z to given precision
	 */
	protected static BigDecimal cosh (BigDecimal z, int n) {
		BigDecimal ez = exp(z, n);
		BigDecimal emz = BigDecimal.ONE.divide(ez, new MathContext (n));
		BigDecimal sum = ez.add(emz);
		return sum.multiply(HALF);
		
	}
	
	/**
	 * Calculates the hyperbolic tangent of the argument by its exponential definition
	 * (exp(2x)-1) / (exp(2x+1).
	 * @param z is the number of which the hyperbolic tangent is calculated
	 * @param n is the number of digits to which tanh(x) is calculated
	 * @return the hyperbolic tangent of z to given precision
	 */
	protected static BigDecimal tanh (BigDecimal z, int n) {
		BigDecimal eh2z = exp(z.multiply(new BigDecimal (2)), n);
		BigDecimal zaehler = eh2z.subtract(BigDecimal.ONE);
		BigDecimal nenner = eh2z.add(BigDecimal.ONE);
		return zaehler.divide(nenner, new MathContext (n));
	}
	
	/**
	 * Calculates the arithmetic mean of two numbers (n + o) / 2 to infinite precision.
	 * This method is used to calculate the agm (=arithmetic-geometric mean) of two positive
	 * real numbers.
	 * @param n is the first number
	 * @param o is the second number
	 * @return (n + o) * 0.5
	 */
	private static BigDecimal am (BigDecimal n, BigDecimal o) {
		BigDecimal m = n.add(o);
		m = m.multiply(HALF);
		return m;
	}
	
	/**
	 * Calculates the geometric mean of two numbers (sqrt(n * o)). This method is also used
	 * in the agm method to calculate the arithmetic-geometric mean of two numbers. Both numbers
	 * have to have the same sign.
	 * @param n is the first number
	 * @param o is the second number
	 * @param n0 is the number of digits the result should have
	 * @return sqrt(n * o) to given precision
	 */
	private static BigDecimal gm (BigDecimal n, BigDecimal o, int n0) {
		BigDecimal p = n.multiply(o).round(new MathContext (n0));
		BigDecimal result = sqrt(p, n0);
		return result;
	}
	
	/**
	 * Calculates the arithmetic-geometric mean of two numbers. The agm is computed by
	 * repeatedly calculating the arithmetic and the geometric mean of two numbers which
	 * converge to one number. Both numbers have to be non-negative.
	 * @param l is the first number
	 * @param m is the second number
	 * @param n is the number of digits of precision
	 * @return the agm of l and m to given precision
	 */
	protected static BigDecimal agm (BigDecimal l, BigDecimal m, int n) {
		if (l.signum() == 0 || m.signum() == 0) {
			return BigDecimal.ZERO;
		} else if (l == m || l.compareTo(m) == 0) {
			return l;
		}
		
		BigDecimal abd = l;
		BigDecimal gbd = m;
		BigDecimal abd1 = abd;
		BigDecimal gbd1 = gbd;
		int mag = 0;
		
		int i = 0;
		do {
			abd1 = am(abd, gbd);
			gbd1 = gm(abd, gbd, n);
			abd = abd1;
			gbd = gbd1;
			mag = digitsm1Estimate(abd);
		} while (mag != digitsm1Estimate(gbd) - 1 && mag - 1 != digitsm1Estimate(gbd) && mag != digitsm1Estimate(gbd));
		
		if (mag != digitsm1Estimate(gbd)) {
			for (; i < 2; i++) {
				abd1 = am(abd, gbd);
				gbd1 = gm(abd, gbd, n);
				abd = abd1;
				gbd = gbd1;
				mag = digitsm1Estimate(abd);
			}
		}
		
		boolean b = false;
		i = 1;
		int magDiff = 0;
		int prec = n;
		while (b == false) {
			magDiff = digitsm1Estimate(abd.subtract(gbd));
			abd1 = am(abd, gbd);
			if ((-magDiff + mag) < 20) {
				gbd1 = gm(abd, gbd, n);
			} else {
				prec = -magDiff + mag - 8;
				gbd1 = sqrtApprox(abd.multiply(gbd).round(new MathContext (n)), gbd1.round(new MathContext (prec)), prec, n);
			}
			abd = abd1;
			gbd = gbd1;
			if (i >= n << 1) {
				b = true;
			}
			
			i <<= 1;
		}
		BigDecimal result = am(abd, gbd);
		return result;
	}
	
	/**
	 * Calculates the cuberoot (cbrt) of a real number via Newton's method. The initial guess is
	 * computed using the StrictMath library (see doublecbrt method) which is always accurate
	 * to about 15 digits. The number can be either positive, zero or negative. It can detect
	 * cube numbers during the iteration process to then make the computation a lot faster.
	 * @param m is the number of which the cuberoot is to be calculated
	 * @param n0 is the number of digits to which cbrt(x) is calculated
	 * @return the cuberoot of m to given precision
	 */
	protected static BigDecimal cbrt (BigDecimal m, int n0) {
		if (m.signum() == 0) {
			return BigDecimal.ZERO;
		}
		BigDecimal result = doublecbrt(m);
		if (result.pow(3).compareTo(m) == 0) {
			return result.round(new MathContext (n0));
		}
		
		int mag = digitsm1Estimate(m) - 1;
		int length = (int)(StrictMath.log(n0) / StrictMath.log(1.5)) - 4;
		int [] precs = new int [length];
		double n1 = n0;
		int i = 0;
		while (i < length) {
			precs [i] = (int)(n1) + 3;
			n1 /= 1.5;
			i++;
		}
		BigDecimal three = new BigDecimal (3);
		for (int j = 0; j < length; j++) {
			result = multiplyByPowerOfTwo(result, 1).add(m.divide(result.multiply(result), precs [i - j - 1] - (mag / 3), RoundingMode.HALF_UP));
			result = result.divide(three, precs [i - j - 1] - (mag / 3), RoundingMode.HALF_UP);
		}
		return result;
	}
	
	/**
	 * Calculates the area hyperbolic tangent (inverse function of the hyperbolic tangent) by
	 * defining it as ln((1+x) / (1-x)) * 0.5. So, the number has to be in the interval of -1
	 * and 1 (excluding both numbers).
	 * @param number is the number of which the inverse hyperbolic tangent is to be calculated
	 * @param n is the number of digits of precision
	 * @return the area hyperbolic tangent to given precision
	 */
	protected static BigDecimal artanh (BigDecimal number, int n) {
		BigDecimal zaehler = BigDecimal.ONE.add(number);
		BigDecimal nenner = BigDecimal.ONE.subtract(number);
		BigDecimal ln = ln(zaehler.divide(nenner, new MathContext (n)), n);
		return ln.multiply(HALF);
	}
	
	/**
	 * Calculates the area hyperbolic sine (inverse function of the hyperbolic sine) by
	 * defining it as ln(x + sqrt(x^2+1)).
	 * @param number is the number of which the inverse hyperbolic sine is to be calculated
	 * @param n is the number of digits of precision
	 * @return the area hyperbolic sine to given precision
	 */
	protected static BigDecimal arsinh (BigDecimal number, int n) {
		BigDecimal root = sqrt(number.multiply(number).add(BigDecimal.ONE), n);
		BigDecimal arg = root.add(number);
		return ln(arg, n);
	}
	
	/**
	 * Calculates the area hyperbolic cosine (inverse function of the hyperbolic tangent) by
	 * defining it as ln(x + sqrt(x^2-1)). So, the number has to be larger than, or equal
	 * to 1.
	 * @param number is the number of which the inverse hyperbolic cosine is to be calculated
	 * @param n is the number of digits of precision
	 * @return the area hyperbolic cosine to given precision
	 */
	protected static BigDecimal arcosh (BigDecimal number, int n) {
		BigDecimal root = sqrt(number.multiply(number).subtract(BigDecimal.ONE), n);
		BigDecimal arg = root.add(number);
		return ln(arg, n);
	}
	
	/**
	 * This method manipulates the argument in order to enhance the performance of the
	 * calculation of the arctangent in radiants.
	 * @param a is the number the arctangents is to be calculated of 
	 * @param n0 is number of digits to which the arctangent is to be calculated
	 * @return the arctangent of a to given precision
	 */
	protected static BigDecimal atana (BigDecimal a, int n0) {
		BigDecimal meins = BigDecimal.ONE.negate();
		if (a.compareTo(BigDecimal.ONE) < 0) {
			if (a.compareTo(meins) > 0) {
				 return arctanEuler(a, n0);
			} else {
				a = BigDecimal.ONE.divide(a, new MathContext (n0));
				BigDecimal e = arctanEuler(a, n0 + 1);
				BigDecimal p = pi(BigDecimal.valueOf(-5, 1), n0);
				p = p.subtract(e);
				return p;
			}
		} else {
			a = BigDecimal.ONE.divide(a, new MathContext (n0));
			BigDecimal e = arctanEuler(a, n0);
			BigDecimal pi = pi(HALF, n0);
			e = pi.subtract(e);
			return e;
		}
	}
	
	/**
	 * Calculates the arctangent of a number in either radiants, degrees or gons. This
	 * method uses the atana method (and the arctanEuler method) to calculate the arctangent. 
	 * @param n is number of which the arctangent is to be calculated
	 * @param o determines whether the arctangent should be in radiants, degrees or gons.
	 * o can be either "RAD", "DEG" or "GRAD" (ignoring the case).
	 * @param n0 is the number of digits of precision
	 * @return the arctangent of n to given precision and format (rad, deg or grad)
	 */
	protected static BigDecimal atanda (BigDecimal n, String o, int n0) {
		n = atana(n, n0 + 1);
		if (o.equalsIgnoreCase("deg")) {
			BigDecimal d = pi(BigDecimal.ONE, n0);
			BigDecimal b = new BigDecimal (180);
			n = n.multiply(b);
			n = n.divide(d, new MathContext (n0));
			return n;
		} else if (o.equalsIgnoreCase("rad")) {
			return n.round(new MathContext (n0));
		} else {
			BigDecimal d = pi(BigDecimal.ONE, n0);
			BigDecimal b = new BigDecimal (200);
			n = n.multiply(b);
			n = n.divide(d, new MathContext (n0));
			return n;
		}
	}
	
	/**
	 * Calculates the arcsine of a number in either radiants, degrees or gons. The actual
	 * calculation is done in the arctanEuler method.
	 * @param n1 is number of which the arcsine is to be calculated
	 * @param o determines whether the arcsine should be in radiants, degrees or gons.
	 * o can be either "RAD", "DEG" or "GRAD" (ignoring the case).
	 * @param n0 is the number of digits of precision
	 * @return the arcsine of n to given precision and format (rad, deg or grad)
	 */
	protected static BigDecimal asinda (BigDecimal n1, String r, int n0) {
		BigDecimal o1 = n1.multiply(n1);
		BigDecimal p = n1;
		BigDecimal q = n1;
		p = BigDecimal.ONE.subtract(o1);
		BigDecimal p1 = sqrt(p, n0 + 1);
		p1 = q.divide(p1, new MathContext (n0 + 1));
		BigDecimal result = p1;
		result = atanda(result, r, n0);
		return result;
	}
	
	/**
	 * Calculates the arccosine of a number in either radiants, degrees or gons. The actual
	 * calculation is done in the arctanEuler method.
	 * @param n1 is number of which the arccosine is to be calculated
	 * @param o determines whether the arccosine should be in radiants, degrees or gons.
	 * o can be either "RAD", "DEG" or "GRAD" (ignoring the case).
	 * @param n0 is the number of digits of precision
	 * @return the arccosine of n to given precision and format (rad, deg or grad)
	 */
	protected static BigDecimal acosda (BigDecimal m, String r, int n0) {
		if (m.signum() > 0) {
			BigDecimal o1 = m.multiply(m);
			BigDecimal p = m;
			BigDecimal q = m;
			p = BigDecimal.ONE.subtract(o1);
			p = sqrt(p, n0);
			p = p.divide(q, new MathContext (n0));
			return atanda(p, r, n0);
		} else {
			m = m.negate();
			BigDecimal o1 = m.multiply(m);
			BigDecimal p = m;
			BigDecimal q = m;
			p = BigDecimal.ONE.subtract(o1);
			p = sqrt(p, n0);
			p = p.divide(q, new MathContext (n0));
			BigDecimal e = atanda(p, r, n0);
			BigDecimal ha = new BigDecimal (180);
			if (r.equals("grad")) {
				ha = new BigDecimal (200);
			} else if (r.equals("rad")) {
				ha = pi(BigDecimal.ONE, n0);
			}
			e = ha.subtract(e);
			return e;
		}
	}
	
	/**
	 * Calculates the cumulative distribution function
	 * (https://en.wikipedia.org/wiki/Cumulative_distribution_function).
	 * @param z is the number of which the distribution function (cdf) is calculated
	 * @param n is the number of digits to which the function is calculated
	 * @param add determines whether the integral of the probability density function
	 * @return the distribution function of z to given precision
	 */
	protected static BigDecimal cdf (BigDecimal z, int n, boolean add) {
		BigDecimal result = z;
		BigDecimal i = BigDecimal.ONE;
		BigDecimal k = BigDecimal.ONE;
		BigDecimal two = new BigDecimal (2);
		BigDecimal term = z;
		BigDecimal zsqr = z.multiply(z);
		int mag = digitsm1Estimate(z);
		int prec = n;
		if (mag < 0) {
			prec -= mag;
		}
		while (term.signum() != 0) {
			term = term.multiply(k);
			k = multiplyByPowerOfTwo(i, 1).add(BigDecimal.ONE);
			term = term.divide(two.multiply(i).multiply(k), prec, RoundingMode.HALF_UP);
			term = term.multiply(zsqr);
			term = term.negate();
			i = i.add(BigDecimal.ONE);
			result = result.add(term);
		}
		result = result.divide(sqrt(pi(new BigDecimal (2), n), n), new MathContext (n));
		if (add) {
			result = result.add(HALF);
		}
		return result.round(new MathContext (n));
	}
	
	/**
	 * Transforms the argument in the form of my, sigma and x into z for the Error Function.
	 * @param x the value, the probability of it being undermined is calculated
	 * @param my is the expected value
	 * @param sigma is the standard deviation
	 * @param n is the number of digits z has to be calculated to
	 * @return the z value of the transformed values of x, my and sigma
	 */
	protected static BigDecimal zTransformation (BigDecimal x, BigDecimal my, BigDecimal sigma, int n) {
		return x.subtract(my).divide(sigma, n, RoundingMode.HALF_UP);
	}
	
	/**
	 * Calculates the binomial coefficient of two (integer) numbers.
	 * @param l is the first number (that larger one)
	 * @param m is the second number (the smaller one)
	 * @param n0 is the number of digits of precision
	 * @return the binomial coefficient of l and m to given precision
	 */
	protected static BigDecimal binomialCoefficient (BigDecimal l, BigDecimal m, int n0) {
		int n = l.intValue();
		int k = m.intValue();
		if (n == k || k == 0) {
			return BigDecimal.ONE;
		} else if (k == 1 || n == k + 1) {
			return l;
		}
		k = k <= n - k ? k : n - k;
		m = new BigDecimal (k);
		BigDecimal thismarg = l.subtract(m);
		BigDecimal result = l;
		for (int i = thismarg.intValue() + 1; i < l.intValue(); i++) {
			result = result.multiply(new BigDecimal (i), new MathContext (n0));
		}
		return result.divide(factorial(m), new MathContext (n0));
	}
	
	/**
	 * Calculates the binomial coefficient of two (integer) numbers.
	 * @param l is the first number (that larger one)
	 * @param m is the second number (the smaller one)
	 * @return the binomial coefficient of l and m to infinite precision
	 */
	protected static BigDecimal binomialCoefficient (BigDecimal l, BigDecimal m) {
		int n = l.intValue();
		int k = m.intValue();
		if (n == k || k == 0) {
			return BigDecimal.ONE;
		} else if (k == 1 || n == k + 1) {
			return l;
		}
		k = k <= n - k ? k : n - k;
		m = new BigDecimal (k);
		BigDecimal thismarg = l.subtract(m);
		BigDecimal result = l;
		for (int i = thismarg.intValue() + 1; i < l.intValue(); i++) {
			result = result.multiply(new BigDecimal (i));
		}
		return result.divide(factorial(m));
	}
	
	/**
	 * Calculates the lemniscate constant which can be calculated with pi/agm(1, sqrt(2)).
	 * @param n is the number of digits the lemniscate contant is to be calculated
	 * @return the lemniscate constant to given precision
	 */
	protected static BigDecimal lemniscate (int n) {
		BigDecimal sqrt2 = sqrt(new BigDecimal (2), n);
		BigDecimal pi2 = pi(BigDecimal.ONE, n);
		BigDecimal l = pi2.divide(agm(BigDecimal.ONE, sqrt2, n), new MathContext (n));
		return l;
	}
	
	/**
	 * Calculates the arctangent of a number in radiants. This method uses another one
	 * (zAtanManipulator) twice to speed up the calculation process of the series.
	 * @param z is the number of which the arctangent is to be calculated
	 * @param n is the number of digits the arctangent of z is to be calculated
	 * @return the arctangent of z to given precision
	 */
	protected static BigDecimal arctanEuler (BigDecimal z, int n) {
		if (z.signum() == 0) {
			return BigDecimal.ZERO;
		}
		z = zAtanManipulator(z, n);
		z = zAtanManipulator(z, n);
		BigDecimal result = BigDecimal.ZERO;
		z = BigDecimal.ONE.divide(z, new MathContext (n));
		BigDecimal zsqrp1 = BigDecimal.ONE.add(z.pow(2));
		BigDecimal term = BigDecimal.ONE;
		BigDecimal i = BigDecimal.ZERO;
		BigDecimal isqr = BigDecimal.ZERO;
		BigDecimal two = new BigDecimal (2);
		BigDecimal makesqr = BigDecimal.ONE;
		BigDecimal j = two;
		int mag = digitsm1Estimate(z);
		while (term.signum() != 0) {
			result = result.add(term);
			i = i.add(BigDecimal.ONE);
			term = multiplyByPowerOfTwo(term, 2);
			isqr = isqr.add(makesqr);
			term = term.multiply(isqr);
			makesqr = makesqr.add(two);
			term = term.divide(j.multiply(j.add(BigDecimal.ONE).multiply(zsqrp1)), n + mag, RoundingMode.HALF_UP);
			j = j.add(two);
		}
		result = result.multiply(multiplyByPowerOfTwo(z, 2));
		result = result.divide(zsqrp1, new MathContext (n));
		return result;
	}
	
	/**
	 * Manipulates the argument to make the calculation of the series for the arctangent faster.
	 * @param z is the argument that is to be made smaller
	 * @param n is the number of digits the manipulated argument is to be calculated
	 * @return the changed argument
	 */
	private static BigDecimal zAtanManipulator (BigDecimal z, int n) {
		BigDecimal sqrt1pzsqr = sqrt(z.multiply(z).add(BigDecimal.ONE), n);
		return z.divide(BigDecimal.ONE.add(sqrt1pzsqr), new MathContext (n));
	}
	
	/**
	 * Calculates the natural logarithm of a positive number.
	 * This is the fastest algorithm in this library to calculate the natural logarithm of
	 * a BigDecimal. It inverses the exponential function with the Newton method.
	 * @param z is the number the natural logarithm is to be calculated of
	 * @param n is the number of digits ln(x) is to be calculated to
 	 * @return the natural logarithm of z to given precision
	 */
	protected static BigDecimal lnNewton (BigDecimal z, int n) {
		BigDecimal lnApprox = doubleln(z);
		return lnNewton(z, lnApprox, 15, n);
	}
	
	protected static BigDecimal lnNewton (BigDecimal z, BigDecimal approx, int n1, int n2) {
		boolean is10 = z.compareTo(BigDecimal.TEN) == 0;
		boolean is2 = !is10 && z.compareTo(new BigDecimal (2)) == 0;
		if (is10 && ln10CachedMaxDigits >= n2) {
			return ln10Cached.round(new MathContext (n2));
		} else if (is2 && n2 <= ln2CachedMaxDigits) {
			return ln2Cached.round(new MathContext (n2));
		}
		
		int length = (int)(StrictMath.log((double)(n2) / n1) / StrictMath.log(3)) + 1;
		int [] precs = new int [length];
		double n0 = n2;
		int i = 0;
		while (i < length) {
			precs [i] = (int)(n0) + 6;
			n0 /= 3;
			i++;
		}
		BigDecimal e = eFaster(n2 + 6);
		BigDecimal ode = BigDecimal.ONE.divide(e, new MathContext (n2));
		BigDecimal result = approx;
		BigDecimal exp = BigDecimal.ONE;
		for (int j = 0; j < i; j++) {
			exp = expFaster(result, e, ode, precs [i - j - 1]);
			result = result.add(multiplyByPowerOfTwo((z.subtract(exp)).divide(z.add(exp), new MathContext ((int)(precs [i - j - 1] / 1.5))), 1));
		}
		
		if (is10) {
			ln10Cached = result;
			ln10CachedMaxDigits = n2;
		} else if (is2) {
			ln2Cached = result;
			ln2CachedMaxDigits = n2;
		}
		
		return result;
	}
	
	/**
	 * Calculates the common logarithm (base 10 logarithm) of a number. If the number is an
	 * integer power of 10, the method returns the magnitude (precision - scale - 1) of the
	 * argument. If not, the natural logarithm of the number is calculated and divided by ln10.
	 * @param z is the number of which the common logarithm is calculated
	 * @param n is the number of digits the common logarithm of z is calculated to
	 * @return the common logarithm of z to given precision
	 */
	protected static BigDecimal lg (BigDecimal z, int n) {
		int mag = digitsm1(z);
		if (z.scaleByPowerOfTen(-mag).compareTo(BigDecimal.ONE) == 0) {
			return new BigDecimal (mag);
		} else {
			BigDecimal ln10 = ln(BigDecimal.TEN, n);
			return ln(z, n).divide(ln10, new MathContext (n));
		}
	}
	
	/**
	 * Calculates the inverse function of the cumulative distribution function to 16 digits
	 * using Newton's method. This method is used by the invcdf method as an initial
	 * guess.
	 * @param z is the number of which the inverse cdf is to be calculated
	 * @param fromNegativeInfinity determines whether the distribution function is measured from
	 * negative infinity (true) or from zero (false)
	 * @return the inverse cumulative distribution of z to given precision
	 */
	private static BigDecimal invcdf16 (BigDecimal z, boolean fromNegativeInfinity) {
	//	BigDecimal approx = HALF;
		BigDecimal approx = probitRough(z);
		BigDecimal result = approx;
		BigDecimal result2 = BigDecimal.ZERO;
		BigDecimal z1 = BigDecimal.ZERO;
		BigDecimal z2 = z1;
		BigDecimal e = eFaster(20);
		BigDecimal ode = BigDecimal.ONE.divide(e, new MathContext (20));
		BigDecimal pi2 = pi(new BigDecimal (2), 20);
		boolean b = false;
		boolean a = false;
		int i = 0;
		while (b == false) {
			z1 = cdf(result, 20, fromNegativeInfinity).subtract(z);
			z2 = pi2.multiply(expFaster(result.multiply(result), e, ode, 20));
			z2 = sqrt(z2, 20);
			result = result.subtract(z1.multiply(z2));
			if (a || digitsm1Estimate(result) == digitsm1Estimate(result2)) {
				a = true;
			}
			if (a) {
				i++;
			}
			if (a && i >= 10) {
				b = true;
			}
			result2 = result;
		}
		return result;
	}
	
	/**
	 * Calculates the inverse of the cdf of a number. This method uses Newton's
	 * method to approximate it from the actual cumulative distribution function. The initial
	 * guess is calculated by the invcdf16 method up to 16 digits.
	 * @param z is the number of which the inverse cumulative distribution function is
	 * calculated
	 * @param n0 is the number of digits of precision
	 * @param fromNegativeInfinity determines whether the distribution function is computed from
	 * negative infinity (true) or 0 (false)
	 * @return the inverse cumulative distribution function of z to given precision
	 */
	protected static BigDecimal invcdf (BigDecimal z, int n0, boolean fromNegativeInfinity) {
		if ((z.signum() == 0 && fromNegativeInfinity == false) || (z.compareTo(HALF) == 0 && fromNegativeInfinity)) {
			return BigDecimal.ZERO;
		} else {
			BigDecimal result = invcdf16(z, fromNegativeInfinity);
			BigDecimal z1 = BigDecimal.ZERO;
			BigDecimal z2 = z1;
			BigDecimal e = eFaster(n0 + 2);
			BigDecimal ode = BigDecimal.ONE.divide(e, new MathContext (n0));
			BigDecimal pi2 = pi(new BigDecimal (2), n0 + 2);
			double n2 = n0;
			int i = 0;
			int length = (int)(StrictMath.log(n2) / StrictMath.log(2)) - 2;
			int [] precs = new int [length];
			while (i < length) {
				precs [i] = (int)(n2) + 2;
				n2 /= 2;
				i++;
			}
			for (int j = 0; j < i; j++) {
				z1 = cdf(result, precs [i - j - 1], fromNegativeInfinity).subtract(z);
				z2 = pi2.multiply(expFaster(result.multiply(result), e, ode, precs [i - j - 1]));
				z2 = sqrt(z2, precs [i - j - 1]);
				result = result.subtract(z1.multiply(z2));
			}
			return result.round(new MathContext (n0));
		}
	}
	
	/**
	 * Calculates the square root of a non-negative number by inverting the square function
	 * via Newton's method. The method actually doing this process is sqrtApprox. This one only
	 * calls sqrtApprox with the number of which the square root is to be calculated, an initial
	 * guess that has about 15 correct digits (doublesqrt method).
	 * @param z is the number of which the square root is to be calculated
	 * @param n is the number of digits of precision
	 * @return the square root of z to given precision
	 */
	protected static BigDecimal sqrt (BigDecimal z, int n) {
		return sqrtApprox(z, doublesqrt(z), 15, n);
	}

	protected static BigDecimal multiplyByPowerOfTwo (BigDecimal z, int p) {
		return new BigDecimal (z.unscaledValue().shiftLeft(p), z.scale());
	}
	
	/**
	 * Calculates the square root of a number using Newton's method to invert the square
	 * function. It can detect square numbers during the iteration process to then make the
	 * computation a lot faster.
	 * @param z is the number of which the square root is to be calculated
	 * @param approx is the first approximation of the square root
	 * @param n1 is the number of digits to which approx accurately represents the square root
	 * of z
	 * @param n2 is the number of digits to which the square root is to be calculated
	 * @return the square root of z to given precision
	 */
	private static BigDecimal sqrtApprox (BigDecimal z, BigDecimal approx, int n1, int n2) {
		if (z.signum() == 0) {
			return BigDecimal.ZERO;
		}
		
		int mag = digitsm1Estimate(z) - 1;
		
		int length = (int)((StrictMath.log((double)(n2) / (double)(n1))) / StrictMath.log(2.0));
		
		if ((n2 >> length) > n1) {
			length++;
		}
		
		int middlePrec = n2 >> (length >> 1);
		
		BigDecimal zRounded = precisionEstimate(z) > middlePrec ? z.round(new MathContext (middlePrec)) : z;
		
		BigDecimal z1 = approx;
		int j = 0;
		for (; j < (length >> 1); j++) {
			if ((n2 >> (length - j - 1)) >= middlePrec) {
				break;
			}
			z1 = multiplyByPowerOfTwo(z1.add(zRounded.divide(z1, (n2 >> (length - j - 1)) + 2 - (mag >> 1), RoundingMode.HALF_UP)), -1);
		}
		
		for (; j < length; j++) {
			z1 = multiplyByPowerOfTwo(z1.add(z.divide(z1, (n2 >> (length - j - 1)) + 2 - (mag >> 1), RoundingMode.HALF_UP)), -1);
		}
		
		return z1;
	}
	
	protected static int precisionEstimate (BigDecimal z) {
		int bits = z.unscaledValue().bitLength();
		return (int)(StrictMath.round(bits * lg2));
	}
	
	/**
	 * Returns the magnitude of a number (the exponent of the scientific notation of a number)
	 * @param z is the number of which the magnitude is to be returned
	 * @return the magnitude of z
	 */
	protected static int digitsm1 (BigDecimal z) {
		return z.precision() - z.scale() - 1;
	}
	
	private static int digitsm1Estimate (BigDecimal z) {
		return precisionEstimate(z) - z.scale() - 1;
	}
	
	/**
	 * These are the limit values of double. Actually, DOUBLEMINVALUE is the value with the
	 * smallest magnitude where a double number can still represent the value to its fullest
	 * precision.
	 */
	private static final BigDecimal DOUBLEMAXVALUE = new BigDecimal (Double.MAX_VALUE);
	private static final BigDecimal DOUBLEMINVALUE = new BigDecimal (Double.MIN_NORMAL);
	
	/**
	 * This method is used to calculate the common logarithm of "any" positive real number
	 * (even ones that are out of the range of double). It is called when the requested
	 * precision is not greater than 15 and the argument does not have more than 15 significant
	 * digits.
	 * @param z is the number of which the common logarithm is to be calculated
	 * @return the common logarithm of z to the precision of double (usually between 15 and 17
	 * significant digits)
	 */
	protected static BigDecimal doublelg (BigDecimal z) {
		BigDecimal z1 = z.subtract(BigDecimal.ONE);
		if (z1.signum() > 0 && z1.compareTo(DOUBLEMINVALUE) < 0) {
			return z1.multiply(new BigDecimal (StrictMath.log10(StrictMath.E)));
		}

		int power = digitsm1(z);
		power = power > 300 || power < -300 ? power : 0;
		z = z.scaleByPowerOfTen(-power);
		
		if (digitsm1Estimate(z) < -1) {
			return BigDecimal.valueOf(StrictMath.log(z.doubleValue()) / ln10 + (double)(power));
		}
		
		z1 = z.subtract(BigDecimal.ONE);
		return BigDecimal.valueOf(StrictMath.log1p(z1.doubleValue()) / ln10 + (double)(power));
	}
	
	/**
	 * This method is used to calculate the natural logarithm of "any" positive real number. If
	 * the number can be represented in a double, the log method in the StrictMath library is
	 * called. If not, the common logarithm (calculated by the doublelg method) is multiplied by
	 * the natural logarithm of 10. This is because it is easier to manipulate the common
	 * logarithm function for extreme arguments than the natural logarithm.
	 * @param z is the number of which the natural logarithm is to be calculated
	 * @return the natural logarithm of z to 15 - 17 digits of precision
	 */
	protected static BigDecimal doubleln (BigDecimal z) {
		if (z.compareTo(DOUBLEMAXVALUE) > 0 || z.compareTo(DOUBLEMINVALUE) < 0) {
			return doublelg(z).multiply(new BigDecimal (ln10));
		} else {
			BigDecimal z1 = z.subtract(BigDecimal.ONE);
			if (z1.signum() > 0 && z1.compareTo(DOUBLEMINVALUE) < 0) {
				return z1;
			}
			if (digitsm1Estimate(z1) > -2) {
				return BigDecimal.valueOf(StrictMath.log1p(z1.doubleValue()));
			} else {
				return BigDecimal.valueOf(StrictMath.log(z.doubleValue()));
			}
		}
	}
	
	/**
	 * Calculates the square root of a number using StrictMath to apply Newton's Method. This
	 * method only manipulates the argument to be representable by a double.
	 * @param z is the number of which the square root is to be calculated
	 * @return the square root of z to the maximum precision of double
	 */
	protected static BigDecimal doublesqrt (BigDecimal z) {
		int power = digitsm1Estimate(z);
		boolean scale = power > 300 || power < -300;
		if (scale) {
			if (power % 2 != 0) {
				power--;
			}
			z = z.scaleByPowerOfTen(-power);
		}
		double result = StrictMath.sqrt(z.doubleValue());
		BigDecimal r = BigDecimal.valueOf(result);
		r = scale ? r.scaleByPowerOfTen(power / 2) : r;
		return r;
	}
	
	/**
	 * Calculates the cube root of a number using StrictMath to apply Newton's Method. This
	 * method only manipulates the argument to be representable by a double.
	 * @param z is the number of which the cube root is to be calculated
	 * @return the cube root of z to the maximum precision of double
	 */
	protected static BigDecimal doublecbrt (BigDecimal z) {
		int power = digitsm1Estimate(z);
		boolean scale = power > 300 || power < -300;
		if (scale) {
			while (power % 3 != 0) {
				power--;
			}
			z = z.scaleByPowerOfTen(-power);
		}
		BigDecimal result = BigDecimal.valueOf(StrictMath.cbrt(z.doubleValue()));
		result = scale ? result.scaleByPowerOfTen(power / 3) : result;
		return result;
	}
	
	/**
	 * Is the precalculated common logarithm of 2 computed with the StrictMath library.
	 */
	protected static final double lg2 = StrictMath.log10(2);
	
	/**
	 * Calculates 10^z with the StrictMath library. If the argument cannot be directly
	 * represented by a double, it is manipulated to fit into a double.
	 * @param z is the argument
	 * @return 10^z to 15 - 17 digits
	 */
	protected static BigDecimal doublepowten (BigDecimal z) {
		BigInteger power = BigInteger.ZERO;
		if (z.compareTo(new BigDecimal (300)) >= 0 || z.compareTo(new BigDecimal (300)) <= 0) {
			power = z.toBigInteger();
		}
		BigDecimal z1 = z.subtract(new BigDecimal (power));
		BigDecimal result = BigDecimal.valueOf(StrictMath.pow(10, z1.doubleValue()));
		return result.scaleByPowerOfTen(power.intValueExact());
	}
	
	/**
	 * Calculates exp(z) (=e^z) where e is Euler's Number (=2.71828...) using the doublepowten
	 * method. The argument is divided by the natural logarithm of 10 to be able to calculate
	 * exp(z) as 10^(z / ln(10)) with the previously mentioned method.
	 * @param z is the number to whose power Euler's Number is risen
	 * @return exp(z) to the maximum precision of double
	 */
	protected static BigDecimal doubleexp (BigDecimal z) {
		z = BigDecimal.valueOf(z.doubleValue() / ln10);
		return doublepowten(z);
	}
	
	/**
	 * Calculates z^e using the doublepowten and doublelg methods in order to provide this
	 * function not only for values which are representable by doubles.
	 * @param z is the base
	 * @param e is the exponent
	 * @return z^e to 15 - 17 digits of precision
	 */
	protected static BigDecimal doublepower (BigDecimal z, BigDecimal e) {
		return doublepowten(doublelg(z).multiply(e));
	}
	
	/**
	 * Calculates the inverse nth root (=reciprocal value of the nth root) using Newton's
	 * method. It checks for roots with a finite decimal expansion and in this case returns the
	 * result before the loop is supposed to finish. The approximated value of the root is
	 * inserted as an argument (by the invroot method).
	 * The reason for calculating the reciprocal value instead of the root itself, is the
	 * convergence that stays the same for all degrees and doesn't decrease when the order goes
	 * up.
	 * @param z is the of which the nth root is to be calculated
	 * @param approx is an approximation of the nth root which is enhanced during the iteration
	 * @param order is the order of the root
	 * @param n1 is the number of exact digits of approx
	 * @param n2 is the number of digits to which the nth root is to be calculated
	 * @return the inverse root of z (z^(-1/order)) to given precision
	 */
	private static BigDecimal invrootApprox (BigDecimal z, BigDecimal approx, int order, int n1, int n2) {
		if (n1 >= n2) {
			return approx.round(new MathContext (n2));
		}
		if (approx.pow(order).multiply(z).compareTo(BigDecimal.ONE) == 0) {
			return approx.round(new MathContext (n2));
		}
		int length = (int)((StrictMath.log((double)(n2) / n1)) / StrictMath.log(2)) + 1;
		int [] precs = new int [length];
		double n = n2;
		int i = 0;
		while (i < length) {
			precs [i] = (int)(n) + 2;
			n /= 2;
			i++;
		}
		BigDecimal d = new BigDecimal (order);
		BigDecimal dp1 = new BigDecimal (order + 1);
		int precision = z.precision();
		int mag = precision - z.scale() - 1;
		boolean b = true;
		for (int j = 0; j < i; j++) {
			approx = approx.multiply(dp1).subtract(z.multiply(approx.pow(order + 1))).divide(d, precs [i - j - 1] + mag + 1, RoundingMode.HALF_UP);
			if (b && precs [i - j - 1] / order > precision) {
				if (approx.round(new MathContext (precs [i - j - 1])).pow(order).multiply(z).round(new MathContext (precs [i - j - 1])).compareTo(BigDecimal.ONE) == 0) {
					if (approx.pow(order).multiply(z).compareTo(BigDecimal.ONE) == 0) {
						return approx;
					}
				} else {
					b = false;
				}
			}
		}
		return approx;
	}
	
	/**
	 * Calculates the inverse nth root (=reciprocal value of the nth root) using Newton's
	 * method. It checks for roots with a finite decimal expansion and in this case returns the
	 * result before the loop is supposed to finish. The approximated value of the root is
	 * calculated via the doublepower method (z^(-1/order)).
	 * The reason for calculating the reciprocal value instead of the root itself, is the
	 * convergence that stays the same for all degrees and doesn't decrease when the order goes
	 * up.
	 * @param z is the of which the nth root is to be calculated
	 * @param order is the order of the root
	 * @param n is the number of digits to which the nth root is to be calculated
	 * @return the inverse root of z (z^(-1/order)) to given precision
	 */
	protected static BigDecimal invroot (BigDecimal z, int order, int n) {
		return invrootApprox(z, doublepower(z, BigDecimal.ONE.divide(new BigDecimal (-order), new MathContext (16))), order, 15, n);
	}
	
	/**
	 * Calculates the (Gaussian) Error Function using the cumulative distribution function (cdf
	 * method).
	 * @param z is the number of which the erf is to be calculated
	 * @param n is the number of digits to which erf(z) is to be calculated
	 * @return erf(z) to given precision
	 */
	protected static BigDecimal erf (BigDecimal z, int n) {
		BigDecimal two = new BigDecimal (2);
		return two.multiply(cdf(z.multiply(sqrt(two, n)), n, true)).subtract(BigDecimal.ONE);
	}
	
	/**
	 * Estimates the inverse gaussian error function to about three digits. It is used in the
	 * erfinv method.
	 * @param z is the number of which the inverse erf is to be calculated
	 * @return erfinv(z) to 10 digits (not all of them might be correct)
	 */
	private static BigDecimal erfinvRough (BigDecimal z) {
		if (z.signum() == 0) {
			return BigDecimal.ZERO;
		} else if (z.signum() > 0) {
			int prec = 10;
			BigDecimal a = BigDecimal.valueOf(147, 3);
			BigDecimal ln = ln(BigDecimal.ONE.subtract(z.multiply(z)), prec);
			BigDecimal sqrtInside = new BigDecimal (2).divide(pi(a, prec), new MathContext (prec));
			sqrtInside = sqrtInside.add(ln.multiply(HALF));
			BigDecimal fs = sqrt(sqrtInside.multiply(sqrtInside).subtract(ln.divide(a, new MathContext (prec))), prec);
			return sqrt(fs.subtract(sqrtInside), prec);
		} else {
			return erfinvRough(z.negate()).negate();
		}
	}
	
	/**
	 * Estimates the probit function (inverse of the cdf function) via the erfinvRough method.
	 * @param z is the number (probability) of which the probit function is to be calculated
	 * @return probit(z) to 10 digits (not all of them might be correct)
	 */
	private static BigDecimal probitRough (BigDecimal z) {
		BigDecimal two = new BigDecimal (2);
		return sqrt(two, 10).multiply(erfinvRough(two.multiply(z).subtract(BigDecimal.ONE)));
	}
	
	/**
	 * Calculates the inverse error function via the invcdf method.
	 * @param z is the number of which the erfinv function is to be calculated
	 * @param n is the number of digits to which the erfinv function is to be calculated
	 * @return erfinv(z) to given precision
	 */
	protected static BigDecimal erfinv (BigDecimal z, int n) {
		return invcdf(z.add(BigDecimal.ONE).multiply(HALF), n, true).multiply(sqrt(HALF, n));
	}

	protected static BigDecimal eulerMascheroniConstant (int n0) {
		if (n0 <= emCachedMaxDigits) {
			return emCached.round(new MathContext (n0));
		}
		
		int n = (int)(StrictMath.log((double)(n0) * StrictMath.log(10.0)) / StrictMath.log(2.0)) + 1;
		
		BigDecimal coeff = multiplyByPowerOfTwo(BigDecimal.ONE, n);
		coeff = coeff.multiply(exp(coeff.negate(), n0));
		
		BigDecimal sum = BigDecimal.ONE;
		BigDecimal sum2 = BigDecimal.ONE;
		
		int m = 1;
		
		BigInteger twoPow = BigInteger.ONE;
		BigInteger factorial = BigInteger.ONE;
		
		BigDecimal innerSum = BigDecimal.ONE;
		
		do {
			sum2 = sum;
			twoPow = twoPow.shiftLeft(n);
			factorial = factorial.multiply(BigInteger.valueOf(m + 1));
			innerSum = innerSum.add(BigDecimal.ONE.divide(BigDecimal.ONE.add(new BigDecimal (m)), n0, RoundingMode.HALF_UP));
			sum = sum.add(new BigDecimal (twoPow).divide(new BigDecimal (factorial), n0, RoundingMode.HALF_UP).multiply(innerSum), new MathContext (n0));
			m++;
		} while (sum.compareTo(sum2) != 0);
		
		emCached = coeff.multiply(sum).subtract(new BigDecimal (n).multiply(ln2(n0)));
		emCachedMaxDigits = n0;
		return emCached;
	}

	protected static BigInteger subFactorial (int n) {
		if (n == 1) {
			return BigInteger.ZERO;
		}
		
		BigInteger sum = BigInteger.ONE;
		int k = -n;
		BigInteger factor = BigInteger.ONE;
		
		for (int j = 0; j < n - 2; j++) {
			factor = factor.multiply(BigInteger.valueOf(k));
			k++;
			sum = sum.add(factor);
		}
		
		return sum.abs();
	}

	protected static BigDecimal arcsinNewton (BigDecimal z, int n) {
		int length = (int)((StrictMath.log(n / 15.0)) / StrictMath.log(3)) + 1;
		int [] precs = new int [length];
		double n1 = n;
		int i = 1;
		int inc = 6;
		precs [0] = n;
		n1 /= 3;
		while (i < length) {
			precs [i] = (int)(n1) + inc;
			n1 /= 3;
			i++;
		}
		
		BigDecimal pi = pi(BigDecimal.ONE, n);
		
		BigDecimal approx = digitsm1Estimate(z) > -20 ? new BigDecimal (StrictMath.asin(z.doubleValue())) : z;
		
		BigDecimal sin = BigDecimal.ONE;
		BigDecimal cos = BigDecimal.ONE;
		BigDecimal cossqr = BigDecimal.ONE;
		BigDecimal fraction = BigDecimal.ONE;
		
		for (int j = 0; j < i; j++) {
			sin = sinr(approx, pi.round(new MathContext (precs [i - j - 1])), precs [i - j - 1]);
			cossqr = BigDecimal.ONE.subtract(sin.multiply(sin).round(new MathContext (precs [i - j - 1])));
			cos = sqrt(cossqr, precs [i - j - 1]);
			fraction = sin.subtract(z).divide(cossqr.add(BigDecimal.ONE).subtract(sin.multiply(z)), new MathContext ((int)((double)(precs [i - j - 1]) / 1.5)));
			approx = approx.subtract(multiplyByPowerOfTwo(cos.multiply(fraction), 1)).round(new MathContext (precs [i - j - 1]));
		}
		
		return approx;
	}
	
	protected static BigDecimal arccosNewton (BigDecimal z, int n) {
		int length = (int)((StrictMath.log(n / 15.0)) / StrictMath.log(3)) + 1;
		int [] precs = new int [length];
		double n1 = n;
		int i = 1;
		int inc = 6;
		precs [0] = n;
		n1 /= 3;
		while (i < length) {
			precs [i] = (int)(n1) + inc;
			n1 /= 3;
			i++;
		}
		
		BigDecimal pi = pi(BigDecimal.ONE, n);
		
		BigDecimal approx = new BigDecimal (StrictMath.acos(z.doubleValue()));
		
		BigDecimal cos = BigDecimal.ONE;
		BigDecimal sin = BigDecimal.ONE;
		BigDecimal sinsqr = BigDecimal.ONE;
		BigDecimal fraction = BigDecimal.ONE;
		
		for (int j = 0; j < i; j++) {
			cos = cosr(approx, pi.round(new MathContext (precs [i - j - 1])), precs [i - j - 1]);
			sinsqr = BigDecimal.ONE.subtract(cos.multiply(cos).round(new MathContext (precs [i - j - 1])));
			sin = sqrt(sinsqr, precs [i - j - 1]);
			fraction = cos.subtract(z);
			fraction = fraction.divide(sinsqr.add(BigDecimal.ONE).subtract(z.multiply(cos)), new MathContext ((int)((double)(precs [i - j - 1]) / 1.5)));
			approx = approx.add(multiplyByPowerOfTwo(sin.multiply(fraction), 1)).round(new MathContext (precs [i - j - 1]));
		}
		
		return approx;
	}
	
	protected static BigDecimal arctanNewton (BigDecimal z, int n) {
		boolean inv = false;
		if (z.abs().compareTo(BigDecimal.ONE) >= 0) {
			z = BigDecimal.ONE.divide(z, new MathContext (n));
			inv = true;
		}
		
		int length = (int)((StrictMath.log(n / 15.0)) / StrictMath.log(3)) + 1;
		int [] precs = new int [length];
		double n1 = n;
		int i = 1;
		int inc = 6;
		precs [0] = n;
		n1 /= 3;
		while (i < length) {
			precs [i] = (int)(n1) + inc;
			n1 /= 3;
			i++;
		}
		
		BigDecimal pi = pi(BigDecimal.ONE, n);
		
		BigDecimal approx = digitsm1Estimate(z) > -20 ? new BigDecimal (StrictMath.atan(z.doubleValue())) : z;
		
		BigDecimal tan = BigDecimal.ONE;
		BigDecimal tansqrp1 = BigDecimal.ONE;
		BigDecimal ztansqrp1 = z;
		BigDecimal fraction = BigDecimal.ONE;
		
		for (int j = 0; j < i; j++) {
			tan = tanr(approx, pi.round(new MathContext (precs [i - j - 1])), precs [i - j - 1]);
			tansqrp1 = tan.multiply(tan).round(new MathContext (precs [i - j - 1])).add(BigDecimal.ONE);
			ztansqrp1 = tansqrp1.multiply(z);
			fraction = tan.multiply(tansqrp1).subtract(ztansqrp1);
			fraction = fraction.divide(tan.multiply(ztansqrp1).add(tansqrp1), new MathContext ((int)((double)(precs [i - j - 1]) / 1.5)));
			approx = approx.subtract(fraction).round(new MathContext (precs [i - j - 1]));
		}
		
		BigDecimal result = approx;
		
		if (inv) {
			result = z.signum() < 0 ? pi.multiply(BigDecimal.valueOf(-5, 1)).subtract(approx) : pi.multiply(HALF).subtract(approx);
		}
		
		return result;
	}
	
	protected static BigDecimal lnAGM (BigDecimal z, int n) {
		boolean is2 = z.compareTo(new BigDecimal (2)) == 0;
		boolean is10 = !is2 && z.compareTo(BigDecimal.TEN) == 0;
		if (is2) {
			return ln2(n);
		} else if (is10 && ln10CachedMaxDigits >= n) {
			return ln10Cached.round(new MathContext (n));
		}
		int n0 = (int)(n + StrictMath.log10(n));
		int p = (int)((double)(n0) / lg2);
		int m = (p >> 1) - (int)((double)(digitsm1Estimate(z)) / lg2) + 2;
		BigDecimal piHalf = pi(HALF, n0);
		BigDecimal halfPow = BigDecimal.ONE;
		if (m - 2 > 0) {
			halfPow = HALF.pow(m - 2);
		} else {
			halfPow = multiplyByPowerOfTwo(BigDecimal.ONE, 2 - m);
		}
		BigDecimal secondNumber = halfPow.divide(z, new MathContext (n0));
		BigDecimal agm = agm(BigDecimal.ONE, secondNumber, n0);
		BigDecimal ln2 = ln2(n0);
		BigDecimal result = piHalf.divide(agm, new MathContext (n0)).subtract(ln2.multiply(new BigDecimal (m)));
		
		if (is10) {
			ln10Cached = result;
			ln10CachedMaxDigits = n;
		}
		
		return result;
	}
	
	protected static BigDecimal ln (BigDecimal z, int n) {
		if (n <= 3000) {
			return lnNewton(z, n);
		} else {
			return lnAGM(z, n);
		}
	}
	
	private static BigDecimal ln2 (int n) {
		if (n <= ln2CachedMaxDigits) {
			return ln2Cached.round(new MathContext (n));
		}
		
		int n0 = (int)(n + StrictMath.log10(n));
		int p = (int)((double)(n0) / lg2);
		int m = (p >> 1) + 2;
		BigDecimal secondNumber = HALF.pow(m - 1);
		BigDecimal agm = agm(BigDecimal.ONE, secondNumber, n);
		BigDecimal ln2 = pi(BigDecimal.ONE, n).divide(new BigDecimal ((long)(m + 1) << 1).multiply(agm), new MathContext (n));
		ln2Cached = ln2;
		ln2CachedMaxDigits = n;
		return ln2Cached;
	}
	
	protected static BigDecimal expNewton (BigDecimal z, int n) {
		if (z.compareTo(BigDecimal.ONE) == 0) {
			return eFaster(n);
		} else if (z.compareTo(BigDecimal.ONE.negate()) == 0) {
			return em1Faster(n);
		}
		
		BigDecimal approx = doubleexp(z);
		int length = (int)((StrictMath.log(n / 15.0)) / StrictMath.log(3)) + 2;
		int [] precs = new int [length];
		double n1 = n;
		int i = 1;
		int inc = 6;
		precs [0] = n;
		n1 /= 3;
		while (i < length) {
			precs [i] = (int)(n1) + inc;
			n1 /= 3;
			i++;
		}
		
		pi(BigDecimal.ONE, n + 10);
		ln(new BigDecimal (2), n + 10);
		BigDecimal lnMz = BigDecimal.ZERO;
		BigDecimal two = new BigDecimal (2);
		for (int j = 0; j < i; j++) {
			lnMz = lnAGM(approx.round(new MathContext (precs [i - j - 1])), (int)((double)(precs [i - j - 1]) * 1.1)).subtract(z);
			approx = approx.subtract(multiplyByPowerOfTwo(approx.multiply(lnMz).divide(two.add(lnMz), new MathContext ((int)(precs [i - j - 1] / 1.5))), 1));
		}
		
		return approx;
	}
	
	protected static BigDecimal exp (BigDecimal z, int n) {
		if (n <= 3000) {
			return expFaster(z, n);
		} else {
			return expNewton(z, n);
		}
	}
	
	protected static BigDecimal lambertW (BigDecimal z, int n) {
		BigDecimal approx = BigDecimal.ZERO;
		int it = 15;
		if (z.signum() == 0) {
			return BigDecimal.ZERO;
		} else if (z.signum() > 0) {		
			approx = doubleln(z.add(BigDecimal.ONE));
			if (z.compareTo(new BigDecimal (2)) >= 0) {
				it = 7;
				BigDecimal lnz = doubleln(z);
				approx = lnz.subtract(doubleln(lnz)).add(doubleln(lnz).divide(lnz, new MathContext (16)));
			}
		} else {
			if (digitsm1Estimate(z) < -2) {
				it = 5;
				approx = z;
			} else {
				it = 20;
				approx = BigDecimal.valueOf(-5, 1);
			}
		}
		
		BigDecimal expW = doubleexp(approx);
		BigDecimal wExpW = approx.multiply(expW);
		
		for (int i = 0; i < it; i++) {
			approx = approx.subtract(wExpW.subtract(z).divide(expW.add(wExpW), new MathContext (8)));
			expW = doubleexp(approx);
			wExpW = approx.multiply(expW);
		}
		
		int length = (int)((StrictMath.log(n / 15.0)) / StrictMath.log(2)) + 1;
		int [] precs = new int [length];
		double n1 = n;
		int i = 1;
		int inc = 4;
		precs [0] = n;
		n1 /= 2;
		while (i < length) {
			precs [i] = (int)(n1) + inc;
			n1 /= 2;
			i++;
		}
		
		if (n > 3000) {
			pi(BigDecimal.ONE, n);
			ln(new BigDecimal (2), n);
		}
		
		for (int j = 0; j < i; j++) {
			expW = exp(approx, precs [i - j - 1]);
			wExpW = approx.multiply(expW);
			approx = approx.subtract(wExpW.subtract(z).divide(expW.add(wExpW), new MathContext (precs [i - j - 1] >> 1)));
		}
		
		return approx;
	}
	
	private static BigDecimal piGaussLegendre (BigDecimal m, int n) {
		if (n <= piCachedMaxDigits) {
			return piCached.multiply(m).round(new MathContext (n));
		}
		
		BigDecimal an = BigDecimal.ONE;
		BigDecimal bn = sqrt(HALF, n);
		BigDecimal tn = BigDecimal.valueOf(25, 2);
		BigDecimal anp1 = BigDecimal.ZERO;
		int i = 0;
		
		for (; i < 4; i++) {
			anp1 = multiplyByPowerOfTwo(an.add(bn), -1);
			bn = sqrt(bn.multiply(an).round(new MathContext (n)), n);
			tn = tn.subtract(multiplyByPowerOfTwo(an.subtract(anp1).pow(2), i));
			an = anp1;
		}
		
		int correctDigits = 15;
		
		while (correctDigits < n) {
			anp1 = multiplyByPowerOfTwo(an.add(bn), -1);
			bn = sqrtApprox(bn.multiply(an).round(new MathContext (n)), anp1, correctDigits, n);
			tn = tn.subtract(multiplyByPowerOfTwo(an.subtract(anp1).pow(2), i));
			an = anp1;
			correctDigits <<= 1;
			i++;
		}
		
		piCached = an.add(bn).pow(2).divide(multiplyByPowerOfTwo(tn, 2), n, RoundingMode.HALF_UP);
		piCachedMaxDigits = n;
		return piCached.multiply(m);
	}
	
	protected static BigDecimal pi (BigDecimal m, int n) {
		if (n < 8000) {
			return piFaster(m, n);
		} else {
			return piGaussLegendre(m, n);
		}
	}
	
	private static BigInteger get2pownthpowerof10(int n) {
		if (n == 0) {
			return BigInteger.ONE;
		} else if (n == 1) {
			return BigInteger.TEN;
		} else if (n - 2 < 30) {
			if (populatedpowOf10 >= n - 2) {
				return powsOf10[n - 2];
			} else {
				if (populatedpowOf10 == -1) {
					powsOf10[0] = BigInteger.valueOf(100);
					populatedpowOf10 = 0;
				}
				for (int i = populatedpowOf10 + 1; i <= n - 2; i++) {
					powsOf10[i] = powsOf10[i - 1].multiply(powsOf10[i - 1]);
				}
				populatedpowOf10 = n - 2;
				return powsOf10[n - 2];
			}
		} else {
			BigInteger pow = get2pownthpowerof10(powsOf10.length + 1);
			for (int i = 0; i < n - powsOf10.length + 1; i++) {
				pow = pow.multiply(pow);
			}
			return pow;
		}
	}
	
	protected static BigDecimal stripTrailingZeros (BigDecimal z, boolean once) {
		if (z.signum() == 0) {
			return BigDecimal.ZERO;
		}
		int powNum = 1;
		BigInteger powTen = get2pownthpowerof10(powNum);
		BigInteger unscaled = z.unscaledValue();
		
		BigInteger [] divrem = new BigInteger [2];
		divrem [0] = unscaled;
		divrem [1] = BigInteger.ZERO;
		
		int scaleDiff = 0;
		int i = 0;
		
		divrem = unscaled.divideAndRemainder(powTen);
		
		if (divrem [1].signum() != 0) {
			return z;
		}
		
		powNum++;
		powTen = get2pownthpowerof10(powNum);
		unscaled = divrem [0];
		scaleDiff = 1;
		
		while (!once) {
			divrem = unscaled.divideAndRemainder(powTen);
			
			if (divrem [1].signum() == 0) {
				powNum++;
				powTen = get2pownthpowerof10(powNum);
				unscaled = divrem [0];
				scaleDiff <<= 1;
				scaleDiff++;
			} else {
				if (i != 0) {
					return stripTrailingZeros(new BigDecimal (unscaled, z.scale() - scaleDiff), false);
				} else {
					break;
				}
			}
			i++;
		}
		
		return stripTrailingZeros(new BigDecimal (unscaled, z.scale() - scaleDiff), true);
	}
} 
