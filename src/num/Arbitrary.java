/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

import java.math.*;

/**
 * This class is part of the API of this library. So, all the methods are safe to call, as their
 * returned results are either correct or an exception (mostly an ArithmeticException) is thrown.
 * The methods usually either call the equivalent ones from the Functions class (which are
 * protected) or from this class.
 * @author Johannes Kloimböck
 *
 */
public class Arbitrary {
	
	/**
	 * Prevent the instantiation of this class.
	 */
	private Arbitrary () {}
	
	/**
	 * If not specified in one of the methods, the library will internally use 5 more digits than
	 * returned to ensure that the result is always accurate to the last digit.
	 */
	private static final int internalPrecision = 5;
	
	/**
	 * Checks, whether the specified precision with the MathContext and the internal precision is
	 * not 0, as this would mean that the result is supposed to have infinitely many digits. As
	 * it is impossible to do so in most cases, it is completely "forbidden".
	 * @param mc is the MathContext that is to be checked
	 * @param iP is the specified internal precision
	 * @throws IllegalArgumentException if the number of digits to be calculated is 0.
	 */
	private static void mcprec0 (MathContext mc, int iP)
			throws IllegalArgumentException
	{
		if (mc.getPrecision() + iP == 0) {
			throw new IllegalArgumentException ("Cannot calculate this function to unlimited precision!");
		}
	}
	
	/**
	 * Calculates the natural logarithm of arg calling the lnNewton method in Functions. If,
	 * however, the required number of digits (mc.getPrecision + iP) is not larger than 15,
	 * it will instead call the doubleln method.
	 * @param arg is the BigDecimal number of which the natural logarithm is to be calculated
	 * @param mc specifies how many digits the result should have and how it should be rounded
	 * @param iP is the number of extra digits to which ln(x) is to be calculated
	 * @return the natural logarithm of arg as a BigDecimal to given precision (MathContext mc)
	 * @throws ArithmeticException if arg is not positive (arg <= 0)
	 */
	public static BigDecimal ln (BigDecimal arg, MathContext mc, int iP)
			throws ArithmeticException
	{
		mcprec0(mc, iP);
		if (arg.signum() > 0) {
			if (mc.getPrecision() <= 15 - iP) {
				return Functions.doubleln(arg).round(mc);
			} else {
				return Functions.ln(arg, mc.getPrecision() + iP).round(mc);
			}
		} else {
			throw new ArithmeticException ("Cannot calculate the natural logarithm of a non-positive number!");
		}
	}
	
	/**
	 * Calculates the natural logarithm of arg calling the ln method (above). This method can be
	 * used when the programmer does not want to specify an internal precision. In this case, the
	 * additional number of digits is the default one from the internalPrecision variable.
	 * @param arg is the BigDecimal number of which the natural logarithm is to be calculated
	 * @param mc specifies how many digits the result should have and how it should be rounded
	 * @return the natural logarithm of arg as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal ln (BigDecimal arg, MathContext mc) {
		return ln(arg, mc, internalPrecision);
	}
	
	/**
	 * Calculates the common logarithm of arg calling the Functions.lg method. If the overall
	 * precision is not greater than 15 (significant) digits the doublelg method is used for the
	 * computation instead. In order to return an exact result (all digits are correct) an
	 * internal precision can be specified which is added to the one from the MathContext for the
	 * actual computing process. When this method returns the result, these extra digits are
	 * removed by the rounding process.
	 * @param arg is the BigDecimal number of which the common logarithm is to be calculated
	 * @param mc is the MathContext that specifies how many digits are to be calculated and how
	 * the last digit should be rounded
	 * @param iP is the number of additional digits of precision 
	 * @return the common logarithm of arg as a BigDecimal to given precision (MathContext mc)
	 * @throws ArithmeticException if the argument is not a positive number
	 */
	public static BigDecimal lg (BigDecimal arg, MathContext mc, int iP)
			throws ArithmeticException
	{
		mcprec0(mc, iP);
		if (arg.signum() > 0) {
			if (mc.getPrecision() <= 15 - iP) {
				return Functions.doublelg(arg).round(mc);
			} else {
				return Functions.lg(arg, mc.getPrecision() + iP).round(mc);
			}
		} else {
			throw new ArithmeticException ("Cannot calculate the common logarithm of a non-positive number!");
		}
	}
	
	/**
	 * Calculates the common logarithm of arg calling the lg method (above). The internal
	 * precision is set to the default value.
	 * @param arg is the BigDecimal number of which the common logarithm is to be calculated
	 * @param mc is the MathContext that specifies the number of digits of precision of the
	 * returned result and how it is rounded.
	 * @return the common logarithm of arg as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal lg (BigDecimal arg, MathContext mc) {
		return lg(arg, mc, internalPrecision);
	}
	
	/**
	 * Calculates the logarithm of any number (arg1) to any base (arg2) by dividing the natural
	 * logarithm of arg1 by the natural logarithm of arg2 (ln(arg1) / ln(arg2)).
	 * To make the calculation especially for a small number of digits as efficient as possible,
	 * the internal precision of the two calls of the ln method is set to 0. 
	 * @param arg1 is the BigDecimal number of which the logarithm is to be calculated
	 * @param arg2 is the base (BigDecimal) of the logarithm
	 * @param mc specifies the number of digits of the result
	 * @param iP is the extra number of digits that is used to calculate the result and to
	 * hide potential false digits due to roundoff errors.
	 * @return the logarithm of arg1 to the base of arg2 to given precision (MathContext mc)
	 * @throws ArithmeticException if at least one of arg1 and arg2 is not positive or is the base
	 * is equal to 1.
	 */
	public static BigDecimal log (BigDecimal arg1, BigDecimal arg2, MathContext mc, int iP)
			throws ArithmeticException
	{
		mcprec0(mc, iP);
		if (arg1.signum() > 0 && arg2.signum() > 0 && arg2.compareTo(BigDecimal.ONE) != 0) {
			if (mc.getPrecision() <= 15 - iP && arg1.precision() <= 15 && arg2.precision() <= 15) {
				return Functions.doublelg(arg1).divide(Functions.doublelg(arg2), mc);
			} else {
				BigDecimal ln = ln(arg1, new MathContext (mc.getPrecision() + iP), 0);
				BigDecimal base = ln(arg2, new MathContext (mc.getPrecision() + iP), 0);
				return ln.divide(base, mc);
			}
		} else {
			throw new ArithmeticException ("Either arg1 and/or arg2 are/is non-negative or arg2 equals 1!");
		}
	}
	
	/**
	 * Calculates the logarithm of arg1 to the base of arg2 by calling the log method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg1 is the BigDecimal number of which the logarithm is to be calculated
	 * @param arg2 is the base (BigDecimal) of the logarithm
	 * @param mc MathContext to specify the precision of the calculated logarithm
	 * @return the logarithm of arg1 to the base of arg2 to given precision (MathContext mc)
	 */
	public static BigDecimal log (BigDecimal arg1, BigDecimal arg2, MathContext mc) {
		return log(arg1, arg2, mc, internalPrecision);
	}
	
	/**
	 * Calculates Euler's Number e (=2.71828...) by calling the eFaster method from the Functions
	 * class, if the desired precision is greater than 15 digits. If not, then the value provided
	 * by StrictMath.E is returned.
	 * @param mc MathContext to specify the precision of the returned BigDecimal
	 * @param iP is the number of extra precision for the internal calculation
	 * @return Euler's Number as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal e (MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (mc.getPrecision() <= 15 - iP) {
			return new BigDecimal (StrictMath.E).round(mc);
		} else {
			return Functions.eFaster(mc.getPrecision() + iP).round(mc);
		}
	}
	
	/**
	 * Calculates Euler's Number e (=2.71828...) by calling the e method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param mc MathContext specifies the returned number of digits
	 * @return Euler's Number as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal e (MathContext mc) {
		return e(mc, internalPrecision);
	}
	
	/**
	 * Calculates Pi (=3.14159...) by calling the piFaster method from the Functions class, if
	 * the desired precision is greater than 15 digits. If not, then the value provided by
	 * StrictMath.PI is returned.
	 * @param mc MathContext to specify the precision of the returned BigDecimal
	 * @param iP is the number of extra precision for the internal calculation
	 * @return Pi as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal pi (MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (mc.getPrecision() <= 15 - iP) {
			return new BigDecimal (StrictMath.PI).round(mc);
		} else {
			return Functions.pi(BigDecimal.ONE, mc.getPrecision() + iP).round(mc);
		}
	}
	
	/**
	 * Calculates Pi (=3.14159...) by calling the pi method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param mc MathContext specifies the returned number of digits
	 * @return Pi as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal pi (MathContext mc) {
		return pi(mc, internalPrecision);
	}
	
	/**
	 * Calculates the lemniscate constant (=2.6221...) by calling the lemniscate method from the
	 * Functions class.
	 * @param mc MathContext to specify the precision of the returned BigDecimal
	 * @param iP is the number of extra precision for the internal calculation
	 * @return the lemniscate constant as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal lemniscate (MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.lemniscate(mc.getPrecision() + iP).round(mc);
	}
	
	/**
	 * Calculates Pi (=2.6221...) by calling the lemniscate method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param mc MathContext specifies the returned number of digits
	 * @return the lemniscate constant as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal lemniscate (MathContext mc) {
		return lemniscate(mc, internalPrecision);
	}
	
	/**
	 * Calculates exp(z) (=e^x, where e is Euler's Number) calling either the expFaster method
	 * (if the overall precision is greater than 15 digits and the argument has more than 15
	 * digits) or the doubleexp method.
	 * @param arg is the BigDecimal of which the exponential function (base e) is to be
	 * calculated
	 * @param mc MathContext specifies the number of digits of the returned result
	 * @param iP is the number of extra digits which are used during the calculation. They are
	 * not shown in the returned BigDecimal.
	 * @return exp(x) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal exp (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (mc.getPrecision() <= 15 - iP && arg.precision() <= 15) {
			return Functions.doubleexp(arg).round(mc);
		} else {
			return Functions.exp(arg, mc.getPrecision() + iP).round(mc);
		}
	}
	
	/**
	 * Calculates exp(x) (=e^x, where x is Euler's Number) calling the exp method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the BigDecimal of which the exponential function (base e) is to be
	 * calculated
	 * @param mc MathContext specifies the number of digits of the returned result
	 * @return exp(x) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal exp (BigDecimal arg, MathContext mc) {
		return exp(arg, mc, internalPrecision);
	}
	
	/**
	 * Raises base to the power of exp. If base is a non-negative real number, exp can be any
	 * real number. If base is negative, exp must be an integer. If the precision is not greater
	 * than 15 digits and both arguments don't have more than 15 significant digits, the
	 * doublepower method is called. Else, if exp is an integer the pow method of BigDecimal
	 * is used and if exp is any real number, the power function is calculated as
	 * e^(exp * ln(base)) using the exp and ln methods.
	 * @param base is the number wich is to be raised to the power of exp
	 * @param exp is the exponent to which the base is raised
	 * @param mc MathContext specifies the number of significant digits of the result
	 * @param iP is the number of extra digits that are used internally in the computation
	 * @return base^exp as a BigDecimal to given precision (MathContext mc)
	 * @throws ArithmeticException when the base is negative and the exponent is non-integer
	 */
	public static BigDecimal power (BigDecimal base, BigDecimal exp, MathContext mc, int iP)
			throws ArithmeticException
	{ 
		if (Functions.isInteger(exp)) {
			if (mc.getPrecision() <= 15 - iP && base.precision() <= 15 && exp.precision() <= 15) {
				return Functions.doublepower(base, exp).round(mc);
			} else {
				if (exp.signum() >= 0) {
					return base.pow(exp.intValueExact(), mc);
				} else {
					base = BigDecimal.ONE.divide(base, new MathContext (mc.getPrecision() + iP));
					exp = exp.negate();
					return power(base, exp, mc, iP);
				}
			}
		} else {
			mcprec0(mc, iP);
			if (base.signum() >= 0) {
				if (mc.getPrecision() <= 15 - iP && base.precision() <= 15 && exp.precision() <= 15) {
					return Functions.doublepower(base, exp).round(mc);
				} else {
					if (exp.signum() >= 0) {
						BigDecimal intpowerpart = power(base, new BigDecimal (exp.toBigInteger()), new MathContext (mc.getPrecision() + iP), 0);
						exp = exp.subtract(new BigDecimal (exp.toBigInteger()));
						BigDecimal ln = ln(base, new MathContext (mc.getPrecision() + iP), 0);
						ln = ln.multiply(exp);
						return exp(ln, new MathContext (mc.getPrecision() + iP), 0).multiply(intpowerpart, mc);
					} else {
						exp = exp.negate();
						return BigDecimal.ONE.divide(power(base, exp, new MathContext (mc.getPrecision() + iP)), mc);
					}
				}
			} else {
				throw new ArithmeticException ("The base has to be a non-negative number and the exponent is a non-integer number!");
			}
		}
	}
	
	/**
	 * Raises base to the power of exp by calling the power method (above).
	 * @param base is the number wich is to be raised to the power of exp
	 * @param exp is the exponent to which the base is raised
	 * @param mc MathContext specifies the number of significant digits of the result
	 * @param iP is the number of extra digits that are used internally in the computation
	 * @return base^exp as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal power (BigDecimal base, BigDecimal exp, MathContext mc) {
		return power(base, exp, mc, internalPrecision);
	}
	
	/**
	 * Calculates 10 to the power of arg. If arg is an integer, the decimal point is just
	 * shifted. If not, then 10^arg is calculated as e^(arg * ln(10)).
	 * @param arg is the BigDecimal exponent
	 * @param mc MathContext that specifies the precision of the returned result in significant
	 * digits
	 * @param iP is the number of extra digits used in the calculation
	 * @return 10^arg as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal powTen (BigDecimal arg, MathContext mc, int iP) {
		if (Functions.isInteger(arg)) {
			return BigDecimal.ONE.scaleByPowerOfTen(arg.intValueExact());
		} else {
			mcprec0(mc, iP);
			if (mc.getPrecision() <= 15 - iP && arg.precision() <= 15) {
				return Functions.doublepowten(arg).round(mc);
			} else {
				BigDecimal ln10 = Functions.ln(BigDecimal.TEN, mc.getPrecision() + iP);
				BigDecimal exp = arg.multiply(ln10);
				return exp(exp, mc, iP);
			}
		}
	}
	
	/**
	 * Calculates 10 to the power of arg calling the powTen method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the BigDecimal exponent
	 * @param mc MathContext that specifies the precision of the returned result in significant
	 * digits
	 * @return 10^arg as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal powTen (BigDecimal arg, MathContext mc) {
		return powTen(arg, mc, internalPrecision);
	}
	
	/**
	 * Calculates the sine of any real number interpreting it as an angle in radiants by calling
	 * the sinr method.
	 * @param arg is the angle as a BigDecimal of which the sine is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param iP is the number of extra digits used in the calculation
	 * @return sin(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal sin (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.sinr(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
	}
	
	/**
	 * Calculates the sine of any real number interpreting it as an angle in radiants by calling
	 * the sinr method.
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the angle as a BigDecimal of which the sine is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @return sin(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal sin (BigDecimal arg, MathContext mc) {
		return sin(arg, mc, internalPrecision);
	}
	
	/**
	 * Calculates the sine of any real number interpreting it as an angle by calling either the
	 * sinr, the sind or the sing method. This depends on the angle mode.
	 * @param arg is the angle as a BigDecimal of which the sine is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param angleMode specifies whether arg is in radiants ("RAD"), degrees ("DEG") or gons
	 * ("GRAD"). The case does not matter.
	 * @param iP is the number of extra digits used in the calculation
	 * @return sin(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal sin (BigDecimal arg, String angleMode, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (angleMode.equalsIgnoreCase("rad")) {
			return Functions.sinr(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
		} else if (angleMode.equalsIgnoreCase("deg")) {
			return Functions.sind(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
		} else if (angleMode.equalsIgnoreCase("grad")) {
			return Functions.sing(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("The angle-mode must be either 'rad', 'deg' or 'grad'!");
		}
	}
	
	/**
	 * Calculates the sine of any real number interpreting it as an angle by calling the sin
	 * method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the angle as a BigDecimal of which the sine is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param angleMode specifies whether arg is in radiants ("RAD"), degrees ("DEG") or gons
	 * ("GRAD"). The case does not matter.
	 * @return sin(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal sin (BigDecimal arg, String angleMode, MathContext mc) {
		return sin(arg, angleMode, mc, internalPrecision);
	}
	
	/**
	 * Calculates the cosine of any real number interpreting it as an angle in radiants by
	 * calling the cosr method.
	 * @param arg is the angle as a BigDecimal of which the cosine is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param iP is the number of extra digits used in the calculation
	 * @return cos(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal cos (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.cosr(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
	}
	
	/**
	 * Calculates the cosine of any real number interpreting it as an angle in radiants by
	 * calling the cosr method.
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the angle as a BigDecimal of which the cosine is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @return cos(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal cos (BigDecimal arg, MathContext mc) {
		return cos(arg, mc, internalPrecision);
	}
	
	/**
	 * Calculates the cosine of any real number interpreting it as an angle by calling either the
	 * cosr, the cosd or the cosg method. This depends on the angle mode.
	 * @param arg is the angle as a BigDecimal of which the cosine is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param angleMode specifies whether arg is in radiants ("RAD"), degrees ("DEG") or gons
	 * ("GRAD"). The case does not matter.
	 * @param iP is the number of extra digits used in the calculation
	 * @return cos(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal cos (BigDecimal arg, String angleMode, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (angleMode.equalsIgnoreCase("rad")) {
			return Functions.cosr(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
		} else if (angleMode.equalsIgnoreCase("deg")) {
			return Functions.cosd(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
		} else if (angleMode.equalsIgnoreCase("grad")) {
			return Functions.cosg(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("The angle-mode must be either 'rad', 'deg' or 'grad'!");
		}
	}
	
	/**
	 * Calculates the cosine of any real number interpreting it as an angle by calling the cos
	 * method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the angle as a BigDecimal of which the cosine is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param angleMode specifies whether arg is in radiants ("RAD"), degrees ("DEG") or gons
	 * ("GRAD"). The case does not matter.
	 * @return cos(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal cos (BigDecimal arg, String angleMode, MathContext mc) {
		return cos(arg, angleMode, mc, internalPrecision);
	}
	
	/**
	 * Calculates the tangent of any real number interpreting it as an angle in radiants by
	 * calling the tanr method.
	 * @param arg is the angle as a BigDecimal of which the tangent is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param iP is the number of extra digits used in the calculation
	 * @return tan(arg) as a BigDecimal to given precision (MathContext mc)
	 * @throws ArithmeticException if the argument is an uneven multiple of pi/2
	 */
	public static BigDecimal tan (BigDecimal arg, MathContext mc, int iP)
			throws ArithmeticException
	{
		mcprec0(mc, iP);
		BigDecimal ratio = arg.divide(pi(new MathContext (mc.getPrecision() + iP), 0), new MathContext (mc.getPrecision() + iP));
		if (Functions.isInteger(ratio.subtract(BigDecimal.valueOf(5, 1))) == false) {
			return Functions.tanr(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("Cannot calculate the tangent of n*pi/2, where n is an odd integer!");
		}
	}
	
	/**
	 * Calculates the tangent of any real number interpreting it as an angle in radiants by
	 * calling the tanr method.
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the angle as a BigDecimal of which the tangent is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @return tan(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal tan (BigDecimal arg, MathContext mc) {
		return tan(arg, mc, internalPrecision);
	}
	
	/**
	 * Calculates the tangent of any real number interpreting it as an angle by calling either
	 * the tanr, the tand or the tang method. This depends on the angle mode.
	 * @param arg is the angle as a BigDecimal of which the tangent is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param angleMode specifies whether arg is in radiants ("RAD"), degrees ("DEG") or gons
	 * ("GRAD"). The case does not matter.
	 * @param iP is the number of extra digits used in the calculation
	 * @return tan(arg) as a BigDecimal to given precision (MathContext mc)
	 * @throws ArithmeticException if the argument is an uneven multiple of pi/2
	 */
	public static BigDecimal tan (BigDecimal arg, String angleMode, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (angleMode.equalsIgnoreCase("rad")) {
			return tan(arg, mc);
		} else if (angleMode.equalsIgnoreCase("deg")) {
			BigDecimal ratio = arg.divide(new BigDecimal (180), new MathContext (mc.getPrecision() + iP));
			if (Functions.isInteger(ratio.subtract(BigDecimal.valueOf(5, 1))) == false) {
				return Functions.tand(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
			} else {
				throw new ArithmeticException ("Cannot calculate the tangent of n*90, where n is an odd integer!");
			}
		} else if (angleMode.equalsIgnoreCase("grad")) {
			BigDecimal ratio = arg.divide(new BigDecimal (200), new MathContext (mc.getPrecision() + iP));
			if (Functions.isInteger(ratio.subtract(BigDecimal.valueOf(5, 1))) == false) {
				return Functions.tang(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
			} else {
				throw new ArithmeticException ("Cannot calculate the tangent of n*100, where n is an odd integer!");
			}
		} else {
			throw new ArithmeticException ("The angle-mode must be either 'rad', 'deg' or 'grad'!");
		}
	}
	
	/**
	 * Calculates the tangent of any real number interpreting it as an angle by calling the tan
	 * method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the angle as a BigDecimal of which the tangent is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param angleMode specifies whether arg is in radiants ("RAD"), degrees ("DEG") or gons
	 * ("GRAD"). The case does not matter.
	 * @return tan(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal tan (BigDecimal arg, String angleMode, MathContext mc) {
		return tan(arg, angleMode, mc, internalPrecision);
	}
	
	/**
	 * Calculates the cotangent of any real number interpreting it as an angle in radiants by
	 * calling the cotr method.
	 * @param arg is the angle as a BigDecimal of which the cotangent is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param iP is the number of extra digits used in the calculation
	 * @return cot(arg) as a BigDecimal to given precision (MathContext mc)
	 * @throws ArithmeticException if the argument is a multiple of pi
	 */
	public static BigDecimal cot (BigDecimal arg, MathContext mc, int iP)
			throws ArithmeticException
	{
		mcprec0(mc, iP);
		BigDecimal ratio = arg.divide(pi(new MathContext (mc.getPrecision() + iP), 0), new MathContext (mc.getPrecision() + iP));
		if (Functions.isInteger(ratio) == false) {
			return Functions.cotr(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("Cannot calculate the cotangent of n*pi, where n is an integer!");
		}
	}
	
	/**
	 * Calculates the cotangent of any real number interpreting it as an angle in radiants by
	 * calling the cotr method.
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the angle as a BigDecimal of which the tangent is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @return cot(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal cot (BigDecimal arg, MathContext mc) {
		return cot(arg, mc, internalPrecision);
	}
	
	/**
	 * Calculates the cotangent of any real number interpreting it as an angle by calling either
	 * the cotr, the cotd or the cotg method. This depends on the angle mode.
	 * @param arg is the angle as a BigDecimal of which the tangent is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param angleMode specifies whether arg is in radiants ("RAD"), degrees ("DEG") or gons
	 * ("GRAD"). The case does not matter.
	 * @param iP is the number of extra digits used in the calculation
	 * @return cot(arg) as a BigDecimal to given precision (MathContext mc)
	 * @throws ArithmeticException if the argument is a multiple of pi
	 */
	public static BigDecimal cot (BigDecimal arg, String angleMode, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (angleMode.equalsIgnoreCase("rad")) {
			return cot(arg, mc);
		} else if (angleMode.equalsIgnoreCase("deg")) {
			BigDecimal ratio = arg.divide(new BigDecimal (180), new MathContext (mc.getPrecision() + iP));
			if (Functions.isInteger(ratio) == false) {
				return Functions.cotd(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
			} else {
				throw new ArithmeticException ("Cannot calculate the cotangent of n*180, where n is an integer!");
			}
		} else if (angleMode.equalsIgnoreCase("grad")) {
			BigDecimal ratio = arg.divide(new BigDecimal (200), new MathContext (mc.getPrecision() + iP));
			if (Functions.isInteger(ratio) == false) {
				return Functions.cotg(arg, pi(new MathContext (mc.getPrecision() + iP), 0), mc.getPrecision() + iP).round(mc);
			} else {
				throw new ArithmeticException ("Cannot calculate the cotangent of n*200, where n is an integer!");
			}
		} else {
			throw new ArithmeticException ("The angle-mode must be either 'rad', 'deg' or 'grad'!");
		}
	}
	
	/**
	 * Calculates the cotangent of any real number interpreting it as an angle by calling the tan
	 * method (above).
	 * The number of digits in the internal precision is set to the value specified in the
	 * variable internalPrecision.
	 * @param arg is the angle as a BigDecimal of which the cotangent is to be calculated
	 * @param mc MathContext specifies the precision of the result
	 * @param angleMode specifies whether arg is in radiants ("RAD"), degrees ("DEG") or gons
	 * ("GRAD"). The case does not matter.
	 * @return cot(arg) as a BigDecimal to given precision (MathContext mc)
	 */
	public static BigDecimal cot (BigDecimal arg, String angleMode, MathContext mc) {
		return cot(arg, angleMode, mc, internalPrecision);
	}
	
	/**
	 * Calculates the arc sine of a number between -1 and 1 calling the asinda method.
	 * @param arg 
	 * @param mc 
	 * @param iP 
	 * @return 
	 * @throws 
	 */
	public static BigDecimal asin (BigDecimal arg, MathContext mc, int iP)
			throws ArithmeticException
	{
		mcprec0(mc, iP);
		if (arg.compareTo(BigDecimal.ONE) <= 0 && arg.compareTo(BigDecimal.ONE.negate()) >= 0) {
			if (arg.compareTo(BigDecimal.ONE) != 0) {
				if ((mc.getPrecision() + iP) < 300) {
					return Functions.asinda(arg, "rad", mc.getPrecision() + iP).round(mc);
				} else {
					return Functions.arcsinNewton(arg, mc.getPrecision() + iP).round(mc);
				}
			} else {
				return pi(new MathContext (mc.getPrecision() + iP), 0).multiply(BigDecimal.valueOf(5, 1)).round(mc);
			}
		} else {
			throw new ArithmeticException ("Cannot calculate the asin of a number that is either larger than 1 or smaller than -1!");
		}
	}
	
	public static BigDecimal asin (BigDecimal arg, MathContext mc) {
		return asin(arg, mc, internalPrecision);
	}
	
	public static BigDecimal asin (BigDecimal arg, String angleMode, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (angleMode.equalsIgnoreCase("rad") || angleMode.equalsIgnoreCase("deg") || angleMode.equalsIgnoreCase("grad")) {
			if (arg.compareTo(BigDecimal.ONE) <= 0 && arg.compareTo(BigDecimal.ONE.negate()) >= 0) {
				if (arg.compareTo(BigDecimal.ONE) != 0) {
					if ((mc.getPrecision() + iP) < 300) {
						return Functions.asinda(arg, angleMode, mc.getPrecision() + iP).round(mc);
					} else {
						BigDecimal rad = Functions.arcsinNewton(arg, mc.getPrecision() + iP).round(mc);
						if (angleMode.equalsIgnoreCase("rad")) {
							return rad;
						} else if (angleMode.equalsIgnoreCase("deg")) {
							return rad.multiply(BigDecimal.valueOf(180)).divide(pi(new MathContext (mc.getPrecision() + iP)), mc);
						} else {
							return rad.multiply(BigDecimal.valueOf(200)).divide(pi(new MathContext (mc.getPrecision() + iP)), mc);
						}
					}
				} else {
					BigDecimal result = BigDecimal.ZERO;
					if (angleMode.equalsIgnoreCase("rad")) {
						result = pi(new MathContext (mc.getPrecision() + iP), 0).multiply(BigDecimal.valueOf(5, 1)).round(mc);
					} else if (angleMode.equalsIgnoreCase("deg")) {
						result = BigDecimal.valueOf(90, 0).round(mc);
					} else {
						result = BigDecimal.valueOf(100, 0).round(mc);
					}
					
					return result;
				}
			} else {
				throw new ArithmeticException ("Cannot calculate the asin of a number that is either larger than 1 or smaller than -1!");
			}
		} else {
			throw new ArithmeticException ("The angle-mode must be either 'rad', 'deg' or 'grad'!");
		}
	}
	
	public static BigDecimal asin (BigDecimal arg, String angleMode, MathContext mc) {
		return asin(arg, angleMode, mc, internalPrecision);
	}
	
	public static BigDecimal acos (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (arg.compareTo(BigDecimal.ONE) <= 0 && arg.compareTo(BigDecimal.ONE.negate()) >= 0) {
			if (arg.signum() != 0) {
				if ((mc.getPrecision() + iP) < 300) {
					return Functions.acosda(arg, "rad", mc.getPrecision() + iP).round(mc);
				} else {
					return Functions.arccosNewton(arg, mc.getPrecision() + iP).round(mc);
				}
			} else {
				return pi(new MathContext (mc.getPrecision() + iP), 0).multiply(BigDecimal.valueOf(5, 1)).round(mc);
			}
		} else {
			throw new ArithmeticException ("Cannot calculate the acos of a number that is either larger than 1 or smaller than -1!");
		}
	}
	
	public static BigDecimal acos (BigDecimal arg, MathContext mc) {
		return acos(arg, mc, internalPrecision);
	}
	
	public static BigDecimal acos (BigDecimal arg, String angleMode, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (angleMode.equalsIgnoreCase("rad") || angleMode.equalsIgnoreCase("deg") || angleMode.equalsIgnoreCase("grad")) {
			if (arg.compareTo(BigDecimal.ONE) <= 0 && arg.compareTo(BigDecimal.ONE.negate()) >= 0) {
				if (arg.signum() != 0) {
					if ((mc.getPrecision() + iP) < 300) {
						return Functions.acosda(arg, angleMode, mc.getPrecision() + iP).round(mc);
					} else {
						BigDecimal rad = Functions.arccosNewton(arg, mc.getPrecision() + iP).round(mc);
						if (angleMode.equalsIgnoreCase("rad")) {
							return rad;
						} else if (angleMode.equalsIgnoreCase("deg")) {
							return rad.multiply(BigDecimal.valueOf(180)).divide(pi(new MathContext (mc.getPrecision() + iP)), mc);
						} else {
							return rad.multiply(BigDecimal.valueOf(200)).divide(pi(new MathContext (mc.getPrecision() + iP)), mc);
						}
					}
				} else {
					BigDecimal result = BigDecimal.ZERO;
					if (angleMode.equalsIgnoreCase("rad")) {
						result = pi(new MathContext (mc.getPrecision() + iP)).multiply(BigDecimal.valueOf(5, 1)).round(mc);
					} else if (angleMode.equalsIgnoreCase("deg")) {
						result = BigDecimal.valueOf(90, 0).round(mc);
					} else {
						result = BigDecimal.valueOf(100, 0).round(mc);
					}
					
					return result;
				}
			} else {
				throw new ArithmeticException ("Cannot calculate the acos of a number that is either larger than 1 or smaller than -1!");
			}
		} else {
			throw new ArithmeticException ("The angle-mode must be either 'rad', 'deg' or 'grad'!");
		}
	}
	
	public static BigDecimal acos (BigDecimal arg, String angleMode, MathContext mc) {
		return acos(arg, angleMode, mc, internalPrecision);
	}
	
	public static BigDecimal atan (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if ((mc.getPrecision() + iP) < 300) {
			return Functions.atanda(arg, "rad", mc.getPrecision() + iP).round(mc);
		} else {
			return Functions.arctanNewton(arg, mc.getPrecision() + iP).round(mc);
		}
	}
	
	public static BigDecimal atan (BigDecimal arg, MathContext mc) {
		return atan(arg, mc, internalPrecision);
	}
	
	public static BigDecimal atan (BigDecimal arg, String angleMode, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (angleMode.equalsIgnoreCase("rad") || angleMode.equalsIgnoreCase("deg") || angleMode.equalsIgnoreCase("grad")) {
			if ((mc.getPrecision() + iP) < 300) {
				return Functions.atanda(arg, angleMode, mc.getPrecision() + iP).round(mc);
			} else {
				BigDecimal rad = Functions.arctanNewton(arg, mc.getPrecision() + iP).round(mc);
				if (angleMode.equalsIgnoreCase("rad")) {
					return rad;
				} else if (angleMode.equalsIgnoreCase("deg")) {
					return rad.multiply(BigDecimal.valueOf(180)).divide(pi(new MathContext (mc.getPrecision() + iP)), mc);
				} else {
					return rad.multiply(BigDecimal.valueOf(200)).divide(pi(new MathContext (mc.getPrecision() + iP)), mc);
				}
			}
		} else {
			throw new ArithmeticException ("The angle-mode must be either 'rad', 'deg' or 'grad'!");
		}
	}
	
	public static BigDecimal atan (BigDecimal arg, String angleMode, MathContext mc) {
		return atan(arg, angleMode, mc, internalPrecision);
	}
	
	public static BigDecimal acot (BigDecimal arg, MathContext mc, int iP) {
		return atan(BigDecimal.ONE.divide(arg, new MathContext (mc.getPrecision() + iP)), mc, iP);
	}
	
	public static BigDecimal acot (BigDecimal arg, MathContext mc) {
		return acot(arg, mc, internalPrecision);
	}
	
	public static BigDecimal acot (BigDecimal arg, String angleMode, MathContext mc, int iP) {
		return atan(BigDecimal.ONE.divide(arg, new MathContext (mc.getPrecision() + iP)), angleMode, mc, iP);
	}
	
	public static BigDecimal acot (BigDecimal arg, String angleMode, MathContext mc) {
		return acot(arg, angleMode, mc, internalPrecision);
	}
	
	public static BigDecimal sinh (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.sinh(arg, mc.getPrecision() + iP).round(mc);
	}
	
	public static BigDecimal sinh (BigDecimal arg, MathContext mc) {
		return sinh(arg, mc, internalPrecision);
	}
	
	public static BigDecimal cosh (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.cosh(arg, mc.getPrecision() + iP).round(mc);
	}
	
	public static BigDecimal cosh (BigDecimal arg, MathContext mc) {
		return cosh(arg, mc, internalPrecision);
	}
	
	public static BigDecimal tanh (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.tanh(arg, mc.getPrecision() + iP).round(mc);
	}
	
	public static BigDecimal tanh (BigDecimal arg, MathContext mc) {
		return tanh(arg, mc, internalPrecision);
	}
	
	public static BigDecimal arsinh (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.arsinh(arg, mc.getPrecision() + iP).round(mc);
	}
	
	public static BigDecimal arsinh (BigDecimal arg, MathContext mc) {
		return arsinh(arg, mc, internalPrecision);
	}
	
	public static BigDecimal arcosh (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (arg.compareTo(BigDecimal.ONE) >= 0) {
			return Functions.arcosh(arg, mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("Cannot calculate the arcosh of a number that is smaller than 1!");
		}
	}
	
	public static BigDecimal arcosh (BigDecimal arg, MathContext mc) {
		return arcosh(arg, mc, internalPrecision);
	}
	
	public static BigDecimal artanh (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (arg.compareTo(BigDecimal.ONE) < 0 && arg.compareTo(BigDecimal.ONE.negate()) > 0) {
			return Functions.artanh(arg, mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("Cannot calculate the artanh of a number that is larger than 1 or smaller than -1!");
		}
	}
	
	public static BigDecimal artanh (BigDecimal arg, MathContext mc) {
		return artanh(arg, mc, internalPrecision);
	}
	
	public static BigDecimal agm (BigDecimal arg1, BigDecimal arg2, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (arg1.signum() >= 0 && arg2.signum() >= 0) {
			return Functions.agm(arg1, arg2, mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("Both arguments have to be non-negative numbers!");
		}
	}
	
	public static BigDecimal agm (BigDecimal arg1, BigDecimal arg2, MathContext mc) {
		return agm(arg1, arg2, mc, internalPrecision);
	}
	
	public static BigDecimal factorial (BigDecimal arg, MathContext mc, int iP) {
		if (Functions.isInteger(arg)) {
			if (arg.signum() >= 0) {
				return Functions.factorial(arg, mc.getPrecision() + iP).round(mc);
			} else {
				throw new ArithmeticException ("Cannot calculate the factorial of a negative integer!");
			}
		} else {
			throw new ArithmeticException ("Cannot calculate the factorial of a non-integer number!");
		}
	}
	
	public static BigDecimal factorial (BigDecimal arg, MathContext mc) {
		return factorial(arg, mc, internalPrecision);
	}
	
	public static BigInteger factorial (BigDecimal arg) {
		if (Functions.isInteger(arg)) {
			if (arg.signum() >= 0) {
				return Functions.factorial(arg).toBigInteger();
			} else {
				throw new ArithmeticException ("Cannot calculate the factorial of a negative integer!");
			}
		} else {
			throw new ArithmeticException ("Cannot calculate the factorial of a non-integer number!");
		}
	}
	
	public static BigDecimal factorial (BigInteger arg, MathContext mc, int iP) {
		if (arg.signum() >= 0) {
			return Functions.factorial(new BigDecimal (arg), mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("Cannot calculate the factorial of a negative integer!");
		}
	}
	
	public static BigDecimal factorial (BigInteger arg, MathContext mc) {
		return factorial (arg, mc, internalPrecision);
	}
	
	public static BigInteger factorial (BigInteger arg) {
		if (arg.signum() >= 0) {
			return Functions.factorial(new BigDecimal (arg)).toBigInteger();
		} else {
			throw new ArithmeticException ("Cannot calculate the factorial of a negative integer!");
		}
	}
	
	public static BigDecimal cbrt (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (mc.getPrecision() <= 10 && arg.precision() <= 15) {
			return Functions.doublecbrt(arg).round(mc);
		} else {
			return Functions.cbrt(arg, mc.getPrecision() + iP).round(mc);
		}
	}
	
	public static BigDecimal cbrt (BigDecimal arg, MathContext mc) {
		return cbrt(arg, mc, internalPrecision);
	}
	
	public static BigDecimal cdf (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.cdf(arg, mc.getPrecision() + iP, true).round(mc);
	}
	
	public static BigDecimal cdf (BigDecimal arg, MathContext mc) {
		return cdf(arg, mc, internalPrecision);
	}
	
	public static BigDecimal cdf (BigDecimal x, BigDecimal my, BigDecimal sigma, MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.cdf(Functions.zTransformation(x, my, sigma, mc.getPrecision() + iP), mc.getPrecision() + iP, true).round(mc);
	}
	
	public static BigDecimal cdf (BigDecimal x, BigDecimal my, BigDecimal sigma, MathContext mc) {
		return cdf(x, my, sigma, mc, internalPrecision);
	}
	
	public static BigDecimal binomialCoefficient (BigDecimal arg1, BigDecimal arg2, MathContext mc, int iP) {
		if (arg1.compareTo(arg2) >= 0 && Functions.isInteger(arg1) && Functions.isInteger(arg2)) {
			return Functions.binomialCoefficient(arg1, arg2, mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("Make sure that both numbers are integers and arg1 is not smaller than arg2!");
		}
	}
	
	public static BigDecimal binomialCoefficient (BigDecimal arg1, BigDecimal arg2, MathContext mc) {
		return binomialCoefficient(arg1, arg2, mc, internalPrecision);
	}
	
	public static BigDecimal binomialCoefficient (BigDecimal arg1, BigDecimal arg2) {
		if (arg1.compareTo(arg2) >= 0 && Functions.isInteger(arg1) && Functions.isInteger(arg2)) {
			return Functions.binomialCoefficient(arg1, arg2);
		} else {
			throw new ArithmeticException ("Make sure that both numbers are integers and arg1 is not smaller than arg2!");
		}
	}
	
	public static BigDecimal probit (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (arg.signum() > 0 && arg.compareTo(BigDecimal.ONE) < 0) {
			return Functions.invcdf(arg, mc.getPrecision() + iP, true).round(mc);
		} else {
			throw new ArithmeticException ("Argument must be between 0 and 1!");
		}
	}
	
	public static BigDecimal probit (BigDecimal arg, MathContext mc) {
		return probit(arg, mc, internalPrecision);
	}
	
	public static BigDecimal erf (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.erf(arg, mc.getPrecision() + iP).round(mc);
	}
	
	public static BigDecimal erf (BigDecimal arg, MathContext mc) {
		return erf(arg, mc, internalPrecision);
	}
	
	public static BigDecimal erfinv (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (arg.compareTo(BigDecimal.ZERO) > 0 && arg.compareTo(BigDecimal.ONE) < 0) {
			return Functions.erfinv(arg, mc.getPrecision() + iP);
		} else {
			throw new ArithmeticException ("Argument must be between -1 and 1!");
		}
	}
	
	public static BigDecimal erfinv (BigDecimal arg, MathContext mc) {
		return erfinv(arg, mc, internalPrecision);
	}
	
	public static BigDecimal sqrt (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (arg.signum() > 0) {
			arg = arg.round(new MathContext (mc.getPrecision() + iP));
			if (mc.getPrecision() <= 15 - iP && arg.precision() <= 15) {
				return Functions.doublesqrt(arg).round(mc);
			} else {
				return Functions.sqrt(arg, mc.getPrecision() + iP).round(mc);
			}
		} else {
			throw new ArithmeticException ("Argument must be non-negative!");
		}
	}
	
	public static BigDecimal sqrt (BigDecimal arg, MathContext mc) {
		return sqrt(arg, mc, internalPrecision);
	}
	
	public static BigDecimal sqrtExact (BigDecimal arg, int iP) {
		BigInteger unscaled = arg.scale() % 2 == 0 ? arg.unscaledValue() : arg.setScale(arg.scale() + 1).unscaledValue();
		BigInteger [] r = sqrtAndRemainder(unscaled, iP);
		
		if (r [1].signum() != 0) {
			throw new ArithmeticException ("sqrt(arg) cannot be computed exactly!");
		}
		
		return new BigDecimal (r [0], arg.scale() / 2);
	}
	
	public static BigDecimal sqrtExact (BigDecimal arg) {
		return sqrtExact(arg, internalPrecision);
	}
	
	public static BigInteger [] sqrtAndRemainder (BigInteger arg, int iP) {
		if (arg.signum() == 0) {
			BigInteger [] result = new BigInteger [2];
			result [0] = result [1] = BigInteger.ZERO;
			return result;
		}
		
		int shift = (arg.bitLength() >> 1) - 1 - iP;
		shift = shift % 2 != 0 ? shift + 1 : shift;
		
		BigInteger shiftArg = arg.shiftRight(shift);
		BigInteger result = Functions.multiplyByPowerOfTwo(sqrt(new BigDecimal (shiftArg), new MathContext ((int)(shiftArg.bitLength() * Functions.lg2) + 1 + iP), 0), shift >> 1).toBigInteger();
		
		BigInteger rem = arg.subtract(result.multiply(result));
		
		BigInteger remCorrDown = result.shiftLeft(1).add(BigInteger.ONE);
		
		if (rem.signum() < 0) {
			result = result.subtract(BigInteger.ONE);
			rem = rem.add(result.shiftLeft(1)).add(BigInteger.ONE);
		} else if (rem.compareTo(remCorrDown) >= 0) {
			result = result.add(BigInteger.ONE);
			rem = rem.subtract(remCorrDown);
		}
		
		BigInteger [] r = new BigInteger [2];
		r [0] = result;
		r [1] = rem;
		
		return r;
	}
	
	public static BigInteger [] sqrtAndRemainder (BigInteger arg) {
		return sqrtAndRemainder(arg, internalPrecision);
	}
	
	public static BigInteger sqrt (BigInteger arg, int iP) {
		return sqrtAndRemainder(arg, iP) [0];
	}
	
	public static BigInteger sqrt (BigInteger arg) {
		return sqrt(arg, internalPrecision);
	}
	
	public static BigDecimal invroot (BigDecimal arg, int order, MathContext mc, int iP)
			throws ArithmeticException
	{
		mcprec0(mc, iP);
		if (arg.compareTo(BigDecimal.ONE) == 0) {
			return BigDecimal.ONE;
		} else if (arg.compareTo(BigDecimal.ZERO) == 0) {
			if (order <= 0) {
				return BigDecimal.ZERO;
			} else {
				throw new ArithmeticException ("Cannot calculate the reciprocal value of 0!");
			}
		}
		if (arg.signum() < 0 && order % 2 == 0) {
			throw new ArithmeticException ("Cannot calculate the (inverse) root of an even order!");
		}
		
		int signum = arg.signum();
		arg = arg.abs();
		
		if (order == -1) {
			return signum >= 0 ? arg : arg.negate(mc);
		} else if (order == -2) {
			return sqrt(arg, mc, iP);
		} else if (order > 1) {
			BigDecimal root = BigDecimal.ZERO;
			if (order < 45) {
				root = Functions.invroot(arg, order, mc.getPrecision() + iP).round(mc);
			} else {
				BigDecimal exponent = BigDecimal.ONE.divide(new BigDecimal (order), new MathContext (mc.getPrecision() + iP));
				root = power(arg, exponent, mc, iP);
			}
			return signum >= 0 ? root : root.negate();
		} else if (order == 1) {
			BigDecimal root = BigDecimal.ONE.divide(arg, mc);
			return signum >= 0 ? root : root.negate();
		} else if (order == -3) {
			BigDecimal root = cbrt(arg, mc, iP);
			return signum >= 0 ? root : root.negate();
		} else if (order <= -1) {
			BigDecimal root = BigDecimal.ZERO;
			if (order > -45) {
				root = BigDecimal.ONE.divide(Functions.invroot(arg, -order, mc.getPrecision() + iP), mc);
			} else {
				BigDecimal exponent = BigDecimal.ONE.divide(new BigDecimal (-order), new MathContext (mc.getPrecision() + iP));
				root = power(arg, exponent, mc, iP);
			}
			return signum >= 0 ? root : root.negate();
		} else {
			throw new ArithmeticException ("Cannot calculate the zeroeth root of" + arg + "!");
		}
	}
	
	public static BigDecimal invroot (BigDecimal arg, int order, MathContext mc) {
		return invroot(arg, order, mc, internalPrecision);
	}
	
	public static BigDecimal root (BigDecimal arg, int order, MathContext mc, int iP)
			throws ArithmeticException
	{
		mcprec0(mc, iP);
		try {
			return invroot(arg, -order, mc, iP);
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public static BigDecimal root (BigDecimal arg, int order, MathContext mc) {
		return root(arg, order, mc, internalPrecision);
	}
	
	public static BigDecimal atan2 (BigDecimal y, BigDecimal x, MathContext mc, int iP)
			throws ArithmeticException
	{
		if (x.signum() > 0) {
			return atan(y.divide(x, new MathContext (mc.getPrecision() + iP)), mc, iP);
		} else if (x.signum() < 0 && y.signum() >= 0) {
			return atan2(y, x.negate(), new MathContext (mc.getPrecision() + iP), 0).negate().add(pi(new MathContext (mc.getPrecision() + iP), 0), mc);
		} else if (x.signum() < 0 && y.signum() < 0) {
			return atan2(y.negate(), x.negate(), new MathContext (mc.getPrecision() + iP), 0).subtract(pi(new MathContext (mc.getPrecision() + iP), 0), mc);
		} else if (x.signum() == 0 && y.signum() > 0) {
			return pi(mc, iP).multiply(BigDecimal.valueOf(5, 1), mc);
		} else if (x.signum() == 0 && y.signum() < 0) {
			return pi(mc, iP).multiply(BigDecimal.valueOf(-5, 1), mc);
		} else {
			throw new ArithmeticException ("Both arguments cannot be 0 at the same time!");
		}
	}
	
	public static BigDecimal atan2 (BigDecimal arg1, BigDecimal arg2, MathContext mc) {
		return atan2(arg1, arg2, mc, internalPrecision);
	}
	
	public static BigDecimal hypot (BigDecimal arg1, BigDecimal arg2, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (arg1.signum() == 0) {
			return arg2.round(mc);
		} else if (arg2.signum() == 0) {
			return arg1.round(mc);
		} else {
			return sqrt(arg1.multiply(arg1).add(arg2.multiply(arg2)).round(new MathContext (mc.getPrecision() + iP)), mc, iP);
		}
	}
	
	public static BigDecimal hypot (BigDecimal arg1, BigDecimal arg2, MathContext mc) {
		return hypot(arg1, arg2, mc, internalPrecision);
	}
	
	public static BigDecimal subfactorial (BigInteger arg, MathContext mc, int iP) {
		if (arg.signum() > 0) {
			return NTools.roundToInt(new BigDecimal (factorial(arg).add(BigInteger.ONE)).divide(e(new MathContext (mc.getPrecision() + iP)), mc), RoundingMode.DOWN);
		} else if (arg.signum() == 0) {
			return BigDecimal.ONE;
		} else {
			throw new ArithmeticException ("Argument must be non-negative!");
		}
	}
	
	public static BigDecimal subfactorial (BigInteger arg, MathContext mc) {
		return subfactorial(arg, mc, internalPrecision);
	}

	public static BigInteger subfactorial (BigInteger arg) {
		if (arg.signum() >= 0) {
			return Functions.subFactorial(arg.intValueExact());
		} else {
			throw new ArithmeticException ("Argument must be non-negative!");
		}
	}

	public static BigDecimal eulerMascheroni (MathContext mc, int iP) {
		mcprec0(mc, iP);
		return Functions.eulerMascheroniConstant(mc.getPrecision() + iP).round(mc);
	}
	
	public static BigDecimal eulerMascheroni (MathContext mc) {
		return eulerMascheroni(mc, internalPrecision);
	}
	
	public static BigDecimal lambertW (BigDecimal arg, MathContext mc, int iP) {
		mcprec0(mc, iP);
		if (arg.compareTo(BigDecimal.ONE.divide(e(new MathContext (mc.getPrecision() + iP), 0), mc).negate()) >= 0) {
			return Functions.lambertW(arg, mc.getPrecision() + iP).round(mc);
		} else {
			throw new ArithmeticException ("Cannot calculate the Lambert W function for arguments smaller than -exp(-1)!");
		}
	}
	
	public static BigDecimal lambertW (BigDecimal arg, MathContext mc) {
		return lambertW(arg, mc, internalPrecision);
	}
}
