/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

public class ComplexDouble {
	private double real;
	private double imaginary;
	
	public ComplexDouble (double r, double i) {
		real = r;
		imaginary = i;
	}
	
	public ComplexDouble () {
		real = imaginary = 0;
	}
	
	public ComplexDouble (AComplex complex) {
		real = complex.getReal().doubleValue();
		imaginary = complex.getImaginary().doubleValue();
	}
	
	public ComplexDouble (String complex) {
		//"a+bi"
		String [] c = new String [2];
		try {
			c = NTools.complexParser(complex);
			real = Double.parseDouble(c [0]);
			imaginary = Double.parseDouble(c [1]);
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException ("Input (" + complex + ") is not of correct format!");
		}
	}
	
	public static final ComplexDouble I = new ComplexDouble (0.0, 1.0);
	public static final ComplexDouble ONE = new ComplexDouble (1.0, 0.0);
	public static final ComplexDouble ZERO = new ComplexDouble (0.0, 0.0);
	
	public double getReal () {
		return real;
	}
	
	public double getImaginary () {
		return imaginary;
	}
	
	@Override
	public String toString () {
		if (this.getImaginary() >= 0) {
			return String.valueOf(this.getReal()) + " + " + String.valueOf(this.getImaginary()) + "i";
		} else {
			return String.valueOf(this.getReal()) + " - " + String.valueOf((-1) * this.getImaginary()) + "i";
		}
	}
	
	public boolean isReal () {
		if (this.getImaginary() == 0.0) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean equals (Object number2) {
		if (this == number2) {
			return true;
		}
		
		if (!(number2 instanceof ComplexDouble)) {
			return false;
		}
		
		if (this.getReal() == number2.getReal() && this.getImaginary() == number2.getImaginary()) {
			return true;
		} else {
			return false;
		}
	}
	
	public ComplexDouble add (ComplexDouble number2) {
		double r = this.getReal() + number2.getReal();
		double i = this.getImaginary() + number2.getImaginary();
		return new ComplexDouble (r, i);
	}
	
	public ComplexDouble subtract (ComplexDouble number2) {
		double r = this.getReal() - number2.getReal();
		double i = this.getImaginary() - number2.getImaginary();
		return new ComplexDouble (r, i);
	}
	
	public ComplexDouble multiply (ComplexDouble number2) {
		double r = this.getReal() * number2.getReal() - this.getImaginary() * number2.getImaginary();
		double i = this.getReal() * number2.getImaginary() + this.getImaginary() * number2.getReal();
		return new ComplexDouble (r, i);
	}
	
	public ComplexDouble conjugate () {
		double i = (-1.0) * this.getImaginary();
		return new ComplexDouble (this.getReal(), i);
	}
	
	public ComplexDouble divide (ComplexDouble number2) {
		double d = number2.getReal() * number2.getReal() + number2.getImaginary() * number2.getImaginary();
		ComplexDouble a = this.multiply(number2.conjugate());
		double r = a.getReal() / d;
		double i = a.getImaginary() / d;
		return new ComplexDouble (r, i);
	}
	
	public ComplexDouble sqr () {
		double r = this.getReal() * this.getReal() - this.getImaginary() * this.getImaginary();
		double i = 2.0 * this.getReal() * this.getImaginary();
		return new ComplexDouble (r, i);
	}
	
	public double arg () {
		return Math.atan2(this.getImaginary(), this.getReal());
	}
	
	public double abs () {
		double abssqr = this.getReal() * this.getReal() + this.getImaginary() * this.getImaginary();
		return Math.sqrt(abssqr);
	}
	
	public ComplexDouble sqrt () {
		if (this.equals(ZERO) == false) {
			double abssqrt = Math.sqrt(this.abs());
			double arg = this.arg() * 0.5;
			double r = abssqrt * Math.cos(arg);
			double i = abssqrt * Math.sin(arg);
			return new ComplexDouble (r, i);
		} else {
			return ZERO;
		}
	}
	
	public ComplexDouble exp () {
		double exp = Math.exp(this.getReal());
		double cos = Math.cos(this.getImaginary());
		double sin = Math.sin(this.getImaginary());
		cos = exp * cos;
		sin = exp * sin;
		return new ComplexDouble (cos, sin);
	}
	
	public ComplexDouble ln () {
		double ln = Math.log(this.abs());
		double arg = this.arg();
		return new ComplexDouble (ln, arg);
	}
	
	public ComplexDouble log (ComplexDouble number2) {
		ComplexDouble ln = this.ln();
		ComplexDouble base = number2.ln();
		return ln.divide(base);
	}
	
	public ComplexDouble power (ComplexDouble number2) {
		if (number2.equals(ZERO) == false) {
			ComplexDouble ln = this.ln().multiply(number2);
			ComplexDouble exp = ln.exp();
			return exp;
		} else {
			return ONE;
		}
	}
	
	public ComplexDouble power (int number2) {
		ComplexDouble r = ONE;
		for (int i = 0; i < number2; i++) {
			r = r.multiply(this);
		}
		return r;
	}
	
	public ComplexDouble cube () {
		return this.sqr().multiply(this);
	}
	
	public ComplexDouble power (double number2) {
		return this.power(new ComplexDouble (number2, 0.0));
	}
	
	public ComplexDouble sin () {
		ComplexDouble expz = this.exp();
		ComplexDouble expmz = ONE.divide(expz);
		return expz.subtract(expmz).multiply(new ComplexDouble (0.5, 0.0));
	}
	
	public ComplexDouble cos () {
		ComplexDouble expz = this.exp();
		ComplexDouble expmz = ONE.divide(expz);
		return expz.add(expmz).multiply(new ComplexDouble (0.5, 0.0));
	}
	
	public ComplexDouble tan () {
		ComplexDouble expiz = this.multiply(I).exp();
		ComplexDouble expmiz = ONE.divide(expiz);
		ComplexDouble sin = expiz.subtract(expmiz);
		ComplexDouble cos = expiz.add(expmiz).multiply(I);
		return sin.divide(cos);
	}
	
	public ComplexDouble asin () {
		ComplexDouble sqrt = ONE.subtract(this.sqr()).sqrt();
		ComplexDouble arg = this.multiply(I).add(sqrt);
		return arg.ln().multiply(new ComplexDouble (0.0, -1.0));
	}
	
	public ComplexDouble acos () {
		ComplexDouble sqrt = ONE.subtract(this.sqr()).sqrt();
		ComplexDouble arg = this.add(sqrt.multiply(I));
		return arg.ln().multiply(new ComplexDouble (0.0, -1.0));
	}
	
	public ComplexDouble atan () {
		ComplexDouble a = ONE.add(I.multiply(this));
		ComplexDouble b = ONE.subtract(I.multiply(this));
		ComplexDouble ln = a.divide(b).ln();
		return ln.multiply(new ComplexDouble (0.0, 0.5));
	}
	
	public ComplexDouble sinh () {
		ComplexDouble expz = this.exp();
		ComplexDouble expmz = ONE.divide(expz);
		return expz.subtract(expmz).multiply(new ComplexDouble (0.5, 0.0));
	}
	
	public ComplexDouble cosh () {
		ComplexDouble expz = this.exp();
		ComplexDouble expmz = ONE.divide(expz);
		return expz.add(expmz).multiply(new ComplexDouble (0.5, 0.0));
	}
	
	public ComplexDouble tanh () {
		ComplexDouble a = new ComplexDouble (2.0, 0.0).divide(this.add(this).exp().add(ONE));
		return ONE.subtract(a);
	}
	
	public ComplexDouble arsinh () {
		ComplexDouble sqrt = this.sqr().add(ONE).sqrt();
		return sqrt.add(this).ln();
	}
	
	public ComplexDouble arcosh () {
		ComplexDouble sqrt = this.sqr().subtract(ONE).sqrt();
		return sqrt.add(this).ln();
	}
	
	public ComplexDouble artanh () {
		ComplexDouble a = ONE.add(this);
		ComplexDouble b = ONE.subtract(this);
		return a.divide(b).ln().divide(new ComplexDouble (2, 0));
	}
}
