/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.math.RoundingMode;


public class NTools {
	
	/**
	 * Prevent the instantiation of this class.
	 */
	private NTools () {}
	
	/**
	 * Method for parsing a complex number of the format "a + bi" from a string.
	 * @param complex is the complex number represented by a string
	 * @return an array of Strings; the real and imaginary part of the number are separated
	 * into two items
	 */
	protected static String [] complexParser (String complex) {
		if (complex.contains(" ") == false) {
			String [] array = new String [2];
			array [0] = complex;
			array [1] = "0";
			return array;
		} else {
			int space = complex.indexOf(" ");
			String real = complex.substring(0, space);
			String imaginary = complex.substring(space + 3, complex.length() - 1);
			String [] array = new String [2];
			array [0] = real;
			array [1] = imaginary;
			if (complex.contains(" - ")) {
				array [1] = "-" + array [1];
			}
			return array;
		}
	}
	
	/**
	 * Converts a BigDecimal number into a string represented by a mantissa and an exponent. The
	 * mantissa is always a number between 1 and 10 (if the number is equal to 0, the output
	 * string is "0".
	 * @param value is the BigDecimal variable that is to be converted into a string
	 * @return the string in scientific notation that corresponds to the BigDecimal argument
	 */
	public static String toScientificString (BigDecimal value) {
		value = value.stripTrailingZeros();
		int mag = Functions.digitsm1(value);
		if (value.signum() == 0) {
			return BigDecimal.ZERO.toPlainString();
		}
		if (value.precision() == 1) {
			if (value.abs().compareTo(BigDecimal.ONE) >= 0) {
				return value.unscaledValue().toString() + "E+" + mag;
			} else {
				mag *= -1;
				return value.unscaledValue().toString() + "E-" + mag;
			}
		} else {
			if (value.abs().compareTo(BigDecimal.ONE) > 0) {
				if (value.signum() > 0) {
					String uv = value.unscaledValue().toString();
					char fd = uv.charAt(0);
					return fd + "." + uv.substring(1) + "E+" + mag;
				} else {
					String uv = value.unscaledValue().toString();
					char fd = uv.charAt(1);
					return "-" + fd + "." + uv.substring(2) + "E+" + mag;
				}
			} else {
				mag *= -1;
				if (value.signum() > 0) {
					String uv = value.unscaledValue().toString();
					char fd = uv.charAt(0);
					return fd + "." + uv.substring(1) + "E-" + mag;
				} else {
					String uv = value.unscaledValue().toString();
					char fd = uv.charAt(1);
					return "-" + fd + "." + uv.substring(2) + "E-" + mag;
				}
			}
		}
	}
	
	public static String toScientificString (BigDecimal arg, MathContext mc) {
		return toScientificString(arg.round(mc));
	}
	
	public static BigDecimal parseBDFromTXTFile (String path) throws IOException, NumberFormatException {
		Path p = Path.of(path);
		String value = Files.readString(p, StandardCharsets.US_ASCII);
		try {
			return new BigDecimal (value);
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException ("The number specified in the file " + p.getFileName() + "is not correctly formatted!");
		}
	}
	
	public static BigDecimal parseBDFromTXTFile (Path path) throws IOException, NumberFormatException {
		String value = Files.readString(path, StandardCharsets.US_ASCII);
		try {
			return new BigDecimal (value);
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException ("The number specified in the file " + path.getFileName() + "is not correctly formatted!");
		}
	}
	
	public static BigInteger parseBIFromTXTFile (String path) throws IOException, NumberFormatException {
		Path p = Path.of(path);
		String value = Files.readString(p, StandardCharsets.US_ASCII);
		try {
			return new BigInteger (value);
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException ("The number specified in the file " + p.getFileName() + "is not correctly formatted!");
		}
	}
	
	public static BigInteger parseBIFromTXTFile (Path path) throws IOException, NumberFormatException {
		String value = Files.readString(path, StandardCharsets.US_ASCII);
		try {
			return new BigInteger (value);
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException ("The number specified in the file " + path.getFileName() + "is not correctly formatted!");
		}
	}
	
	public static void bDToFile (BigDecimal arg, String name)
			throws IOException
	{
		String value = arg.toString();
		BufferedWriter writer = new BufferedWriter (new FileWriter (name));
		writer.write(value);
		writer.close();
	}
	
	public static void bIToFile (BigInteger arg, String name)
			throws IOException
	{
		String value = arg.toString();
		BufferedWriter writer = new BufferedWriter (new FileWriter (name));
		writer.write(value);
		writer.close();
	}
	
	public static int magnitude (BigDecimal arg) {
		return Functions.digitsm1(arg);
	}
	
	public static BigDecimal roundFixedPoint (BigDecimal arg, int e, RoundingMode rm) {
		return arg.setScale(arg.scale() + e, rm);
	}
	
	public static BigDecimal roundFixedPoint (BigDecimal arg, int e) {
		return roundFixedPoint(arg, e, RoundingMode.HALF_UP);
	}
	
	public static BigDecimal roundToInt (BigDecimal arg, RoundingMode rm) {
		return roundFixedPoint(arg, 0, rm);
	}
	
	public static BigDecimal roundToInt (BigDecimal arg) {
		return roundToInt(arg, RoundingMode.HALF_UP);
	}
	
	public static BigDecimal stripTrailingZeros (BigDecimal arg) {
		return Functions.stripTrailingZeros(arg, false);
	}
}
