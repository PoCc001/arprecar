/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

import java.io.IOException;
import java.math.*;
import java.nio.file.Path;

public class ADecimal implements Comparable<ADecimal> {
	private BigDecimal value;
	private int infinity;
	private boolean nan;
	private static MathContext prec = new MathContext (25);
	private static int internalPrecision = 5;
	
	private static final ADecimal DOUBLE_MAX = new ADecimal (Double.MAX_VALUE);
	private static final ADecimal DOUBLE_MIN = new ADecimal (Double.MIN_VALUE);
	private static final ADecimal FLOAT_MAX = new ADecimal (Float.MAX_VALUE);
	private static final ADecimal FLOAT_MIN = new ADecimal (Float.MIN_VALUE);
	
	public static final ADecimal ZERO = new ADecimal (BigDecimal.ZERO);
	public static final ADecimal ONE = new ADecimal (BigDecimal.ONE);
	public static final ADecimal TEN = new ADecimal (BigDecimal.TEN);
	
	// will be made public one day
	private static final ADecimal POS_INFINITY = new ADecimal (null, 1, false);
	private static final ADecimal NEG_INFINITY = new ADecimal (null, -1, false);
	private static final ADecimal NaN = new ADecimal (null, 0, true);
	
	private static MathContext precm10 () {
		return new MathContext (prec.getPrecision() - internalPrecision);
	}
	
	private ADecimal (BigDecimal number, int inf, boolean n) {
		value = number;
		infinity = inf;
		nan = n;
	}
	
	public ADecimal () {
		value = BigDecimal.ZERO;
		infinity = 0;
		nan = false;
	}
	
	public ADecimal (String number) {
		value = new BigDecimal (number);
		infinity = 0;
		nan = false;
	}
	
	public ADecimal (int number) {
		value = new BigDecimal (number);
		infinity = 0;
		nan = false;
	}
	
	public ADecimal (long number) {
		value = new BigDecimal (number);
		infinity = 0;
		nan = false;
	}
	
	public ADecimal (double number) {
		if (Double.isInfinite(number)) {
			throw new IllegalArgumentException ("Infinite number!");
		}
		if (Double.isNaN(number)) {
			throw new IllegalArgumentException ("NaN value!");
		}
		value = BigDecimal.valueOf(number);
		infinity = 0;
		nan = false;
	}
	
	public ADecimal (short number) {
		value = new BigDecimal (number);
		infinity = 0;
		nan = false;
	}
	
	public ADecimal (byte number) {
		value = new BigDecimal (number);
		infinity = 0;
		nan = false;
	}
	
	public ADecimal (float number) {
		if (Float.isInfinite(number)) {
			throw new IllegalArgumentException ("Infinite number!");
		}
		if (Float.isNaN(number)) {
			throw new IllegalArgumentException ("NaN value!");
		}
		value = BigDecimal.valueOf(number);
		infinity = 0;
		nan = false;
	}
	
	public ADecimal (BigDecimal number) {
		value = number;
		infinity = 0;
		nan = false;
	}
	
	public ADecimal (BigInteger number) {
		value = new BigDecimal (number);
		infinity = 0;
		nan = false;
	}
	
	public static ADecimal fromTXTFile (String path) throws IOException, NumberFormatException {
		return new ADecimal (NTools.parseBDFromTXTFile(path));
	}
	
	public static ADecimal fromTXTFile (Path path) throws IOException, NumberFormatException {
		return new ADecimal (NTools.parseBDFromTXTFile(path));
	}
	
	public void toFile (String name)
			throws IOException
	{
		NTools.bDToFile(this.value.round(prec), name);
	}
	
	public static void setPrec (int p) {
		if (p > 0) {
			prec = new MathContext (p + internalPrecision);
		} else {
			throw new IllegalArgumentException ("The precision must be a non-negative number!");
		}
	}
	
	public static void setIP (int iP) {
		if (prec.getPrecision() + iP > 0) {
			prec = precm10();
			internalPrecision = iP;
			prec = new MathContext (prec.getPrecision() + internalPrecision);
		} else {
			throw new IllegalArgumentException ("The overall precision (prec + iP) must be non-negative!");
		}
	}
	
	public static int getPrec () {
		return prec.getPrecision() - internalPrecision;
	}
	
	public static int getIP () {
		return internalPrecision;
	}
	
	public ADecimal add (ADecimal arg) {
		return new ADecimal (this.value.add(arg.value, prec));
	}
	
	public ADecimal subtract (ADecimal arg) {
		return new ADecimal (this.value.subtract(arg.value, prec));
	}
	
	public ADecimal multiply (ADecimal arg) {
		return new ADecimal (this.value.multiply(arg.value, prec));
	}
	
	public ADecimal divide (ADecimal arg) {
		try {
			return new ADecimal (this.value.divide(arg.value, prec));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public boolean isInteger () {
		return Functions.isInteger(value.round(prec));
	}
	
	@Override
	public String toString () {
		if (infinity == 0 && !nan) {
			return value.round(precm10()).toString();
		} else if (infinity == 1) {
			return "POSITIVE INFINITY";
		} else if (infinity == -1) {
			return "NEGATIVE INFINITY";
		} else {
			return "NaN";
		}
	}
	
	public String toPlainString () {
		if (infinity == 0 && !nan) {
			return value.round(precm10()).toPlainString();
		} else if (infinity == 1) {
			return "POSITIVE INFINITY";
		} else if (infinity == -1) {
			return "NEGATIVE INFINITY";
		} else {
			return "NaN";
		}
	}
	
	public String toEngineeringString () {
		if (infinity == 0 && !nan) {
			return value.round(precm10()).toEngineeringString();
		} else if (infinity == 1) {
			return "POSITIVE INFINITY";
		} else if (infinity == -1) {
			return "NEGATIVE INFINITY";
		} else {
			return "NaN";
		}
	}
	
	public String toScientificString () {
		if (infinity == 0 && !nan) {
			return NTools.toScientificString(this.value, precm10());
		} else if (infinity == 1) {
			return "POSITIVE INFINITY";
		} else if (infinity == -1) {
			return "NEGATIVE INFINITY";
		} else {
			return "NaN";
		}
	}
	
	public long toLong () {
		if (infinity == 0 && !nan) {
			try {
				return value.round(precm10()).longValueExact();
			} catch (ArithmeticException ae) {
				throw new IllegalArgumentException ("Cannot convert this to long!");
			}
		} else {
			throw new IllegalArgumentException ("Cannot convert this to long!");
		}
	}
	
	public int toInt () {
		if (infinity == 0 && !nan) {
			try {
				return value.round(precm10()).intValueExact();
			} catch (ArithmeticException ae) {
				throw new IllegalArgumentException ("Cannot convert this to int!");
			}
		} else {
			throw new IllegalArgumentException ("Cannot convert this to int!");
		}
	}
	
	public short toShort () {
		if (infinity == 0 && !nan) {
			try {
				return value.round(precm10()).shortValueExact();
			} catch (ArithmeticException ae) {
				throw new ArithmeticException ("Cannot convert this to short!");
			}
		} else {
			throw new IllegalArgumentException ("Cannot convert this to short!");
		}
	}
	
	public byte toByte () {
		if (infinity == 0 && !nan) {
			try {
				return value.round(precm10()).byteValueExact();
			} catch (ArithmeticException ae) {
				throw new ArithmeticException ("Cannot convert this to byte!");
			}
		} else {
			throw new IllegalArgumentException ("Cannot convert this to byte!");
		}
	}
	
	public double toDouble () {
		if (infinity == 1) {
			return Double.POSITIVE_INFINITY;
		} else if (infinity == -1) {
			return Double.NEGATIVE_INFINITY;
		} else if (nan) {
			return Double.NaN;
		} else if (this.smallerOrEqual(DOUBLE_MAX) && this.largerOrEqual(DOUBLE_MIN)) {
			return this.value.doubleValue();
		} else {
			throw new IllegalArgumentException ("Cannot convert this to double!");
		}
	}
	
	public float toFloat () {
		if (infinity == 1) {
			return Float.POSITIVE_INFINITY;
		} else if (infinity == -1) {
			return Float.NEGATIVE_INFINITY;
		} else if (nan) {
			return Float.NaN;
		} else if (this.smallerOrEqual(FLOAT_MAX) && this.largerOrEqual(FLOAT_MIN)) {
			return this.value.floatValue();
		} else {
			throw new IllegalArgumentException ("Cannot convert this to float!");
		}
	}
	
	@Override
	public int compareTo (ADecimal arg) {
		return this.value.compareTo(arg.value);
	}
	
	public boolean larger (ADecimal arg) {
		if (this.equals(POS_INFINITY)) {
			return true;
		} else if (this.equals(NEG_INFINITY)) {
			return false;
		} else if (this.equals(NaN) || arg.equals(NaN)) {
			throw new IllegalArgumentException ("Cannot compare two numbers when one is NaN!");
		}
		return this.value.round(precm10()).compareTo(arg.value.round(precm10())) > 0;
	}
	
	public boolean largerOrEqual (ADecimal arg) {
		return this.value.round(precm10()).compareTo(arg.value.round(precm10())) >= 0;
	}
	
	public boolean smaller (ADecimal arg) {
		return this.value.round(precm10()).compareTo(arg.value.round(precm10())) < 0;
	}
	
	public boolean smallerOrEqual (ADecimal arg) {
		return this.value.round(precm10()).compareTo(arg.value.round(precm10())) <= 0;
	}
	
	public boolean equalValue (ADecimal arg) {
		return this.value.round(precm10()).compareTo(arg.value.round(precm10())) == 0;
	}
	
	public ADecimal sqrt () {
		try {
			return new ADecimal (Arbitrary.sqrt(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal sqrtExact () {
		try {
			return new ADecimal (Arbitrary.sqrtExact(this.value, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal power (ADecimal arg) {
		return new ADecimal (Arbitrary.power(this.value, arg.value, prec, 0));
	}
	
	public ADecimal power (int arg) {
		return new ADecimal (this.value.pow(arg, prec));
	}
	
	public ADecimal sqr () {
		return new ADecimal (this.value.pow(2, prec));
	}
	
	public ADecimal cube () {
		return new ADecimal (this.value.pow(3, prec));
	}
	
	public static ADecimal pi () {
		if (prec.getPrecision() <= 15) {
			return new ADecimal (StrictMath.PI);
		} else {
			return new ADecimal (Arbitrary.pi(prec, 0));
		}
	}
	
	public static ADecimal e () {
		if (prec.getPrecision() <= 15) {
			return new ADecimal (StrictMath.E);
		} else {
			return new ADecimal (Arbitrary.e(prec, 0));
		}
	}
	
	public static ADecimal lemniscate () {
		return new ADecimal (Arbitrary.lemniscate(prec, 0));
	}
	
	public ADecimal exp () {
		return new ADecimal (Arbitrary.exp(this.value, prec, 0));
	}
	
	public ADecimal ln () {
		try {
			return new ADecimal (Arbitrary.ln(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal lg () {
		try {
			return new ADecimal (Arbitrary.lg(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal log (ADecimal arg) {
		try {
			return new ADecimal (Arbitrary.log(this.value, arg.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal sin () {
		return new ADecimal (Arbitrary.sin(this.value, prec, 0));
	}
	
	public ADecimal sind () {
		return new ADecimal (Arbitrary.sin(this.value, "deg", prec, 0));
	}
	
	public ADecimal sing () {
		return new ADecimal (Arbitrary.sin(this.value, "grad", prec, 0));
	}
	
	public ADecimal cos () {
		return new ADecimal (Arbitrary.cos(this.value, prec, 0));
	}
	
	public ADecimal cosd () {
		return new ADecimal (Arbitrary.cos(this.value, "deg", prec, 0));
	}
	
	public ADecimal cosg () {
		return new ADecimal (Arbitrary.cos(this.value, "grad", prec, 0));
	}
	
	public ADecimal tan () {
		try {
			return new ADecimal (Arbitrary.tan(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal tand () {
		try {
			return new ADecimal (Arbitrary.tan(this.value, "deg", prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal tang () {
		try {
			return new ADecimal (Arbitrary.tan(this.value, "grad", prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal cot () {
		try {
			return new ADecimal (Arbitrary.cot(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal cotd () {
		try {
			return new ADecimal (Arbitrary.cot(this.value, "deg", prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal cotg () {
		try {
			return new ADecimal (Arbitrary.cot(this.value, "grad", prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public BigDecimal toBigDecimal () {
		return value.round(precm10());
	}
	
	public ADecimal mod (ADecimal arg) {
		if (this.isInteger() && arg.isInteger() && this.larger(ZERO) && arg.larger(ZERO)) {
			return new ADecimal (this.value.remainder(arg.value, prec));
		} else {
			throw new ArithmeticException ("Both numbers must be positive integers!");
		}
	}
	
	public ADecimal sinh () {
		return new ADecimal (Arbitrary.sinh(this.value, prec, 0));
	}
	
	public ADecimal cosh () {
		return new ADecimal (Arbitrary.cosh(this.value, prec, 0));
	}
	
	public ADecimal tanh () {
		return new ADecimal (Arbitrary.tanh(this.value, prec, 0));
	}
	
	public ADecimal asin () {
		try {
			return new ADecimal (Arbitrary.asin(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal asind () {
		try {
			return new ADecimal (Arbitrary.asin(this.value, "deg", prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal [] asind (int pref) {
		if (pref == 1) {
			ADecimal [] result = new ADecimal [1];
			result [0] = this.asind();
			return result;
		} else if (pref == 2) {
			ADecimal [] result = new ADecimal [2];
			ADecimal r = this.asind();
			result [0] = new ADecimal (r.value.toBigInteger());
			result [1] = r.subtract(result [0]).multiply(new ADecimal ("60"));
			return result;
		} else if (pref == 3) {
			ADecimal [] result = new ADecimal [3];
			ADecimal r = this.asind();
			result [0] = new ADecimal (r.value.toBigInteger());
			result [1] = new ADecimal (r.subtract(result [0]).multiply(new ADecimal ("60")).value.toBigInteger());
			result [2] = r.subtract(result [0]).subtract(result [1].divide(new ADecimal ("60")));
			result [2] = result [2].multiply(new ADecimal ("3600"));
			return result;
		} else {
			throw new IllegalArgumentException ("Integer must be either 1, 2 or 3!");
		}
	}
	
	public ADecimal [] asind (int pref, boolean dec) {
		ADecimal [] array = this.asind(pref);
		if (dec == true) {
			return array;
		} else {
			array [array.length - 1] = new ADecimal (array [array.length - 1].value.toBigInteger());
			return array;
		}
	}
	
	public ADecimal asing () {
		try {
			return new ADecimal (Arbitrary.asin(this.value, "grad", prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal acos () {
		try {
			return new ADecimal (Arbitrary.acos(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal acosd () {
		try {
			return new ADecimal (Arbitrary.acos(this.value, "deg", prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal [] acosd (int pref) {
		if (pref == 1) {
			ADecimal [] result = new ADecimal [1];
			result [0] = this.acosd();
			return result;
		} else if (pref == 2) {
			ADecimal [] result = new ADecimal [2];
			ADecimal r = this.acosd();
			result [0] = new ADecimal (r.value.toBigInteger());
			result [1] = r.subtract(result [0]).multiply(new ADecimal ("60"));
			return result;
		} else if (pref == 3) {
			ADecimal [] result = new ADecimal [3];
			ADecimal r = this.acosd();
			result [0] = new ADecimal (r.value.toBigInteger());
			result [1] = new ADecimal (r.subtract(result [0]).multiply(new ADecimal ("60")).value.toBigInteger());
			result [2] = r.subtract(result [0]).subtract(result [1].divide(new ADecimal ("60")));
			result [2] = result [2].multiply(new ADecimal ("3600"));
			return result;
		} else {
			throw new IllegalArgumentException ("Integer must be either 1, 2 or 3!");
		}
	}
	
	public ADecimal [] acosd (int pref, boolean dec) {
		ADecimal [] array = this.acosd(pref);
		if (dec == true) {
			return array;
		} else {
			array [array.length - 1] = new ADecimal (array [array.length - 1].value.toBigInteger());
			return array;
		}
	}
	
	public ADecimal acosg () {
		try {
			return new ADecimal (Arbitrary.acos(this.value, "grad", prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal atan () {
		return new ADecimal (Arbitrary.atan(this.value, prec, 0));
	}
	
	public ADecimal atand () {
		return new ADecimal (Arbitrary.atan(this.value, "deg", prec, 0));
	}
	
	public ADecimal [] atand (int pref) {
		if (pref == 1) {
			ADecimal [] result = new ADecimal [1];
			result [0] = this.atand();
			return result;
		} else if (pref == 2) {
			ADecimal [] result = new ADecimal [2];
			ADecimal r = this.atand();
			result [0] = new ADecimal (r.value.toBigInteger());
			result [1] = r.subtract(result [0]).multiply(new ADecimal ("60"));
			return result;
		} else if (pref == 3) {
			ADecimal [] result = new ADecimal [3];
			ADecimal r = this.atand();
			result [0] = new ADecimal (r.value.toBigInteger());
			result [1] = new ADecimal (r.subtract(result [0]).multiply(new ADecimal ("60")).value.toBigInteger());
			result [2] = r.subtract(result [0]).subtract(result [1].divide(new ADecimal ("60")));
			result [2] = result [2].multiply(new ADecimal ("3600"));
			return result;
		} else {
			throw new IllegalArgumentException ("Integer must be either 1, 2 or 3!");
		}
	}
	
	public ADecimal [] atand (int pref, boolean dec) {
		ADecimal [] array = this.atand(pref);
		if (dec == true) {
			return array;
		} else {
			array [array.length - 1] = new ADecimal (array [array.length - 1].value.toBigInteger());
			return array;
		}
	}
	
	public ADecimal atang () {
		return new ADecimal (Arbitrary.atan(this.value, "grad", prec, 0));
	}
	
	public ADecimal acot () {
		return new ADecimal (Arbitrary.acot(this.value, prec, 0));
	}
	
	public ADecimal acotd () {
		return new ADecimal (Arbitrary.acot(this.value, "deg", prec, 0));
	}
	
	public ADecimal [] acotd (int pref) {
		if (pref == 1) {
			ADecimal [] result = new ADecimal [1];
			result [0] = this.acotd();
			return result;
		} else if (pref == 2) {
			ADecimal [] result = new ADecimal [2];
			ADecimal r = this.acotd();
			result [0] = new ADecimal (r.value.toBigInteger());
			result [1] = r.subtract(result [0]).multiply(new ADecimal ("60"));
			return result;
		} else if (pref == 3) {
			ADecimal [] result = new ADecimal [3];
			ADecimal r = this.acotd();
			result [0] = new ADecimal (r.value.toBigInteger());
			result [1] = new ADecimal (r.subtract(result [0]).multiply(new ADecimal ("60")).value.toBigInteger());
			result [2] = r.subtract(result [0]).subtract(result [1].divide(new ADecimal ("60")));
			result [2] = result [2].multiply(new ADecimal ("3600"));
			return result;
		} else {
			throw new IllegalArgumentException ("Integer must be either 1, 2 or 3!");
		}
	}
	
	public ADecimal [] acotd (int pref, boolean dec) {
		ADecimal [] array = this.acotd(pref);
		if (dec == true) {
			return array;
		} else {
			array [array.length - 1] = new ADecimal (array [array.length - 1].value.toBigInteger());
			return array;
		}
	}
	
	public ADecimal acotg () {
		return new ADecimal (Arbitrary.acot(this.value, "grad", prec, 0));
	}
	
	public ADecimal arsinh () {
		return new ADecimal (Arbitrary.arsinh(this.value, prec, 0));
	}
	
	public ADecimal arcosh () {
		try {
			return new ADecimal (Arbitrary.arcosh(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal artanh () {
		try {
			return new ADecimal (Arbitrary.artanh(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal factorial () {
		try {
			return new ADecimal (Arbitrary.factorial(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal powTen () {
		return new ADecimal (Arbitrary.powTen(this.value, prec, 0));
	}
	
	public ADecimal stripTrailingZeros () {
		return new ADecimal (NTools.stripTrailingZeros(this.value.round(precm10())));
	}
	
	public ADecimal agm (ADecimal arg) {
		try {
			return new ADecimal (Arbitrary.agm(this.value, arg.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public static String angleToString (ADecimal [] array) {
		if (array.length == 3) {
			return array [0].toString() + " " + array [1].toString() + "' " + array [2].toString() + "''";
		} else if (array.length == 2) {
			return array [0].toString() + " " + array [1].toString() + "'";
		} else if (array.length == 1) {
			return array [0].toString() + "";
		} else {
			throw new IllegalArgumentException ("The array must have either 1, 2 or 3 elements!");
		}
	}
	
	public ADecimal cbrt () {
		return new ADecimal (Arbitrary.cbrt(this.value, prec, 0));
	}
	
	public ADecimal negate () {
		return new ADecimal (this.value.negate());
	}
	
	protected ADecimal movePointLeft (int n) {
		return new ADecimal (value.movePointLeft(n));
	}
	
	protected ADecimal movePointRight (int n) {
		return new ADecimal (value.movePointRight(n));
	}
	
	private static ADecimal [] sorted (ADecimal arg1, ADecimal arg2) {
		ADecimal [] result = new ADecimal [2];
		if (arg1.largerOrEqual(arg2)) {
			result [0] = arg1;
			result [1] = arg2;
		} else {
			result [0] = arg2;
			result [1] = arg1;
		}
		return result;
	}
	
	public static ADecimal max (ADecimal arg1, ADecimal arg2) {
		return sorted(arg1, arg2) [0];
	}
	
	public static ADecimal min (ADecimal arg1, ADecimal arg2) {
		return sorted(arg1, arg2) [1];
	}
	
	public ADecimal binomialCoefficient (ADecimal arg) {
		return new ADecimal (Arbitrary.binomialCoefficient(this.value, arg.value, prec, 0));
	}
	
	public ADecimal cdf () {
		return new ADecimal (Arbitrary.cdf(this.value, prec, 0));
	}
	
	public ADecimal cdf (ADecimal my, ADecimal sigma) {
		return new ADecimal (Arbitrary.cdf(this.value, my.value, sigma.value, prec, 0));
	}
	
	public ADecimal abs () {
		return new ADecimal (this.value.abs(prec));
	}
	
	public int sgn () {
		return this.value.signum();
	}
	
	public ADecimal ulp () {
		return new ADecimal (value.ulp());
	}
	
	public ADecimal probit () {
		try {
			return new ADecimal (Arbitrary.probit(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal invroot (int order) {
		try {
			return new ADecimal (Arbitrary.invroot(this.value, order, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal root (int order) {
		try {
			return new ADecimal (Arbitrary.root(this.value, order, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal atan2 (ADecimal x) {
		return new ADecimal (Arbitrary.atan2(this.value, x.value, prec, 0));
	}
	
	public ADecimal erf () {
		return new ADecimal (Arbitrary.erf(this.value, prec, 0));
	}
	
	public ADecimal erfinv () {
		try {
			return new ADecimal (Arbitrary.erfinv(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
	
	public ADecimal ceil () {
		int mag = this.magnitude();
		if (mag > 0) {
			return new ADecimal (this.value.round(new MathContext (mag + 1, RoundingMode.CEILING)));
		} else {
			if (this.equals(ZERO)) {
				return ZERO;
			} else {
				return ONE;
			}
		}
	}
	
	public ADecimal floor () {
		int mag = this.magnitude();
		if (mag > 0) {
			return new ADecimal (this.value.round(new MathContext (mag + 1, RoundingMode.FLOOR)));
		} else {
			return ZERO; 
		}
	}
	
	public int magnitude () {
		return Functions.digitsm1(this.value);
	}
	
	public ADecimal round () {
		int mag = this.magnitude();
		if (mag > 0) {
			return new ADecimal (this.value.round(new MathContext (mag + 1)));
		} else {
			return ZERO; 
		}
	}
	
	public ADecimal scaleByPowerOfTen (int scale) {
		return new ADecimal (this.value.scaleByPowerOfTen(scale));
	}
	
	public ADecimal plus () {
		return this;
	}
	
	public ADecimal normalized () {
		return new ADecimal (this.value.scaleByPowerOfTen(-Functions.digitsm1(this.value)));
	}
	
	public ADecimal hypot (ADecimal arg) {
		return new ADecimal (Arbitrary.hypot(this.value, arg.value, prec, 0));
	}
	
	public ADecimal roundToInt (RoundingMode rm) {
		return new ADecimal (NTools.roundToInt(this.value, rm));
	}
	
	public ADecimal roundToInt () {
		return new ADecimal (NTools.roundToInt(this.value));
	}
	
	public ADecimal subfactorial () {
		if (this.isInteger()) {
			return new ADecimal (Arbitrary.subfactorial(this.value.toBigInteger(), prec, 0));
		} else {
			throw new ArithmeticException ("Cannot calculate the subfactorial of a fractional value!");
		}
	}

	public static ADecimal eulerMascheroni () {
		return new ADecimal (Arbitrary.eulerMascheroni(prec, 0));
	}
	
	public ADecimal lambertW () {
		try {
			return new ADecimal (Arbitrary.lambertW(this.value, prec, 0));
		} catch (ArithmeticException ae) {
			throw new ArithmeticException (ae.getMessage());
		}
	}
}
