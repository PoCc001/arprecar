/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

import java.math.*;

/**
 * Package internal class for a way of calculating with floating-point numbers a
 * bit more efficiently than BigDecimal does. It avoids constantly multiplying
 * and dividing by powers of 10 for rounding and scaling.
 * 
 * @author Johannes Kloimböck
 *
 */
class BigFloat extends Number implements Comparable<BigFloat> {
	/**
	 * Serial number of BigFloat
	 */
	private static final long serialVersionUID = 1809052227338232979L;
	
	private BigInteger intVal;
	private int scale;

	protected static final double lg2 = StrictMath.log10(2.0);
	protected static final double ln2 = StrictMath.log(2.0);
	protected static final BigFloat DOUBLE_MIN_NORMAL = new BigFloat(Double.MIN_NORMAL);
	public static final BigFloat ONE = new BigFloat(BigInteger.ONE, 0);
	public static final BigFloat ZERO = new BigFloat(BigInteger.ZERO, 0);
	public static final BigFloat TWO = new BigFloat(BigInteger.TWO, 0);
	public static final BigFloat TEN = new BigFloat(BigInteger.TEN, 0);

	public BigFloat(long val) {
		intVal = BigInteger.valueOf(val);
		scale = 0;
	}

	public BigFloat(double val) {
		if (val == 0.0) {
			intVal = BigInteger.ZERO;
			scale = 0;
		}

		if (val > 0.0) {
			long lVal = Double.doubleToRawLongBits(val);

			if (val < Double.MIN_NORMAL) {
				intVal = BigInteger.valueOf(lVal & 0xfffffffffffffL);
				scale = 1074;
			} else {
				intVal = BigInteger.valueOf(lVal & 0xfffffffffffffL | (1L << 52));
				int exp = (int) (lVal >> 52);
				scale = 1075 - exp;
			}
		} else {
			long lVal = Double.doubleToRawLongBits(-val);

			if (val > -Double.MIN_NORMAL) {
				intVal = BigInteger.valueOf(lVal & 0xfffffffffffffL);
				scale = 1074;
			} else {
				intVal = BigInteger.valueOf(lVal & 0xfffffffffffffL | (1L << 52));
				int exp = (int) (lVal >> 52);
				scale = 1075 - exp;
			}

			intVal = intVal.negate();
		}
	}

	/*
	 for performance reasons it's not recommended to initialize a BigFloat with a
	 String*/
	public BigFloat(String val) {
		this(new BigDecimal(val), new BigDecimal(val).unscaledValue().bitLength());
	}

	public BigFloat(BigDecimal val, int precision) {
		if (val.scale() < 0) {
			val = val.setScale(0);
			intVal = val.unscaledValue();
			scale = 0;
		} else if (val.scale() > 0) {
			BigFloat powTen = new BigFloat(BigInteger.TEN.pow(val.scale()));
			BigFloat t = new BigFloat(val.unscaledValue());
			t = t.divide(powTen, precision);
			intVal = t.intVal;
			scale = t.scale;
		} else {
			intVal = val.unscaledValue();
		}
	}

	public BigFloat(BigInteger val) {
		intVal = val;
		scale = 0;
	}

	public BigFloat(BigInteger val, int s) {
		intVal = val;
		scale = s;
	}

	public BigInteger unscaledValue() {
		return intVal;
	}

	public BigFloat setScale(int s) {
		return new BigFloat(intVal.shiftLeft(s - scale), s);
	}
	
	public BigInteger toBigInteger() {
		return setScale(0).unscaledValue();
	}

	public int scale() {
		return scale;
	}

	public BigFloat round(int precision) {
		int newScale = scale() + precision - precision();
		return setScale(newScale);
	}
	
	public BigFloat roundToMin(int precision) {
		if (precision > precision()) {
			return round(precision);
		} else {
			return this;
		}
	}
	
	public BigFloat roundToMax(int precision) {
		if (precision < precision()) {
			return round(precision);
		} else {
			return this;
		}
	}

	public BigFloat add(BigFloat other) {
		if (scale >= other.scale) {
			other = other.setScale(scale);
			return new BigFloat(intVal.add(other.intVal), scale);
		} else {
			BigFloat t = setScale(other.scale);
			return new BigFloat(t.intVal.add(other.intVal), other.scale);
		}
	}

	public BigFloat negate() {
		return new BigFloat(intVal.negate(), scale);
	}

	public int signum() {
		return intVal.signum();
	}

	public BigFloat abs() {
		return new BigFloat(intVal.abs(), scale);
	}

	public BigFloat subtract(BigFloat other) {
		return add(other.negate());
	}

	public int precision() {
		return bitLength();
	}

	public int magnitude() {
		return precision() - scale - 1;
	}

	public BigFloat multiply(BigFloat other) {
		return new BigFloat(intVal.multiply(other.intVal), scale + other.scale);
	}

	public BigFloat multiply(BigFloat other, int precision) {
		BigFloat t = roundToMax(precision);
		other = other.roundToMax(precision);
		return t.multiply(other).roundToMax(precision);
	}
	
	public BigFloat sqr() {
		return multiply(this);
	}
	
	public BigFloat sqr(int precision) {
		BigFloat t = roundToMax(precision);
		return t.multiply(t).roundToMax(precision);
	}

	public int bitLength() {
		return signum() < 0 ? intVal.subtract(BigInteger.ONE).bitLength() : intVal.bitLength();
	}

	public BigFloat normalize() {
		return new BigFloat(intVal, bitLength() - 1);
	}

	public BigFloat reciproc(int precision) {
		double v = roundToMax(precision).normalize().doubleValue();
		v = 1.0 / v;
		BigFloat approx = new BigFloat(v);
		approx = new BigFloat(approx.intVal, approx.scale() + magnitude());
		
		int iterations = (int)(StrictMath.log(precision) / ln2);
		int p = (precision >> (iterations - 1)) + 5;
		
		BigFloat product = ONE;
		
		for (int i = 0; i < iterations; i++) {
			product = this.round(p).multiply(approx).round(p);
			approx = approx.add(approx.multiply(ONE.subtract(product), p >> 1));
			p = (precision >> (iterations - i - 2)) + 5;
		}
		
		return approx.round(precision);
	}
	
	@SuppressWarnings("unused")
	private BigFloat divideNewton(BigFloat other, int precision) {
		return this.multiply(other.reciproc(precision + 3), precision);
	}
	
	private BigFloat divideBigInteger(BigFloat other, int precision) {
		other = other.roundToMax(precision + 2);
		BigFloat t = round(precision + other.intVal.bitLength() + 2);
		return new BigFloat(t.intVal.divide(other.intVal), t.scale - other.scale);
	}

	public BigFloat divide(BigFloat other, int precision) {
		return divideBigInteger(other, precision);
	}
	
	public BigFloat divideScale(BigFloat other, int newScale) {
		BigFloat t = setScale(newScale + other.scale());
		return new BigFloat(t.intVal.divide(other.intVal), t.scale - other.scale);
	}
	
	public BigFloat divideToIntegralValue(BigFloat other) {
		return divideScale(other, 0);
	}

	public BigFloat shiftLeft(int s) {
		return new BigFloat(intVal, scale - s);
	}

	public BigFloat shiftRight(int s) {
		return new BigFloat(intVal, scale + s);
	}
	
	public BigFloat stripTrailingZeros() {
		if (signum() == 0) {
			return ZERO;
		}
		
		int strip = intVal.getLowestSetBit();
		return new BigFloat(intVal.shiftRight(strip), scale - strip);
	}
	
	public BigFloat pow(int p) {
		return new BigFloat(intVal.pow(p), scale * p);
	}
	
	public BigFloat pow(int p, int precision) {
		if (p == 0) {
			return ONE;
		} else if (p == 1) {
			return roundToMax(precision);
		} else if (p == 2) {
			return sqr(precision);
		} else if (p > 0) {			
			BigFloat shorter = stripTrailingZeros();
			BigFloat power = (p & 1) == 0 ? ONE : shorter;
			p >>= 1;
			precision += 10;
			BigFloat square = shorter.sqr(precision);
			int length = 32 - Integer.numberOfLeadingZeros(p);
			
			for (int i = 1; i <= length; i++) {
				if ((p & 1) == 1) {
					power = power.multiply(square, precision);
				}
				
				square = square.sqr(precision);
				p >>= 1;
			}
			
			return power.round(precision - 10);
		} else {
			if (p == Integer.MIN_VALUE) {
				throw new ArithmeticException("Exponent is too small!");
			}
			
			return pow(-p, precision).reciproc(precision);
		}
	}
	
	@Override
	public int compareTo(BigFloat o) {
		if (signum() != o.signum()) {
			return signum() > o.signum() ? 1 : -1;
		} else if (magnitude() != o.magnitude()) {
			return magnitude() > o.magnitude() ? signum() : -signum();
		} else {
			BigFloat t = setScale(StrictMath.max(scale, o.scale));
			BigFloat os = o.setScale(StrictMath.max(scale, o.scale));
			return t.intVal.compareTo(os.intVal);
		}
	}

	public BigDecimal toBigDecimal() {
		if (scale == 0) {
			return new BigDecimal(intVal);
		}

		if (scale < 0) {
			return new BigDecimal(intVal.shiftLeft(-scale));
		} else {
			BigDecimal bdVal = new BigDecimal(intVal);
			BigDecimal halfPow = BigDecimal.valueOf(5, 1).pow(scale);
			return bdVal.multiply(halfPow);
		}
	}
	
	@Override
	public String toString() {
		return toBigDecimal().round(new MathContext((int) ((double) (intVal.bitLength()) * lg2))).toString();
	}
	
	public String toBinaryString() {
		String str = intVal.toString(2);

		if (scale == 0) {
			return str;
		} else if (scale < 0) {
			return setScale(0).toBinaryString();
		} else {
			BigFloat i = setScale(0);
			String iStr = i.toBinaryString();
			iStr = iStr + ".";
			int zeros = scale() - precision();
			
			String z = "";
			for (int c = 0; c < zeros; ++c) {
				z += "0";
			}
			
			iStr = iStr + z + this.subtract(i).unscaledValue().abs().toString(2);

			return iStr;
		}
	}

	@Override
	public int intValue() {
		return toBigInteger().intValue();
	}

	@Override
	public long longValue() {
		return toBigInteger().longValue();
	}

	@Override
	public float floatValue() {
		if (signum() == 0) {
			return 0.0f;
		} else if (magnitude() < -126) {
			if (magnitude() < -149) {
				return 0.0f;
			}
			int lv = round(magnitude() + 150).unscaledValue().abs().intValue();
			return Float.intBitsToFloat(lv) * signum();
		}
		
		int lv = round(24).unscaledValue().abs().intValue() & ((1 << 23) - 1);
		int exp = magnitude();
		exp += 127;
		lv |= exp << 23;
		return Float.intBitsToFloat(lv) * signum();
	}
	
	@Override
	public double doubleValue() {
		if (signum() == 0) {
			return 0.0;
		} else if (magnitude() < -1022) {
			if (magnitude() < -1074) {
				return 0.0;
			}
			long lv = round(magnitude() + 1075).unscaledValue().abs().longValue();
			return Double.longBitsToDouble(lv) * signum();
		}
		
		long lv = round(53).unscaledValue().abs().longValue() & ((1L << 52) - 1);
		int exp = magnitude();
		exp += 1023;
		lv |= (long)(exp) << 52;
		return Double.longBitsToDouble(lv) * signum();
	}
}
