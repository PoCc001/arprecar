/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

import java.math.BigInteger;
import java.math.BigDecimal;
import java.math.MathContext;

public class RationalNumber implements Comparable<RationalNumber>{
	private BigInteger numerator;
	private BigInteger denominator;
	private boolean negative;
	
	public static final RationalNumber ZERO = new RationalNumber (BigInteger.ZERO, BigInteger.ONE, false);
	public static final RationalNumber ONE = new RationalNumber (BigInteger.ONE, BigInteger.ONE, false);
	public static final RationalNumber HALF = new RationalNumber (BigInteger.ONE, BigInteger.TWO, false);
	
	private void parseFromLongs (long n, long d) {
		numerator = BigInteger.valueOf(n);
		denominator = BigInteger.valueOf(d);
		if ((n < 0 || d < 0) && !(n < 0 && d < 0)) {
			negative = true;
		}
		numerator = numerator.abs();
		denominator = denominator.abs();
	}
	
	public RationalNumber (long n, long d) {
		if (d != 0) {
			parseFromLongs(n, d);
			BigInteger gcd = gcd(numerator, denominator);
			numerator = numerator.divide(gcd);
			denominator = denominator.divide(gcd);
		} else {
			throw new ArithmeticException ("Denominator is 0!");
		}
	}
	
	public RationalNumber (int n, int d) {
		if (d != 0) {
			parseFromLongs(n, d);
			BigInteger gcd = gcd(numerator, denominator);
			numerator = numerator.divide(gcd);
			denominator = denominator.divide(gcd);
		} else {
			throw new ArithmeticException ("Denominator is 0!");
		}
	}
	
	private void parseFromBigInts (BigInteger n, BigInteger d) {
		numerator = n;
		denominator = d;
		if (n.signum() != d.signum()) {
			negative = true;
		}
		numerator = numerator.abs();
		denominator = denominator.abs();
	}
	
	public RationalNumber (BigInteger n, BigInteger d) {
		if (d.signum() != 0) {
			parseFromBigInts(n, d);
			BigInteger gcd = gcd(numerator, denominator);
			numerator = numerator.divide(gcd);
			denominator = denominator.divide(gcd);
		} else {
			throw new ArithmeticException ("Denominator is 0!");
		}
	}
	
	private void parseFromBigDecimal (BigDecimal rational) {
		if (rational.signum() == -1) {
			negative = true;
		}
		
		if (rational.scale() <= 0) {
			numerator = rational.toBigInteger();
			denominator = BigInteger.ONE;
		} else {
			numerator = rational.unscaledValue();
			denominator = BigInteger.TEN.pow(rational.scale());
		}
		numerator = numerator.abs();
	}
	
	/**
	 * NOT FINISHED YET!	
	 * @param rational Input number that is to be transformed into a RationalNumber
	 */
	private void parseFromString (String rational) {
		if (rational.contains(" / ")) {
			int div = rational.indexOf('/');
			try {
				numerator = new BigInteger (rational.substring(0, div - 1));
				denominator = new BigInteger (rational.substring(div + 1));
			} catch (NumberFormatException nfe) {
				throw new NumberFormatException ("Input string '" + rational +  "' is not correctly formatted!");
			}
		} else if (rational.contains(".")) {
			if (rational.contains("(") == false) {
				try {
					parseFromBigDecimal(new BigDecimal (rational));
				} catch (NumberFormatException nfe) {
					throw new NumberFormatException ("Input string '" + rational + "' is not correctly formatted!");
				}
			} else {
				//still missing!!
			}
		} else {
			try {
				denominator = new BigInteger (rational);
			} catch (NumberFormatException nfe) {
				throw new NumberFormatException ("Input string '" + rational + "' is not correctly formatted!");
			}
			numerator = BigInteger.ONE;
		}
	}
	
	public RationalNumber (BigDecimal rational) {
		if (rational.signum() == 0) {
			numerator = BigInteger.ZERO;
			denominator = BigInteger.ONE;
		} else {
			parseFromBigDecimal(rational);
			BigInteger gcd = gcd(numerator, denominator);
			numerator = numerator.divide(gcd);
			denominator = denominator.divide(gcd);
		}
	}
	
	public RationalNumber (String rational) {
		negative = rational.charAt(0) == '-';
		if (negative) {
			rational = rational.substring(1);
		}
		parseFromString(rational);
		BigInteger gcd = gcd(numerator, denominator);
		numerator = numerator.divide(gcd);
		denominator = denominator.divide(gcd);
	}
	
	private static BigInteger gcd (BigInteger n, BigInteger d) {
		BigInteger h;
		int nZeros = n.getLowestSetBit();
		int dZeros = d.getLowestSetBit();
		
		int minZeros = Math.min(nZeros, dZeros);
		
		n = n.shiftRight(minZeros);
		d = d.shiftRight(minZeros);
		while (d.signum() != 0) {
			h = n.remainder(d);
			n = d;
			d = h;
		}
		return n.shiftLeft(minZeros);
	}
	
	public BigInteger gcd () {
		return gcd(numerator, denominator);
	}
	
	public int signum () {
		if (numerator.signum() == 0) {
			return 0;
		} else if (negative) {
			return -1;
		} else {
			return 1;
		}
	}
	
	private static BigInteger lcmStatic (BigInteger a, BigInteger b) {
		return a.multiply(b).abs().divide(gcd(a, b));
	}
	
	private static RationalNumber addAbsolute (RationalNumber arg1, RationalNumber arg2) {
		if (arg1.denominator.compareTo(arg2.denominator) != 0) {
			BigInteger lcm = lcmStatic(arg1.denominator, arg2.denominator);
			BigInteger num1 = arg1.numerator.multiply(lcm.divide(arg1.denominator));
			BigInteger num2 = arg2.numerator.multiply(lcm.divide(arg2.denominator));
			return new RationalNumber (num1.add(num2), lcm, false);
		} else {
			return new RationalNumber (arg1.numerator.add(arg2.numerator), arg1.denominator, false);
		}
	}
	
	private static RationalNumber subtractAbsolute (RationalNumber arg1, RationalNumber arg2) {
		if (arg1.denominator.compareTo(arg2.denominator) != 0) {
			BigInteger lcm = lcmStatic(arg1.denominator, arg2.denominator);
			BigInteger num1 = arg1.numerator.multiply(lcm.divide(arg1.denominator));
			BigInteger num2 = arg2.numerator.multiply(lcm.divide(arg2.denominator));
			boolean sgn = false;
			if (num1.compareTo(num2) < 0) {
				sgn = true;
			}
			return new RationalNumber (num1.subtract(num2), lcm, sgn);
		} else {
			return ZERO;
		}
	}
	
	public RationalNumber add (RationalNumber arg) {
		int sgn1 = this.signum();
		int sgn2 = arg.signum();
		if (sgn1 == sgn2) {
			if (sgn1 >= 0) {
				return addAbsolute (this, arg);
			} else {
				return addAbsolute (this, arg).negate();
			}
		} else {
			if (sgn1 > sgn2) {
				return subtractAbsolute (this, arg);
			} else {
				return subtractAbsolute (arg, this);
			}
		}
	}
	
	public RationalNumber add (BigInteger arg) {
		return this.add(new RationalNumber (arg, BigInteger.ONE));
	}
	
	private RationalNumber (BigInteger n, BigInteger d, boolean neg) {
		numerator = n;
		denominator = d;
		if (n.signum() != 0) {
			negative = neg;
		} else {
			negative = false;
		}
	}
	
	public RationalNumber negate () {
		return new RationalNumber (numerator, denominator, negative == false);
	}
	
	public RationalNumber subtract (RationalNumber arg) {
		return this.add(arg.negate());
	}
	
	public RationalNumber subtract (BigInteger arg) {
		return this.subtract(new RationalNumber (arg, BigInteger.ONE));
	}
	
	public RationalNumber multiply (RationalNumber arg) {
		if (this.signum() == 0 || arg.signum() == 0) {
			return ZERO;
		} else {
			BigInteger gcd = gcd(numerator, arg.denominator);
			BigInteger num1 = numerator.divide(gcd);
			BigInteger den2 = arg.denominator.divide(gcd);
			gcd = gcd(denominator, arg.numerator);
			BigInteger num2 = arg.numerator.divide(gcd);
			BigInteger den1 = denominator.divide(gcd);
			return new RationalNumber (num1.multiply(num2), den1.multiply(den2), this.negative != arg.negative);
		}
	}
	
	public RationalNumber multiply (BigInteger arg) {
		return this.multiply(new RationalNumber (arg, BigInteger.ONE));
	}
	
	public RationalNumber invert () {
		if (numerator.signum() != 0) {
			return new RationalNumber (denominator, numerator, negative);
		} else {
			throw new ArithmeticException ("Cannot invert the number, as the numerator is 0!");
		}
	}
	
	public RationalNumber divide (RationalNumber arg) {
		try {
			return this.multiply(arg.invert());
		} catch (ArithmeticException ae) {
			throw new ArithmeticException ("Cannot divide by a number that has the numerical value 0!");
		}
	}
	
	public RationalNumber divide (BigInteger arg) {
		if (arg.signum() != 0) {
			return this.multiply(new RationalNumber (BigInteger.ONE, arg));
		} else {
			throw new ArithmeticException ("Cannot divide by 0!");
		}
	}
	
	public RationalNumber power (int exp) {
		if (numerator.signum() == 0) {
			return ZERO;
		}
		if (exp >= 0) {
			boolean neg = false;
			if (negative && exp % 2 != 0) {
				neg = true;
			}
			return new RationalNumber (numerator.pow(exp), denominator.pow(exp), neg);
		} else {
			exp *= -1;
			return this.power(exp).invert();
		}
	}
	
	@Override
	public String toString () {
		return numerator.toString() + " / " + denominator.toString();
	}
	
	public String toString (MathContext mc) {
		return new BigDecimal (numerator).divide(new BigDecimal (denominator), mc).toString();
	}
	
	public String toPlainString (MathContext mc) {
		return new BigDecimal (numerator).divide(new BigDecimal (denominator), mc).toPlainString();
	}
	
	public String toEngineeringString (MathContext mc) {
		return new BigDecimal (numerator).divide(new BigDecimal (denominator), mc).toEngineeringString();
	}
	
	public String toScientificString (MathContext mc) {
		return NTools.toScientificString(new BigDecimal (numerator).divide(new BigDecimal (denominator), mc));
	}

	@Override
	public int compareTo (RationalNumber o) {
		if (!this.negative && o.negative) {
			return 1;
		} else if (this.negative && !o.negative) {
			return -1;
		} else {
			BigInteger equalDenom = lcmStatic(this.denominator, o.denominator);
			BigInteger fact1 = equalDenom.divide(this.denominator);
			BigInteger fact2 = equalDenom.divide(o.numerator);
			RationalNumber t = new RationalNumber(this.numerator, equalDenom);
			t.numerator = t.numerator.multiply(fact1);
			o = new RationalNumber(o.numerator, equalDenom);
			o.numerator = o.numerator.multiply(fact2);
			int c = t.numerator.compareTo(o.numerator);
			if (this.negative) {
				return -c;
			} else {
				return c;
			}
		}
	}
}
