/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

import java.math.*;

public class AComplex {
	private BigDecimal real;
	private BigDecimal imaginary;
	
	private static MathContext prec = new MathContext (25);
	private static int internalPrecision = 5;
	
	public static final AComplex ZERO = new AComplex (BigDecimal.ZERO, BigDecimal.ZERO);
	public static final AComplex ONE = new AComplex (BigDecimal.ONE, BigDecimal.ZERO);
	public static final AComplex I = new AComplex (BigDecimal.ZERO, BigDecimal.ONE);
	
	public static void setPrec (int precision) {
		if (precision > 0) {
			prec = new MathContext (precision + internalPrecision);
		} else {
			throw new IllegalArgumentException ("The precision must be a positive number!");
		}
	}
	
	public static int getPrec () {
		return prec.getPrecision() - internalPrecision;
	}
	
	public static MathContext precm10 () {
		return new MathContext (prec.getPrecision() - internalPrecision);
	}
	
	public static void setIP (int precision) {
		if (precision + prec.getPrecision() > 0) {
			prec = precm10();
			internalPrecision = precision;
			prec = new MathContext (prec.getPrecision() + internalPrecision);
		} else {
			throw new IllegalArgumentException ("The precision as a whole must be positive!");
		}
	}
	
	public static int getIP () {
		return internalPrecision;
	}
	
	public AComplex (String r, String i) {
		real = new BigDecimal (r);
		imaginary = new BigDecimal (i);
	}
	
	public AComplex (BigDecimal r, BigDecimal i) {
		real = r;
		imaginary = i;
	}
	
	public AComplex (BigInteger r, BigInteger i) {
		real = new BigDecimal (r);
		imaginary = new BigDecimal (i);
	}
	
	public AComplex (int r, int i) {
		real = new BigDecimal (r);
		imaginary = new BigDecimal (i);
	}
	
	public AComplex (double r, double i) {
		real = new BigDecimal (r);
		imaginary = new BigDecimal (i);
	}
	
	public AComplex (ComplexDouble complex) {
		real = new BigDecimal (complex.getReal());
		imaginary = new BigDecimal (complex.getImaginary());
	}
	
	public AComplex (ADecimal r, ADecimal i) {
		real = r.toBigDecimal();
		imaginary = i.toBigDecimal();
	}
	
	public AComplex (String complex) {
		try {
			String [] c = NTools.complexParser(complex);
			real = new BigDecimal (c [0]);
			imaginary = new BigDecimal (c [1]);
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException ("Input (" + complex + ") is not of correct format!");
		}
	}
	
	public BigDecimal getReal () {
		return real.round(prec);
	}
	
	public BigDecimal getImaginary () {
		return imaginary.round(prec);
	}
	
	public boolean equals (AComplex number2) {
		if (this.getReal().compareTo(number2.getReal()) == 0 && this.getImaginary().compareTo(number2.getImaginary()) == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public String toString () {
		if (this.getImaginary().signum() >= 0) {
			return this.getReal().round(precm10()).toString() + " + " + this.getImaginary().round(precm10()).toString() + "i";
		} else {
			return this.getReal().round(precm10()).toString() + " - " + this.getImaginary().negate().round(precm10()).toString() + "i";
		}
	}
	
	public String toPlainString () {
		if (this.getImaginary().signum() >= 0) {
			return this.getReal().round(precm10()).toPlainString() + " + " + this.getImaginary().round(precm10()).toPlainString() + "i";
		} else {
			return this.getReal().round(precm10()).toPlainString() + " - " + this.getImaginary().negate().round(precm10()).toPlainString() + "i";
		}
	}
	
	public String toEngineeringString () {
		if (this.getImaginary().signum() >= 0) {
			return this.getReal().round(precm10()).toEngineeringString() + " + " + this.getImaginary().round(precm10()).toEngineeringString() + "i";
		} else {
			return this.getReal().round(precm10()).toEngineeringString() + " - " + this.getImaginary().negate().round(precm10()).toEngineeringString() + "i";
		}
	}
	
	public String toScientificString () {
		if (this.getImaginary().signum() >= 0) {
			return NTools.toScientificString(this.getReal().round(precm10())) + " + " + NTools.toScientificString(this.getImaginary().round(precm10())) + "i";
		} else {
			return NTools.toScientificString(this.getReal().round(precm10())) + " - " + NTools.toScientificString(this.getImaginary().negate().round(precm10())) + "i";
		}
	}
	
	public boolean isReal () {
		if (this.getImaginary().signum() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public BigDecimal arg () {
		try {
			return Arbitrary.atan2(this.getImaginary(), this.getReal(), prec, 0);
		} catch (ArithmeticException ae) {
			throw new ArithmeticException ("Angle of the complex number 0+0i cannot be determined!");
		}
	}
	
	public BigDecimal abs () {
		BigDecimal abssqr = this.abssqr().round(prec);
		return Arbitrary.sqrt(abssqr, prec, 0);
	}
	
	private BigDecimal abssqr () {
		return this.getReal().multiply(this.getReal()).add(this.getImaginary().multiply(this.getImaginary()));
	}
	
	public AComplex conjugate () {
		return new AComplex (this.getReal(), this.getImaginary().negate());
	}
	
	public AComplex add (AComplex summand2) {
		BigDecimal r1 = this.getReal();
		BigDecimal i1 = this.getImaginary();
		BigDecimal r2 = summand2.getReal();
		BigDecimal i2 = summand2.getImaginary();
		return new AComplex (r1.add(r2, prec), i1.add(i2, prec));
	}
	
	public AComplex subtract (AComplex summand2) {
		BigDecimal r1 = this.getReal();
		BigDecimal i1 = this.getImaginary();
		BigDecimal r2 = summand2.getReal();
		BigDecimal i2 = summand2.getImaginary();
		return new AComplex (r1.subtract(r2), i1.subtract(i2));
	}
	
	public AComplex multiply (AComplex factor) {
		BigDecimal r1 = this.getReal();
		BigDecimal i1 = this.getImaginary();
		BigDecimal r2 = factor.getReal();
		BigDecimal i2 = factor.getImaginary();
		BigDecimal r = r1.multiply(r2).subtract(i1.multiply(i2), prec);
		BigDecimal i = i1.multiply(r2).add(r1.multiply(i2), prec);
		return new AComplex (r, i);
	}
	
	public AComplex divide (AComplex divisor) {
		AComplex n1 = this.multiply(divisor.conjugate());
		BigDecimal n2 = divisor.abssqr();
		BigDecimal r = n1.getReal().divide(n2, prec);
		BigDecimal i = n1.getImaginary().divide(n2, prec);
		return new AComplex (r, i);
	}
	
	public AComplex sqr () {
		BigDecimal r1 = this.getReal();
		BigDecimal i1 = this.getImaginary();
		BigDecimal r = r1.pow(2).subtract(i1.pow(2));
		BigDecimal i = new BigDecimal (2).multiply(i1.multiply(r1), prec);
		return new AComplex (r, i);
	}

	private static BigDecimal [] sincos (BigDecimal arg) {
		BigDecimal [] sc = new BigDecimal [2];
		sc [0] = Arbitrary.sin(arg, prec, 0);
		sc [1] = Arbitrary.sqrt(BigDecimal.ONE.subtract(sc [0].multiply(sc [0])), prec, 0);
		BigDecimal pi = Arbitrary.pi(prec, 0);
		if (arg.abs().compareTo(pi.add(pi)) > 0) {
			BigDecimal factor = arg.divideToIntegralValue(pi);
			arg = arg.subtract(pi.multiply(factor));
		}
		if (arg.abs().compareTo(pi.multiply(BigDecimal.valueOf(5, 1))) > 0 && arg.abs().compareTo(pi.multiply(BigDecimal.valueOf(15, 1))) <= 0) {
			sc [1] = sc [1].negate();
		}

		return sc;
	}
	
	public AComplex sqrt () {
		if (this.equals(ZERO) == false) {
			BigDecimal abs = this.abs();
			abs = Arbitrary.sqrt(abs, prec, 0);
			BigDecimal arg = this.arg();
			arg = arg.multiply(BigDecimal.valueOf(5, 1));
			BigDecimal [] ri = sincos(arg);
			ri [0] = ri [0].multiply(abs, prec);
			ri [1] = ri [1].multiply(abs, prec);
			return new AComplex (ri [1], ri [0]);
		} else {
			return ZERO;
		}
	}
	
	public AComplex cbrt () {
		if (this.equals(ZERO) == false) {
			BigDecimal abs = this.abs();
			abs = Arbitrary.cbrt(abs, prec, 0);
			BigDecimal arg = this.arg();
			arg = arg.divide(new BigDecimal (3), prec);
			BigDecimal [] ri = sincos(arg);
			ri [0] = ri [0].multiply(abs, prec);
			ri [1] = ri [1].multiply(abs, prec);
			return new AComplex (ri [1], ri [0]);
		} else {
			return ZERO;
		}
	}
	
	public AComplex exp () {
		BigDecimal exp = Arbitrary.exp(this.getReal(), prec, 0);
		BigDecimal imag = this.getImaginary();
		BigDecimal twoPi = Arbitrary.pi(prec, 0).multiply(new BigDecimal (2));
		BigInteger ratio = imag.divide(twoPi, prec).toBigInteger();
		imag = imag.subtract(new BigDecimal (ratio).multiply(twoPi));
		BigDecimal [] sc = new BigDecimal [2];
		BigDecimal lower1 = BigDecimal.valueOf(79, 2);
		BigDecimal upper1 = BigDecimal.valueOf(235, 2);
		BigDecimal lower2 = BigDecimal.valueOf(393, 2);
		BigDecimal upper2 = BigDecimal.valueOf(724, 2);
		if (imag.compareTo(lower1) > 0 && imag.compareTo(upper1) < 0) {
			sc [1] = Arbitrary.cos(this.real, prec, 0);
			sc [0] = Arbitrary.sqrt(BigDecimal.ONE.subtract(sc [1].multiply(sc [1])).round(prec), prec, 0);
		} else if (imag.compareTo(lower2) > 0 && imag.compareTo(upper2) < 0) {
			sc [1] = Arbitrary.cos(this.real, prec, 0);
			sc [0] = Arbitrary.sqrt(BigDecimal.ONE.subtract(sc [1].multiply(sc [1])).round(prec), prec, 0).negate();
		} else {
			sc = sincos(imag);
		}
		BigDecimal cos = sc [1];
		BigDecimal sin = sc [0];
		cos = cos.multiply(exp, prec);
		sin = sin.multiply(exp, prec);
		return new AComplex (cos, sin);
	}
	
	public AComplex ln () {
		if (this.equals(ZERO) == false) {
			BigDecimal ln = Arbitrary.ln(this.abs(), prec, 0);
			BigDecimal arg = this.arg();
			return new AComplex (ln, arg);
		} else {
			throw new ArithmeticException ("Cannot calculate the natural logarithm of 0!");
		}
	}
	
	public AComplex power (AComplex exponent) {
		if (exponent.isReal() && Functions.isInteger(exponent.getReal())) {
			if (exponent.equals(ZERO) == false) { 
				AComplex result = ONE;
				AComplex sqr = this;
				int exp = exponent.getReal().intValue();
				boolean negative = exp < 0;
				if (negative) {
					exp = -exp;
				}

				int l = 32 - Integer.numberOfLeadingZeros(exp);
				for (int i = 0; i < l; i++) {
					if ((exp & 1) == 1) {
						exp >>= 1;
						result = result.multiply(sqr);
					}
					sqr = sqr.sqr();
				}
				return negative ? ONE.divide(result) : result;
			} else {
				return ONE;
			}
		} else {
			AComplex ln = exponent.multiply(this.ln());
			return ln.exp();
		}
	}
	
	public AComplex sin () {
		AComplex expix = this.multiply(I).exp();
		AComplex expmix = ONE.divide(expix);
		return expix.subtract(expmix).divide(new AComplex (0, 2));
	}
	
	public AComplex cos () {
		AComplex expix = this.multiply(I).exp();
		AComplex expmix = ONE.divide(expix);
		return expix.add(expmix).divide(new AComplex (2, 0));
	}
	
	public AComplex tan () {
		AComplex arg = this.multiply(I);
		AComplex exp1 = arg.exp();
		AComplex exp1conj = exp1.conjugate();
		AComplex exp2 = exp1conj.divide(new AComplex (exp1.abssqr(), BigDecimal.ZERO));
		AComplex zaehler = exp1.subtract(exp2);
		AComplex nenner = exp1.add(exp2).multiply(I);
		return zaehler.divide(nenner);
	}
	
	public AComplex asin () {
		AComplex sqr = this.sqr();
		sqr = ONE.subtract(sqr);
		AComplex sqrt = sqr.sqrt();
		AComplex arg = this.multiply(I).add(sqrt);
		return arg.ln().multiply(new AComplex (0, -1));
	}
	
	public AComplex acos () {
		AComplex sqr = this.sqr();
		sqr = ONE.subtract(sqr);
		AComplex sqrt = sqr.sqrt().multiply(I);
		AComplex arg = this.add(sqrt);
		return arg.ln().multiply(new AComplex (0, -1));
	}
	
	public AComplex atan () {
		AComplex a = this.multiply(I).add(ONE);
		AComplex b = ONE.subtract(this.multiply(I));
		a = a.divide(b).ln();
		return a.divide(new AComplex (0, 2));
	}
	
	public AComplex sinh () {
		AComplex expx = this.exp();
		AComplex expmx = ONE.divide(expx);
		return expx.subtract(expmx).divide(new AComplex (2, 0));
	}
	
	public AComplex cosh () {
		AComplex expx = this.exp();
		AComplex expmx = ONE.divide(expx);
		return expx.add(expmx).divide(new AComplex (2, 0));
	}
	
	public AComplex tanh () {
		AComplex arg = this;
		AComplex exp1 = arg.exp();
		AComplex exp1conj = exp1.conjugate();
		AComplex exp2 = exp1conj.divide(new AComplex (exp1.abssqr(), BigDecimal.ZERO));
		AComplex zaehler = exp1.subtract(exp2);
		AComplex nenner = exp1.add(exp2);
		return zaehler.divide(nenner);
	}
	
	public AComplex arsinh () {
		AComplex sqrp1 = this.sqr().add(ONE);
		AComplex arg = this.add(sqrp1.sqrt());
		return arg.ln();
	}
	
	public AComplex arcosh () {
		AComplex sqrm1 = this.sqr().subtract(ONE);
		AComplex arg = this.add(sqrm1.sqrt());
		return arg.ln();
	}
	
	public AComplex artanh () {
		if (this.equals(ONE) == false) {
			AComplex zp1 = ONE.add(this);
			AComplex zm1 = ONE.subtract(this);
			AComplex ln = zp1.divide(zm1).ln();
			return ln.divide(new AComplex (2, 0));
		} else {
			throw new ArithmeticException ("Cannot calculate the artanh of 1!");
		}
	}
}
