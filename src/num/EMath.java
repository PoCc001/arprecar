/**
 * This file is part of Arprecar.
 *
 *  Copyright (C) 2021  Johannes Kloimböck
 *
 *  Arprecar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Arprecar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Arprecar.  If not, see <http://www.gnu.org/licenses/>.
 */

package num;

import static java.lang.Math.*;

/**
 * This class is meant to be an extension to the Math standard library. EMath contains several
 * functions that are not available via the other class.
 * @author Johannes Kloimb�ck
 *
 */
public class EMath {
	
	/**
	 * Prevent the instantiation of this class.
	 */
	private EMath () {}
	
	private static double [] factorials = new double [171];
	private static int factIndex = -1;
	private static final double LN2 = Math.log(2);
	
	public static double arsinh (double x) {
		return Math.log(x + sqrt(x * x + 1));
	}
	
	public static double arcosh (double x) {
		return Math.log(x + sqrt(x * x - 1));
	}
	
	public static double artanh (double x) {
		return Math.log((1 + x) / (1 - x)) / 2;
	}
	
	public static double factorial (long n) {
		if (n == 0) {
			if (factIndex < 0) {
				factIndex = 0;
			}
			factorials [0] = 1;
			return 1;
		} else if (n == 1) {
			if (factIndex < 1) {
				factorials [0] = 1;
				factIndex = 1;
			}
			factorials [1] = 1;
			return 1;
		} else if (n == 2) {
			if (factIndex < 2) {
				factorials [0] = 1;
				factorials [1] = 1;
				factIndex = 2;
			}
			factorials [2] = 2;
			return 2;
		} else if (n >= 3 && n <= 170) {
			if (factIndex < 3) {
				factIndex = 3;
				factorials [0] = 1;
				factorials [1] = 1;
				factorials [2] = 2;
				factorials [3] = 6;
			}
			if (factIndex >= n) {
				return factorials [(int)(n)];
			}
			double r = factorials [factIndex];
			for (long i = n; i > factIndex; i--) {
				r *= i;
				factorials [(int)(i)] = r;
			}
			return r;
		} else if (n > 170) {
			return Double.POSITIVE_INFINITY;
		} else {
			return Double.NaN;
		}
	}
	
	public static double agm (double x, double y) {
		if (x < 0 || y < 0) {
			return Double.NaN;
		} else if (x == 0 || y == 0) {
			return 0;
		} else {
			double a = (x + y) / 2;
			double g = sqrt(x * y);
			double a1 = a;
			double g1 = g;
			boolean b = false;
			while (!b) {
				a = (a1 + g1) / 2;
				g = sqrt(a1 * g1);
				if (a == a1) {
					b = true;
				}
				a1 = a;
				g1 = g;
			}
			return (a + g) / 2;
		}
	}
	
	public static double sind (double x) {
		return sin(x / 180 * PI);
	}
	
	public static double sing (double x) {
		return sin(x / 200 * PI);
	}
	
	public static double cosd (double x) {
		return cos(x / 180 * PI);
	}
	
	public static double cosg (double x) {
		return sin(x / 200 * PI);
	}
	
	public static double tand (double x) {
		return tan(x / 180 * PI);
	}
	
	public static double tang (double x) {
		return tan(x / 200 * PI);
	}
	
	public static double asind (double x) {
		return asin(x) * 180 / PI;
	}
	
	public static double asing (double x) {
		return asin(x) * 200 / PI;
	}
	
	public static double acosd (double x) {
		return acos(x) * 180 / PI;
	}
	
	public static double acosg (double x) {
		return acos(x) * 200 / PI;
	}
	
	public static double atand (double x) {
		return atan(x) * 180 / PI;
	}
	
	public static double atang (double x) {
		return atan(x) * 200 / PI;
	}
	
	public static double log (double x, double b) {
		return Math.log(x) / Math.log(b);
	}
	
	public static double ld (double x) {
		return Math.log(x) / LN2;
	}
	
	public static double cdfFromZero (double x) {
		double result = x;
		double result2 = x + 1;
		double term = -x * x * x / 6;
		double xsqr = -x * x;
		int i = 2;
		while (true) {
			result += term;
			term *= xsqr * (2 * i - 1)/ ((2 * i) * (2 * i + 1));
			i++;
			if (result2 == result) {
				return result / sqrt(2 * PI);
			}
			result2 = result;
		}
	}
	
	public static double cdf (double x) {
		return cdfFromZero(x) + 0.5;
	}
	
	public static double binCoeff (long n, long k) {
		if (n == k || k == 0) {
			return 1;
		} else if (k == 1 || n == k + 1) {
			return n;
		}
		k = k <= n - k ? k : n - k;
		long nmk = n - k;
		double result = n;
		for (long i = nmk + 1; i < n; i++) {
			result *= i;
		}
		return result / factorial(k);
	}
	
	public static long gcd (long x, long y) {
		long h = 0;
		while (y != 0) {
			h = x % y;
			x = y;
			y = h;
		}
		
		return abs(x);
	}
	
	public static boolean coprime (long x, long y) {
		return gcd(x, y) == 1;
	}
	
	public static long lcm (long x, long y) {
		return abs(x * y) / gcd(x, y);
	}
	
	public static double subfactorial (long x) {
		if (x == 0) {
			return 1;
		}
		
		double sf = (factorial(x) + 1) / E;
		
		if (x <= 20) {
			return (double)(sf);
		} else if (x <= 170) {
			return sf;
		} else {
			sf = factorial(170) / E;
			for (long i = 171; i <= x; i++) {
				sf *= i;
			}
			return sf + 1 / E;
		}
	}
}
