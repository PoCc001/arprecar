The **Arprecar library**, the **Advanced Arbitrary Precision Arithmetic library** is meant to evaluate real functions, like the square root, the trigonometric functions, the exponential function and logarithms to arbitrary precision. It only uses functions and classes from the standard library (StrictMath, BigInteger and BigDecimal for example).

This library might be the right choice for you if you...
*  ...need to efficiently calculate various functions to arbitrary precision.
*  ...deal with numbers whose magnitudes are too large or low for primitive datatypes to handle.
*  ...need additional functions to those provided by the standard library (Math and StrictMath).
*  ...want to do calculations with complex or rational numbers.
*  ...(certainly many more reasons ;-))

For some of the methods in *NTools.java*, Java 11 or higher is needed.

Arprecar is licensed under the GNU GPLv3 license (see LICENSE file).

**WARNING:** The documentation is far from complete! In addition, the library might not yet be foolproof and free from bugs and wrong results.
